----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:16:19 04/23/2020 
-- Design Name: 
-- Module Name:    generadorSenales - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity generadorSenales is
    Port ( inEstado : in  STD_LOGIC_VECTOR (1 downto 0);
           VSync : out  STD_LOGIC;
           HSync : out  STD_LOGIC;
           vidAct : out  STD_LOGIC);
end generadorSenales;

architecture Behavioral of generadorSenales is
	signal auxSalidas : STD_LOGIC_VECTOR(2 downto 0);
begin
	VSync <= auxSalidas(2);
	HSync <= auxSalidas(1);
	vidAct <= auxSalidas(0);
	
	with inEstado select
		auxSalidas <= "010" when "00",
						  "110" when "01",
						  "100" when "10",
						  "111" when "11",
						  "000" when others;

end Behavioral;

