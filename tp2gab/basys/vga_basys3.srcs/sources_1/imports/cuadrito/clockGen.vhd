library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity clockGen is
generic(multip : INTEGER := 5);
port( clkIn : in STD_LOGIC;
		clkOut : out STD_LOGIC);
end clockGen;

architecture Behavioral of clockGen is
begin
	process(clkIn)
		variable contador : INTEGER range 0 to multip := 0;
		variable inic : STD_LOGIC := '0';
	begin
		if(clkIn'event and clkIn = '1') then
			if(inic = '0') then
				clkOut <= '0';
				inic := '1';
			end if;
		
			contador := contador + 1;
			
			if(contador = multip/2) then
				clkOut <= '0';
			elsif(contador = multip) then
				clkOut <= '1';
				contador := 0;
			end if;
		end if;
	end process;
end Behavioral;

