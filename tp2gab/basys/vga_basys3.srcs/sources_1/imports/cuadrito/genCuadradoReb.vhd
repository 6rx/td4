----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:28:09 05/03/2020 
-- Design Name: 
-- Module Name:    genCuadradoReb - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity genCuadradoReb is
    Port ( clk : in STD_LOGIC;
			  reset : in  STD_LOGIC;
			  clkMov : in STD_LOGIC;		--salida del frame
           vidEnabled : in  STD_LOGIC;
			  Pxx : in STD_LOGIC_VECTOR(9 downto 0);
			  Pxy : in STD_LOGIC_VECTOR(8 downto 0);
           Rin : in  STD_LOGIC;
           Gin : in  STD_LOGIC;
           Bin : in  STD_LOGIC;
           Rout : out  STD_LOGIC;
           Gout : out  STD_LOGIC;
           Bout : out  STD_LOGIC);
end genCuadradoReb;

architecture Behavioral of genCuadradoReb is

	component clockGen
		generic( multip : integer);
		port( clkIn : in std_logic;
				clkOut : out std_logic);
	end component;

	signal contadorH : std_logic_vector(9 downto 0);
	signal contadorV : std_logic_vector(8 downto 0);
	
	signal estadoH : std_logic_vector(1 downto 0);
	signal estadoV : std_logic_vector(1 downto 0);

	signal auxMover : std_logic;
	signal aleat : std_logic;

begin

	aleat <= Pxx(9) xor Pxx(8) xor Pxx(7) xor Pxx(3) xor Pxy(5) xor Pxy(3) xor Pxy(7) xor Rin xor Gin xor Bin;

	tempMov : clockGen
		generic map(6)
		port map(clkIn => clkMov,
					clkOut => auxMover);
	
	generadorEstado: process(clk)
		variable aumAnt : std_logic := '0';
	begin
		if(clk'event and clk = '1')then
			if(reset = '1')then
				contadorH <= "0000111100";
				contadorV <= "000111100";
				estadoH <= "00";
				estadoV <= "00";
			else
				if(vidEnabled = '1')then
					if(aumAnt = '0')then
						if(auxMover = '1')then
							aumAnt := '1';
							
							if(estadoH = "00") then			--x+
								contadorH <= contadorH + 1;
							elsif(estadoH = "01") then		--x-
								contadorH <= contadorH - 1;
							else									--x=
								contadorH <= contadorH;
							end if;
							
							if(estadoV = "00") then			--y+
								contadorV <= contadorV + 1;
							elsif(estadoV = "01") then		--y-
								contadorV <= contadorV - 1;
							else									--y=
								contadorV <= contadorV;
							end if;
							
							if(639 - 25 = contadorH or 25 = contadorH)then
								if(estadoH = "00") then
									if(aleat = '1')then
										estadoH <= "01";
									else
										estadoH <= "10";
									end if;
								elsif(estadoH = "01") then
									if(aleat = '1')then
										estadoH <= "00";
									else
										estadoH <= "10";
									end if;
								else
									if(aleat = '1')then
										estadoH <= "00";
									else
										estadoH <= "01";
									end if;
								end if;
							end if;
							
							if(439 - 25 = contadorV or 25 = contadorV)then
								if(estadoV = "00") then
									if(aleat = '0')then
										estadoV <= "10";
									else
										estadoV <= "01";
									end if;
								elsif(estadoV = "01") then
									if(aleat = '0')then
										estadoV <= "00";
									else
										estadoV <= "10";
									end if;
								else
									if(aleat = '0')then
										estadoH <= "00";
									else
										estadoH <= "01";
									end if;
								end if;
							end if;
							
							if(Pxx > contadorH - 25 and Pxx < contadorH + 25 and Pxy > contadorV - 25 and Pxy < contadorV + 25) then
								Rout <= not Rin;
								Gout <= not Gin;
								Bout <= not Bin;
							else
								Rout <= Rin;
								Gout <= Gin;
								Bout <= Bin;
							end if;
						else
							estadoH <= estadoH;
							estadoV <= estadoV;
						end if;
					elsif(auxMover = '0')then
						aumAnt := '0';
					end if;
				else
					Rout <= '0';
					Gout <= '0';
					Bout <= '0';
				end if;
			end if;
		end if;
	end process;

end Behavioral;

