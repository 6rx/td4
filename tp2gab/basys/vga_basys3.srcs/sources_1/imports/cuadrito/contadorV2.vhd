library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity contadorV2 is
		Generic( limite : integer;
					largoSalida : integer);
		Port( clk : in STD_LOGIC;
				reset : in STD_LOGIC;
				aumento : in STD_LOGIC;
				enabled : in STD_LOGIC;
				blockEnabled : in STD_LOGIC;
      		salida : out  STD_LOGIC_VECTOR(largoSalida downto 0));
end contadorV2;

architecture Behavioral of contadorV2 is
	signal salidaAux : STD_LOGIC_VECTOR(largoSalida downto 0);
begin
	salida <= salidaAux;
	
	process(clk)
		variable aumAnt : STD_LOGIC := '0';
	begin
		if(clk'event and clk = '1')then
			if(reset = '1') then
				salidaAux <= (others => '0');
			else
				if(enabled = '1') then
					if(blockEnabled = '1')then
						if(aumAnt = '0')then
							if(aumento = '1') then
								aumAnt := '1';
								
								if(conv_integer(salidaAux) = limite)then
									salidaAux <= (others => '0');
								else
									salidaAux <= salidaAux + 1;
								end if;
							else
								salidaAux <= salidaAux;
							end if;
						else
							if(aumento = '0')then
								aumAnt := '0';
							else
								aumAnt := '1';
							end if;
						end if;
					else
						if(aumento = '1')then
							if(conv_integer(salidaAux) = limite)then
								salidaAux <= (others => '0');
							else
								salidaAux <= salidaAux + 1;
							end if;
						else
							salidaAux <= salidaAux;
						end if;
					end if;
				else
					salidaAux <= salidaAux;
				end if;
			end if;
		end if;
	end process;

end Behavioral;

