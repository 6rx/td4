library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity topVGAdriver is
	PORT( clk : IN STD_LOGIC;
			reset : IN STD_LOGIC;
			selecHorVer : IN STD_LOGIC;
			HSync : OUT STD_LOGIC;
			VSync : OUT STD_LOGIC;
			R : OUT STD_LOGIC;
			G : OUT STD_LOGIC;
			B : OUT STD_LOGIC);
end topVGAdriver;

architecture Behavioral of topVGAdriver is

	component clockGen
		generic( multip : integer);
		port( clkIn : in std_logic;
				clkOut : out std_logic);
	end component;
	
	component maquinaEstadosV2
		generic( Sp : integer;
					Bp : integer;
					Dt : integer;
					Fp : integer;
					largoEntrada : integer);
		port( clk : in std_logic;
				reset : in std_logic;
				contIn : in std_logic_vector(largoEntrada downto 0);
				syncOut : out std_logic;
				dispEnabled : out std_logic;
				resetCont : out std_logic); 
	end component;
	
	component contadorV2
		generic( limite : integer;
					largoSalida : integer);
		port( clk : in STD_LOGIC;
				reset : in STD_LOGIC;
				aumento : in STD_LOGIC;
				enabled : in STD_LOGIC;
				blockEnabled : in STD_LOGIC;
      		salida : out  STD_LOGIC_VECTOR(largoSalida downto 0));
	end component;
	
	component generadorBandas is
		port( clk : in std_logic;
				reset : in std_logic;
				Pxx : in std_logic_vector(9 downto 0);
				Pxy : in std_logic_vector(8 downto 0);
				HorVer : in std_logic;
				vidEnabled : in std_logic;
				R : out std_logic;
				G : out std_logic;
				B : out std_logic);
	end component;
	
	component genCuadradoReb is
		port(clk : in STD_LOGIC;
			  reset : in  STD_LOGIC;
			  clkMov : in STD_LOGIC;		--salida del frame
			  vidEnabled : in  STD_LOGIC;
			  Pxx : in STD_LOGIC_VECTOR(9 downto 0);
			  Pxy : in STD_LOGIC_VECTOR(8 downto 0);
			  Rin : in  STD_LOGIC;
			  Gin : in  STD_LOGIC;
			  Bin : in  STD_LOGIC;
			  Rout : out  STD_LOGIC;
			  Gout : out  STD_LOGIC;
			  Bout : out  STD_LOGIC);
	end component;

	signal auxClk25M : std_logic;
	signal auxResetHor : std_logic;
	signal auxResetVer : std_logic;
	signal auxHSync : std_logic;
	signal auxVSync : std_logic;
	signal auxContH : std_logic_vector(9 downto 0);
	signal auxContV : std_logic_vector(8 downto 0);
	signal auxPxx : std_logic_vector(9 downto 0);
	signal auxPxy : std_logic_vector(8 downto 0);
	signal auxHDispEnabled : std_logic;
	signal auxVDispEnabled : std_logic;
	signal auxAumentoVer : std_logic;
	signal auxDispEnabled : std_logic;

	--signal auxMover : std_logic;

	signal auxR : std_logic;
	signal auxG : std_logic;
	signal auxB : std_logic;

begin

	HSync <= not auxHSync;
	VSync <= not auxVSync;
	
	auxAumentoVer <= not auxHSync;
	
	auxDispEnabled <= auxHDispEnabled and auxVDispEnabled;
	
	auxPxx <= auxContH WHEN auxHDispEnabled = '1' and auxVDispEnabled = '1' ELSE (others => '0');
	auxPxy <= auxContV WHEN auxVDispEnabled = '1' ELSE (others => '0');

	clkGen125a25 : clockGen
		generic map(4)
		port map(clkIn => clk,
					clkOut => auxClk25M);
	
	contadorH : contadorV2
		generic map(640,9)
		port map(clk => auxClk25M,
					reset => auxResetHor,
					aumento => '1',
					enabled => '1',
					blockEnabled => '0',
					salida => auxContH);
	
	maqEstH : maquinaEstadosV2
		generic map(96,48,640,16,9)
		port map(clk => auxClk25M,
					reset => reset,
					contIn => auxContH,
					syncOut => auxHSync,
					dispEnabled => auxHDispEnabled,
					resetCont => auxResetHor);
					
	contadorV : contadorV2
		generic map(480,8)
		port map(clk => auxClk25M,
					reset => auxResetVer,
					aumento => auxAumentoVer,
					enabled => '1',
					blockEnabled => '1',
					salida => auxContV);
					
	maqEstV : maquinaEstadosV2
		generic map(3,34,481,11,8)
		port map(clk => auxClk25M,
					reset => reset,
					contIn => auxContV,
					syncOut => auxVSync,
					dispEnabled => auxVDispEnabled,
					resetCont => auxResetVer);
					
	genBan : generadorBandas
		port map(clk => auxClk25M,
					reset => reset,
					Pxx => auxPxx,
					Pxy => auxPxy,
					HorVer => selecHorVer,
					vidEnabled => auxDispEnabled,
					R => auxR,
					G => auxG,
					B => auxB);
					
	generadorCuad: genCuadradoReb
		port map(clk => auxClk25M,
					reset => reset,
					clkMov => auxVSync,
					vidEnabled => auxDispEnabled,
					Pxx => auxPxx,
					Pxy => auxPxy,
					Rin => auxR,
					Gin => auxG,
					Bin => auxB,
					Rout => R,
					Gout => G,
					Bout => B);
					
	
end Behavioral;

