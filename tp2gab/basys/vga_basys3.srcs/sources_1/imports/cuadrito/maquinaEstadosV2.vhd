library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity maquinaEstadosV2 is
	generic( Sp : integer;	--Sync pulse
				Bp : integer;	--Back porch
				Dt : integer;	--Pulse time
				Fp : integer;
				largoEntrada : integer);	--Front porch
	port( clk : in std_logic;
			reset : in std_logic;
			contIn : in std_logic_vector(largoEntrada downto 0);
			syncOut : out std_logic;
			dispEnabled : out std_logic;
			resetCont : out std_logic);
end maquinaEstadosV2;

architecture Behavioral of maquinaEstadosV2 is
	type tipoEstado is (st1_syncPulse, st2_backPorch, st3_displayTime, st4_frontporch);
	signal estado, proxEstado : tipoEstado;
	
	signal proxSyncOut : std_logic;
	signal proxDispEnabled : std_logic;
	signal proxAuxReset : std_logic;
	
	signal auxReset : std_logic;
begin

	resetCont <= auxReset or reset;
	
	SYNC_PROC: process (clk)
   begin
      if (clk'event and clk = '1') then
         if (reset = '1') then
            estado <= st1_syncPulse;
				syncOut <= '0';
				dispEnabled <= '0';
         else
            estado <= proxEstado;
				syncOut <= proxSyncOut;
				dispEnabled <= proxDispEnabled;
         end if;        
      end if;
   end process;
	
	OUTPUT_DECODE: process (estado)
   begin
		case estado is
			when st1_syncPulse => 
				proxSyncOut <= '0';
				proxDispEnabled <= '0';
			when st2_backPorch => 
				proxSyncOut <= '1';
				proxDispEnabled <= '0';
			when st3_displayTime => 
				proxSyncOut <= '1';
				proxDispEnabled <= '1';
			when st4_frontPorch =>
				proxSyncOut <= '1';
				proxDispEnabled <= '0';
			when others => null;
		end case;
   end process;
	
	NEXT_STATE_DECODE: process (estado, contIn)
	begin
		proxEstado <= estado;
		
		case (estado) is
			when st1_syncPulse =>
				if contIn = Sp - 1 then
					proxEstado <= st2_backPorch;
					auxReset <= '1';
				else
					auxReset <= '0';
				end if;
			when st2_backPorch =>
				if contIn = Bp - 1 then
					proxEstado <= st3_displayTime;
					auxReset <= '1';
				else
					auxReset <= '0';
				end if;
			when st3_displayTime =>
				if contIn = Dt - 1 then
					proxEstado <= st4_frontPorch;
					auxReset <= '1';
				else
					auxReset <= '0';
				end if;
			when st4_frontPorch =>
				if contIn = Fp - 1 then
					proxEstado <= st1_syncPulse;
					auxReset <= '1';
				else
					auxReset <= '0';
				end if;
			when others => null;
		end case;
	end process;

end Behavioral;

