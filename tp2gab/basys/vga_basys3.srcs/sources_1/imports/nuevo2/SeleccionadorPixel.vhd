----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:34:39 04/23/2020 
-- Design Name: 
-- Module Name:    SeleccionadorPixel - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SeleccionadorPixel is
    Port ( clk : in  STD_LOGIC;
           vidAct : in  STD_LOGIC;
           resetPxy : in  STD_LOGIC;
			  resetPxx : in  STD_LOGIC;
           Pxx : out  STD_LOGIC_VECTOR (9 downto 0);
           Pxy : out  STD_LOGIC_VECTOR (8 downto 0);
           Rx : out  STD_LOGIC);
end SeleccionadorPixel;

architecture Behavioral of SeleccionadorPixel is

	component contador
	generic( limite : integer);
	port( clk : in std_logic;
			blockEnabled : in std_logic;
			aumento : in std_logic;
			reset : in std_logic;
			salida : out std_logic_vector(9 downto 0));
	end component;
	
	signal auxSalidaPxx : STD_LOGIC_VECTOR(9 downto 0);
	signal auxSalidaPxy : STD_LOGIC_VECTOR(9 downto 0);
	signal auxNoVidAct : STD_LOGIC;
	
begin
	
	Pxx <= auxSalidaPxx;
	Pxy <= auxSalidaPxy (8 downto 0);
	
	auxNoVidAct <= not vidAct;
	
	Rx <= '1' WHEN conv_integer(auxSalidaPxy) = 480 ELSE '0';
	
	contPxx : contador
	generic map (640)
	port map(
		clk => clk,
		aumento => vidAct,
		blockEnabled => '0',
		reset => resetPxx,
		salida => auxSalidaPxx);
		
	contPxy : contador
	generic map (490)
	port map(
		clk => clk,
		aumento => auxNoVidAct,
		blockEnabled => '1',
		reset => resetPxy,
		salida => auxSalidaPxy);

end Behavioral;

