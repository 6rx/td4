library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity contador is
		Generic( limite : integer);
		Port( clk : in STD_LOGIC;
				blockEnabled : in STD_LOGIC;
				aumento : in  STD_LOGIC;
				reset : in STD_LOGIC;
      		salida : out  STD_LOGIC_VECTOR(9 downto 0));
end contador;

architecture Behavioral of contador is
	signal salidaAux : STD_LOGIC_VECTOR(9 downto 0);
begin
	salida <= salidaAux;
	
	process(clk)
		variable aumAnt : STD_LOGIC := '0';
	begin
		if(clk'event and clk = '1')then
			if(blockEnabled = '1')then
				if(reset = '1')then
					salidaAux <= (others => '0');
				elsif(aumento = '1' and aumAnt = '0')then
					aumAnt := '1';
					
					if(conv_integer(salidaAux) = limite)then
						salidaAux <= (others => '0');
					else
						salidaAux <= salidaAux + 1;
					end if;
				elsif(aumento = '0' and aumAnt = '1')then
					aumAnt := '0';
				end if;
			else
				if(reset = '1')then
					salidaAux <= (others => '0');
				elsif(aumento = '1')then
					if(conv_integer(salidaAux) = limite)then
						salidaAux <= (others => '0');
					else
						salidaAux <= salidaAux + 1;
					end if;
				end if;
			end if;
		end if;
	end process;
end Behavioral;