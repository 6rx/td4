----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:34:53 04/21/2020 
-- Design Name: 
-- Module Name:    maqEstVGA - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity maqEstVGA is
	 generic ( Vb : integer := 1600;	-- Vertical pulse width
				  Vc : integer := 23200;	-- Vertical back porch
				  Ve : integer := 8000;	-- Vertical front porch
				  Hb : integer := 96;	-- Horizontal pulse width
				  Hc : integer := 48;	-- Horizontal back porch
				  Hd : integer := 640;	-- Horizontal display time
				  He : integer := 16);	-- Horizontal front porch
    Port ( clk : in  STD_LOGIC;
           Rx : in  STD_LOGIC;
			  reset : in STD_LOGIC;
           E : out  STD_LOGIC_VECTOR (1 downto 0));
end maqEstVGA;

architecture Behavioral of maqEstVGA is
   type state_type is (st1_VSyncPulse, st2_VBackPorch, st3_HSyncPulse, st4_HBackPorch, st5_VidAct, st6_HFrontPorch, st7_VFrontPorchA, st8_VFrontPorchB); 
   signal state, next_state : state_type;
	
	signal proxSal : STD_LOGIC_VECTOR (1 downto 0);
	signal contador : UNSIGNED (14 downto 0);
	signal resetCont0 : STD_LOGIC;
	signal resetCont1 : STD_LOGIC;
	
begin
	resetCont0 <= reset or resetCont1;

   SYNC_PROC: process (clk)
   begin
      if (clk'event and clk = '1') then
         if (reset = '1') then
            state <= st1_VSyncPulse;
            E <= "00";
         else
            state <= next_state;
            E <= proxSal;
         -- assign other outputs to internal signals
         end if;        
      end if;
   end process;
	
	OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
      --below is simple example
		case state is
			when st1_VSyncPulse => proxSal <= "00";
			when st2_VBackPorch => proxSal <= "01";
			when st3_HSyncPulse => proxSal <= "10";
			when st4_HBackPorch => proxSal <= "01";
			when st5_VidAct => proxSal <= "11";
			when st6_HFrontPorch => proxSal <= "01";
			when st7_VFrontPorchA => proxSal <= "10";
			when st8_VFrontPorchB => proxSal <= "01";
			when others => null;
		end case;
   end process;
	--st1_VSyncPulse, st2_VBackPorch, st3_HSyncPulse, st4_HBackPorch, st5_VidAct, st6_HFrontPorch, st7_VFrontPorchA, st8_VFrontPorchB); 
	
	NEXT_STATE_DECODE: process (state, contador, Rx)
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state
      --insert statements to decode next_state
      --below is a simple example

      case (state) is
         when st1_VSyncPulse =>
            if contador = Vb-1 then
               next_state <= st2_VBackPorch;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
         when st2_VBackPorch =>
            if contador = Vc-1 then
               next_state <= st3_HSyncPulse;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
         when st3_HSyncPulse =>
            if contador = Hb-1 then
               next_state <= st4_HBackPorch;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
         when st4_HBackPorch =>
            if contador = Hc-1 then
               next_state <= st5_VidAct;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
         when st5_VidAct =>
            if contador = Hd-1 then
               next_state <= st6_HFrontPorch;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
         when st6_HFrontPorch =>
				if contador = He-1 then
					if(Rx = '0')then
						next_state <= st3_HSyncPulse;
						resetCont1 <= '1';
					else
						next_state <= st7_VFrontPorchA;
						resetCont1 <= '1';
					end if;
				else
					resetCont1 <= '0';
				end if;
         when st7_VFrontPorchA =>
            if contador = Hb-1 then
               next_state <= st8_VFrontPorchB;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
         when st8_VFrontPorchB =>
            if contador = Ve - Hb - 1 then
               next_state <= st1_VSyncPulse;
					resetCont1 <= '1';
				else
					resetCont1 <= '0';
            end if;
      end case;      
   end process;
	
	process(clk)
	begin
		if(clk'event and clk = '1')then
			if(resetCont0 = '1')then
				contador <= (others => '0');
			else
				contador <= contador + 1;
			end if;
		end if;
	end process;

end Behavioral;

