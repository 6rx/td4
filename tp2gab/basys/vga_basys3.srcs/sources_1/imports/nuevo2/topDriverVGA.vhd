----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:38:12 04/23/2020 
-- Design Name: 
-- Module Name:    topDriverVGA - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity topDriverVGA is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           selecHorVer : in  STD_LOGIC;
           VSync : out  STD_LOGIC;
           HSync : out  STD_LOGIC;
			  
			  --muestraPxx : out STD_LOGIC_VECTOR(9 downto 0);
			  --muestraPxy : out STD_LOGIC_VECTOR(8 downto 0);
			  
           Rout : out  STD_LOGIC;
           Gout : out  STD_LOGIC;
           Bout : out  STD_LOGIC);
end topDriverVGA;

architecture Behavioral of topDriverVGA is
	
	component clockGen
		generic (multip : integer);
		port(	clkIn : in STD_LOGIC;
				clkOut : out STD_LOGIC);
	end component;
	
	component SeleccionadorPixel
		port( clk : in  STD_LOGIC;
				vidAct : in  STD_LOGIC;
				resetPxy : in  STD_LOGIC;
				resetPxx : in STD_LOGIC;
				Pxx : out  STD_LOGIC_VECTOR (9 downto 0);
				Pxy : out  STD_LOGIC_VECTOR (8 downto 0);
				Rx : out  STD_LOGIC);
	end component;
	
	component generadorSenales
		port( inEstado : in  STD_LOGIC_VECTOR (1 downto 0);
				VSync : out  STD_LOGIC;
				HSync : out  STD_LOGIC;
				vidAct : out  STD_LOGIC);
	end component;
	
	component maqEstVGA
		generic( Vb : integer;	-- Vertical pulse width
				   Vc : integer;	-- Vertical back porch
				   Ve : integer;	-- Vertical front porch
				   Hb : integer;	-- Horizontal pulse width
				   Hc : integer;	-- Horizontal back porch
				   Hd : integer;	-- Horizontal display time
				   He : integer);	-- Horizontal front porch
		port( clk : in  STD_LOGIC;
				Rx : in  STD_LOGIC;
				reset : in STD_LOGIC;
				E : out  STD_LOGIC_VECTOR (1 downto 0));
	end component;

	component generadorBandas
		port( clk : in std_logic;
				reset : in std_logic;
				Pxx : in  STD_LOGIC_VECTOR (9 downto 0);
           Pxy : in  STD_LOGIC_VECTOR (8 downto 0);
           HorVer : in  STD_LOGIC;
			  vidEnabled : in STD_LOGIC;
           R : out  STD_LOGIC;
           G : out  STD_LOGIC;
           B : out  STD_LOGIC);
	end component;

	signal auxEstados : STD_LOGIC_VECTOR (1 downto 0);
	signal auxRx : STD_LOGIC;
	signal auxVSync : STD_LOGIC;
	signal auxClk25Mhz : STD_LOGIC;
	signal auxVidAct : STD_LOGIC;
	signal auxHSync : STD_LOGIC;
	signal auxPxx : STD_LOGIC_VECTOR (9 downto 0);
	signal auxPxy : STD_LOGIC_VECTOR (8 downto 0);
	signal enResPxx : STD_LOGIC;
	signal enResPxy : STD_LOGIC;
	
begin

	VSync <= auxVSync;
	HSync <= auxHSync;
	
--	muestraPxx <= auxPxx;
--	muestraPxy <= auxPxy;

	clk125Ma25M : clockGen
		generic map(5)
		port map(
			clkIn => clk,
			clkOut => auxClk25Mhz);
					
	maqEst : maqEstVGA
		generic map(1600, 23200, 8000, 96, 48, 640, 16)
		port map(
			clk => auxClk25Mhz,
			Rx => auxRx,
			reset => reset,
			E => auxEstados);
			
	genSel : generadorSenales
		port map(
			inEstado => auxEstados,
			VSync => auxVSync,
			HSync => auxHSync,
			vidAct => auxVidAct);
			
	enResPxy <= not auxVSync;
	enResPxx <= not auxHSync;
			
	selecPx : SeleccionadorPixel
		port map(
			clk => auxClk25Mhz,
			vidAct => auxVidAct,
			resetPxy => enResPxy,
			resetPxx => enResPxx,
			Pxx => auxPxx,
			Pxy => auxPxy,
			Rx => auxRx);
			
	genBand : generadorBandas
		port map(
			clk => auxClk25Mhz,
			reset => reset,
			Pxx => auxPxx,
			Pxy => auxPxy,
			HorVer => selecHorVer,
			vidEnabled => auxVidAct,
			R => Rout,
			G => Gout,
			B => Bout);
			
--	Pxx : in  STD_LOGIC_VECTOR (9 downto 0);
--	  Pxy : in  STD_LOGIC_VECTOR (8 downto 0);
--	  HorVer : in  STD_LOGIC;
--	  vidEnabled : in STD_LOGIC;
--	  R : out  STD_LOGIC;
--	  G : out  STD_LOGIC;
--	  B : out  STD_LOGIC);
			
--			clk : in  STD_LOGIC;
--				resetPxxAumPxy : in  STD_LOGIC;
--				resetPxy : in  STD_LOGIC;
--				Pxx : out  STD_LOGIC_VECTOR (9 downto 0);
--				Pxy : out  STD_LOGIC_VECTOR (8 downto 0);
--				Rx : out  STD_LOGIC);

end Behavioral;

