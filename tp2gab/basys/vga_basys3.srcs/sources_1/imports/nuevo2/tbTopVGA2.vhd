-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
          COMPONENT topDriverVGA is
				 Port ( clk : in  STD_LOGIC;
						  reset : in  STD_LOGIC;
						  selecHorVer : in  STD_LOGIC;
						  VSync : out  STD_LOGIC;
						  HSync : out  STD_LOGIC;
						  
						  --muestraPxx : out STD_LOGIC_VECTOR(9 downto 0);
						  --muestraPxy : out STD_LOGIC_VECTOR(8 downto 0);
						  
						  Rout : out  STD_LOGIC;
						  Gout : out  STD_LOGIC;
						  Bout : out  STD_LOGIC);
          END COMPONENT;

			
			signal clk : STD_LOGIC := '0';
			signal reset : STD_LOGIC := '1';
			signal selecHorVer : STD_LOGIC := '0';
			
			signal VSync : STD_LOGIC;
			signal HSync : STD_LOGIC;
			--signal muestraPxx : STD_LOGIC_VECTOR(9 downto 0);
			--signal muestraPxy : STD_LOGIC_VECTOR(8 downto 0);
         signal Rout : STD_LOGIC;
			signal Gout : STD_LOGIC;
			signal Bout : STD_LOGIC;

  BEGIN

  -- Component Instantiation
          uut: topDriverVGA PORT MAP(
					clk => clk,
					reset => reset,
					selecHorVer => selecHorVer,
					
					VSync => VSync,
					HSync => HSync,
					--muestraPxx => muestraPxx,
					--muestraPxy => muestraPxy,
					Rout => Rout,
					Gout => Gout,
					Bout => Bout
          );
	
		process
		begin
			wait for 200 ns;
			
			reset <= '0';
			wait;
		end process;

		process
		begin
			wait for 4 ns;
			
			clk <= not clk;
		end process;

  END;
