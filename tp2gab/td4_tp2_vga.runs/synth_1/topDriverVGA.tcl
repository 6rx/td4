# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
create_project -in_memory -part xc7z010clg400-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.cache/wt [current_project]
set_property parent.project_path /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property board_part_repo_paths {/home/nacho/.Xilinx/Vivado/2019.2/xhub/board_store} [current_project]
set_property board_part digilentinc.com:zybo-z7-10:part0:1.0 [current_project]
set_property ip_output_repo /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_vhdl -library xil_defaultlib {
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/SeleccionadorPixel.vhd
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/clockGen.vhd
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/contador.vhd
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/genBanV2.vhd
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/generadorSenales.vhd
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/maqEstVGA.vhd
  /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/sources_1/imports/nuevo2/topDriverVGA.vhd
}
# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/constrs_1/imports/digilent-xdc/Zybo-Z7-Master.xdc
set_property used_in_implementation false [get_files /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.srcs/constrs_1/imports/digilent-xdc/Zybo-Z7-Master.xdc]

set_param ips.enableIPCacheLiteLoad 1
close [open __synthesis_is_running__ w]

synth_design -top topDriverVGA -part xc7z010clg400-1


# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef topDriverVGA.dcp
create_report "synth_1_synth_report_utilization_0" "report_utilization -file topDriverVGA_utilization_synth.rpt -pb topDriverVGA_utilization_synth.pb"
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
