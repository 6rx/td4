library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
--use IEEE.STD_LOGIC_ARITH.ALL;

entity graphic_unit is
    Port ( 
            clk                 :   in      std_logic;
            video_on            :   in      std_logic;
            pixel_x, pixel_y    :   in      std_logic_vector (10 downto 0);
            rgb                 :   out     std_logic_vector (2 downto 0)
        );
end graphic_unit;

architecture arch of graphic_unit is
    
    signal  pix_x, pix_y    :   unsigned (10 downto 0);
    
    -- Pipe the colorbar red, green and blue signals
    signal rgb_next     : std_logic_vector(2 downto 0) := (others => '0');

    -- VGA R, G and B signals coming from the main multiplexers
    signal rgb_reg      : std_logic_vector(2 downto 0);

    -- VGA R, G and B signals coming from the main multiplexers
    signal vga_rgb_cmb  : std_logic_vector(2 downto 0);
    
    -- VGA R, G and B signals coming from the main multiplexers
    signal rgb_bg        : std_logic_vector(2 downto 0);
    signal rgb_ball     : std_logic_vector(2 downto 0);
    
    constant MAX_X : integer := 1280;
    constant MAX_Y : integer := 1024;
    
    constant BALL_SIZE: integer := 8;
    constant BALL_X_L : integer := 580;
    constant BALL_X_R : integer := BALL_X_L + BALL_SIZE -1;
    
    constant BALL_Y_T : integer := 238;
    constant BALL_X_B : integer := BALL_Y_T + BALL_SIZE -1;
    
    signal sq_ball_on:  std_logic;    
 
begin
    
    pix_x <= unsigned(pixel_x);            
    pix_y <= unsigned(pixel_y);

    process(clk)
	begin
		if rising_edge(clk) then
            if (pix_x >= 0) and (pix_x < 160) then
                rgb_bg <= "111";
            elsif (pix_x >= 160) and (pix_x < 320)  then
                rgb_bg <= "110";
            elsif (pix_x >= 320) and (pix_x < 480)  then
                rgb_bg <= "101";
            elsif (pix_x >= 480) and (pix_x < 640)  then
                rgb_bg <= "100"; 
            elsif (pix_x >= 640) and (pix_x < 800)  then
                rgb_bg <= "011"; 
            elsif (pix_x >= 800) and (pix_x < 960)  then
                rgb_bg <= "010";                                                              
            elsif (pix_x >= 960) and (pix_x < 1120)  then
                rgb_bg <= "001";
            else
                rgb_bg <= "000";                
            end if;
        end if;	
	end process;
	

    
    ---------------------------------------------------------------------------------------------------
    -- Register Outputs coming from the displaying components and the horizontal and vertical counters
    -------------------------------------------------------------------------------------------------
    process (clk)
    begin
        if (rising_edge(clk)) then
            rgb_next      <= rgb_bg;
        end if;
    end process;
    
    ------------------------------------------------------------
    -- Turn Off VGA RBG Signals if outside of the active screen
    -- Make a 4-bit AND logic with the R, G and B signals
    ------------------------------------------------------------
    vga_rgb_cmb     <=  (video_on & video_on & video_on) and rgb_next;
   
    process (clk)
    begin
        if (rising_edge(clk)) then          
            rgb_reg    <= vga_rgb_cmb;
        end if;
    end process;
    
    rgb     <= rgb_reg;     

end arch;
