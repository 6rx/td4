----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:02:20 04/24/2020 
-- Design Name: 
-- Module Name:    generadorBandas - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity generadorBandas is
    Port ( clk : in STD_LOGIC;
			  reset : in STD_LOGIC;
			  Pxx : in  STD_LOGIC_VECTOR (9 downto 0);
           Pxy : in  STD_LOGIC_VECTOR (8 downto 0);
           HorVer : in  STD_LOGIC;
			  vidEnabled : in STD_LOGIC;
           R : out  STD_LOGIC;
           G : out  STD_LOGIC;
           B : out  STD_LOGIC);
end generadorBandas;

architecture Behavioral of generadorBandas is
   type state_type is (st1, st2, st3, st4, st5, st6, st7, st8); 
   signal state, next_state : state_type; 
   
   signal Rnext : std_logic;
	signal Gnext : std_logic;
	signal Bnext : std_logic;
	
   signal auxR : std_logic;
	signal auxG : std_logic;
	signal auxB : std_logic;
begin

	R <= auxR and vidEnabled;
	G <= auxG and vidEnabled;
	B <= auxB and vidEnabled;

   SYNC_PROC: process (clk)
   begin
      if (clk'event and clk = '1') then
         if (reset = '1') then
            state <= st1;
            auxR <= '0';
				auxG <= '0';
				auxB <= '0';
         else
            state <= next_state;
            auxR <= Rnext;
				auxG <= Gnext;
				auxB <= Bnext;
         end if;        
      end if;
   end process;
 
   OUTPUT_DECODE: process (state)
   begin
		if(state = st1)then
			Rnext <= '1';
			Gnext <= '1';
			Bnext <= '1';
		elsif(state = st2)then
			Rnext <= '1';
			Gnext <= '1';
			Bnext <= '0';
		elsif(state = st3)then
			Rnext <= '0';
			Gnext <= '1';
			Bnext <= '1';
		elsif(state = st4)then
			Rnext <= '0';
			Gnext <= '1';
			Bnext <= '0';
		elsif(state = st5)then
			Rnext <= '1';
			Gnext <= '0';
			Bnext <= '1';
		elsif(state = st6)then
			Rnext <= '1';
			Gnext <= '0';
			Bnext <= '0';
		elsif(state = st7)then
			Rnext <= '0';
			Gnext <= '0';
			Bnext <= '1';
		elsif(state = st8)then
			Rnext <= '0';
			Gnext <= '0';
			Bnext <= '0';
		else
			Rnext <= '0';
			Gnext <= '0';
			Bnext <= '0';
		end if;
	end process;
 
   NEXT_STATE_DECODE: process (state, Pxx, Pxy, HorVer)
   begin
      next_state <= state;  
      case (state) is
         when st1 =>
				if HorVer = '1' then
					if Pxy = 60 then
						next_state <= st2;
					end if;
            elsif Pxx = 80 then
               next_state <= st2;
            end if;
         when st2 =>
				if HorVer = '1' then
					if Pxy = 2*60 then
						next_state <= st3;
					end if;
            elsif Pxx = 2*80 then
               next_state <= st3;
            end if;
         when st3 =>
				if HorVer = '1' then
					if Pxy = 3*60 then
						next_state <= st4;
					end if;
            elsif Pxx = 3*80 then
               next_state <= st4;
            end if;
         when st4 =>
				if HorVer = '1' then
					if Pxy = 4*60 then
						next_state <= st5;
					end if;
            elsif Pxx = 4*80 then
               next_state <= st5;
            end if;
         when st5 =>
				if HorVer = '1' then
					if Pxy = 5*60 then
						next_state <= st6;
					end if;
            elsif Pxx = 5*80 then
               next_state <= st6;
            end if;
         when st6 =>
				if HorVer = '1' then
					if Pxy = 6*60 then
						next_state <= st7;
					end if;
            elsif Pxx = 6*80 then
               next_state <= st7;
            end if;
         when st7 =>
				if HorVer = '1' then
					if Pxy = 7*60 then
						next_state <= st8;
					end if;
            elsif Pxx = 7*80 then
               next_state <= st8;
            end if;
         when st8 =>
				if HorVer = '1' then
					if Pxy = 0 then
						next_state <= st1;
					end if;
				elsif Pxx = 0 then
					next_state <= st1;
            end if;
         when others =>
            next_state <= st1;
      end case;      
   end process;	

end Behavioral;

