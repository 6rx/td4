----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:02:20 04/24/2020 
-- Design Name: 
-- Module Name:    generadorBandas - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity generadorBandas is
    Port ( Pxx : in  STD_LOGIC_VECTOR (9 downto 0);
           Pxy : in  STD_LOGIC_VECTOR (8 downto 0);
           HorVer : in  STD_LOGIC;
			  vidEnabled : in STD_LOGIC;
           R : out  STD_LOGIC;
           G : out  STD_LOGIC;
           B : out  STD_LOGIC);
end generadorBandas;

architecture Behavioral of generadorBandas is
begin
	process(Pxx,Pxy, vidEnabled, HorVer)
	begin
		if(vidEnabled = '0')then
			R <= '0';
			G <= '0';
			B <= '0';
		else
			if(HorVer = '0')then
				if(Pxx<80)then
					R <= '1';
					G <= '1';
					B <= '1';
				elsif(Pxx<2*80)then
					R <= '1';
					G <= '1';
					B <= '0';
				elsif(Pxx<3*80)then
					R <= '0';
					G <= '1';
					B <= '1';
				elsif(Pxx<4*80)then
					R <= '0';
					G <= '1';
					B <= '0';
				elsif(Pxx<5*80)then
					R <= '1';
					G <= '0';
					B <= '1';
				elsif(Pxx<6*80)then
					R <= '1';
					G <= '0';
					B <= '0';
				elsif(Pxx<7*80)then
					R <= '0';
					G <= '0';
					B <= '1';
				elsif(Pxx<8*80)then
					R <= '0';
					G <= '0';
					B <= '0';			
				end if;
			else
				if(Pxy<60)then
					R <= '1';
					G <= '1';
					B <= '1';
				elsif(Pxy<2*60)then
					R <= '1';
					G <= '1';
					B <= '0';
				elsif(Pxy<3*60)then
					R <= '0';
					G <= '1';
					B <= '1';
				elsif(Pxy<4*60)then
					R <= '0';
					G <= '1';
					B <= '0';
				elsif(Pxy<5*60)then
					R <= '1';
					G <= '0';
					B <= '1';
				elsif(Pxy<6*60)then
					R <= '1';
					G <= '0';
					B <= '0';
				elsif(Pxy<7*60)then
					R <= '0';
					G <= '0';
					B <= '1';
				elsif(Pxy<8*60)then
					R <= '0';
					G <= '0';
					B <= '0';
				end if;
			end if;
		end if;	
	end process;

end Behavioral;

