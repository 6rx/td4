// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 28 15:34:12 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.sim/sim_1/synth/timing/xsim/tb_top_time_synth.v
// Design      : topDriverVGA
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module SeleccionadorPixel
   (aumAnt_reg,
    \salidaAux_reg[7] ,
    \salidaAux_reg[6] ,
    \salidaAux_reg[8] ,
    \salidaAux_reg[3] ,
    \salidaAux_reg[8]_0 ,
    aumAnt_reg_0,
    CLK,
    selecHorVer_IBUF,
    \FSM_sequential_state_reg[2] ,
    SR,
    E,
    \salidaAux_reg[0] ,
    \salidaAux_reg[0]_0 );
  output aumAnt_reg;
  output \salidaAux_reg[7] ;
  output \salidaAux_reg[6] ;
  output \salidaAux_reg[8] ;
  output \salidaAux_reg[3] ;
  output \salidaAux_reg[8]_0 ;
  input aumAnt_reg_0;
  input CLK;
  input selecHorVer_IBUF;
  input [0:0]\FSM_sequential_state_reg[2] ;
  input [0:0]SR;
  input [0:0]E;
  input [0:0]\salidaAux_reg[0] ;
  input [0:0]\salidaAux_reg[0]_0 ;

  wire CLK;
  wire [0:0]E;
  wire [0:0]\FSM_sequential_state_reg[2] ;
  wire [0:0]SR;
  wire aumAnt_reg;
  wire aumAnt_reg_0;
  wire contPxx_n_3;
  wire contPxx_n_4;
  wire contPxx_n_5;
  wire contPxy_n_2;
  wire contPxy_n_3;
  wire contPxy_n_4;
  wire contPxy_n_5;
  wire contPxy_n_6;
  wire contPxy_n_7;
  wire [0:0]\salidaAux_reg[0] ;
  wire [0:0]\salidaAux_reg[0]_0 ;
  wire \salidaAux_reg[3] ;
  wire \salidaAux_reg[6] ;
  wire \salidaAux_reg[7] ;
  wire \salidaAux_reg[8] ;
  wire \salidaAux_reg[8]_0 ;
  wire selecHorVer_IBUF;

  contador contPxx
       (.CLK(CLK),
        .G_reg(contPxy_n_4),
        .G_reg_0(contPxy_n_7),
        .Q(contPxy_n_2),
        .R_reg(contPxy_n_6),
        .R_reg_i_1_0(contPxy_n_3),
        .R_reg_i_1_1(contPxy_n_5),
        .\salidaAux_reg[0]_0 (\salidaAux_reg[0] ),
        .\salidaAux_reg[0]_1 (\salidaAux_reg[0]_0 ),
        .\salidaAux_reg[4]_0 (contPxx_n_4),
        .\salidaAux_reg[4]_1 (contPxx_n_5),
        .\salidaAux_reg[6]_0 (\salidaAux_reg[6] ),
        .\salidaAux_reg[8]_0 (\salidaAux_reg[8] ),
        .\salidaAux_reg[8]_1 (\salidaAux_reg[8]_0 ),
        .\salidaAux_reg[9]_0 (contPxx_n_3),
        .selecHorVer_IBUF(selecHorVer_IBUF));
  contador__parameterized1 contPxy
       (.B_reg(contPxx_n_3),
        .B_reg_i_1_0(contPxx_n_4),
        .B_reg_i_1_1(contPxx_n_5),
        .CLK(CLK),
        .E(E),
        .\FSM_sequential_state_reg[2] (\FSM_sequential_state_reg[2] ),
        .Q(contPxy_n_2),
        .SR(SR),
        .aumAnt_reg_0(aumAnt_reg),
        .aumAnt_reg_1(aumAnt_reg_0),
        .\salidaAux_reg[3]_0 (contPxy_n_3),
        .\salidaAux_reg[3]_1 (contPxy_n_5),
        .\salidaAux_reg[3]_2 (\salidaAux_reg[3] ),
        .\salidaAux_reg[6]_0 (contPxy_n_6),
        .\salidaAux_reg[7]_0 (\salidaAux_reg[7] ),
        .\salidaAux_reg[7]_1 (contPxy_n_7),
        .\salidaAux_reg[8]_0 (contPxy_n_4),
        .selecHorVer_IBUF(selecHorVer_IBUF));
endmodule

module clockGen
   (auxClk25Mhz,
    clk_IBUF_BUFG);
  output auxClk25Mhz;
  input clk_IBUF_BUFG;

  wire auxClk25Mhz;
  wire clkOut_i_1_n_0;
  wire clk_IBUF_BUFG;
  wire [2:0]contador;
  wire \contador[0]_i_1_n_0 ;
  wire \contador[1]_i_1_n_0 ;
  wire \contador[2]_i_1_n_0 ;
  wire inic;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h888F8808)) 
    clkOut_i_1
       (.I0(auxClk25Mhz),
        .I1(inic),
        .I2(contador[0]),
        .I3(contador[1]),
        .I4(contador[2]),
        .O(clkOut_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    clkOut_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(clkOut_i_1_n_0),
        .Q(auxClk25Mhz),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h0D)) 
    \contador[0]_i_1 
       (.I0(contador[2]),
        .I1(contador[1]),
        .I2(contador[0]),
        .O(\contador[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \contador[1]_i_1 
       (.I0(contador[1]),
        .I1(contador[0]),
        .O(\contador[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h68)) 
    \contador[2]_i_1 
       (.I0(contador[2]),
        .I1(contador[1]),
        .I2(contador[0]),
        .O(\contador[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\contador[0]_i_1_n_0 ),
        .Q(contador[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\contador[1]_i_1_n_0 ),
        .Q(contador[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\contador[2]_i_1_n_0 ),
        .Q(contador[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    inic_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(1'b1),
        .Q(inic),
        .R(1'b0));
endmodule

module contador
   (\salidaAux_reg[6]_0 ,
    \salidaAux_reg[8]_0 ,
    \salidaAux_reg[8]_1 ,
    \salidaAux_reg[9]_0 ,
    \salidaAux_reg[4]_0 ,
    \salidaAux_reg[4]_1 ,
    R_reg_i_1_0,
    R_reg_i_1_1,
    G_reg,
    G_reg_0,
    Q,
    selecHorVer_IBUF,
    R_reg,
    \salidaAux_reg[0]_0 ,
    \salidaAux_reg[0]_1 ,
    CLK);
  output \salidaAux_reg[6]_0 ;
  output \salidaAux_reg[8]_0 ;
  output \salidaAux_reg[8]_1 ;
  output \salidaAux_reg[9]_0 ;
  output \salidaAux_reg[4]_0 ;
  output \salidaAux_reg[4]_1 ;
  input R_reg_i_1_0;
  input R_reg_i_1_1;
  input G_reg;
  input G_reg_0;
  input [0:0]Q;
  input selecHorVer_IBUF;
  input R_reg;
  input [0:0]\salidaAux_reg[0]_0 ;
  input [0:0]\salidaAux_reg[0]_1 ;
  input CLK;

  wire B_reg_i_9_n_0;
  wire CLK;
  wire G_reg;
  wire G_reg_0;
  wire [0:0]Q;
  wire R_reg;
  wire R_reg_i_1_0;
  wire R_reg_i_1_1;
  wire R_reg_i_4_n_0;
  wire R_reg_i_5_n_0;
  wire [9:0]salidaAux;
  wire \salidaAux[7]_i_2__0_n_0 ;
  wire \salidaAux[7]_i_3__0_n_0 ;
  wire \salidaAux[7]_i_4_n_0 ;
  wire \salidaAux[8]_i_2__0_n_0 ;
  wire \salidaAux[9]_i_4_n_0 ;
  wire \salidaAux[9]_i_5_n_0 ;
  wire [0:0]\salidaAux_reg[0]_0 ;
  wire [0:0]\salidaAux_reg[0]_1 ;
  wire \salidaAux_reg[4]_0 ;
  wire \salidaAux_reg[4]_1 ;
  wire \salidaAux_reg[6]_0 ;
  wire \salidaAux_reg[8]_0 ;
  wire \salidaAux_reg[8]_1 ;
  wire \salidaAux_reg[9]_0 ;
  wire \salidaAux_reg_n_0_[0] ;
  wire \salidaAux_reg_n_0_[1] ;
  wire \salidaAux_reg_n_0_[2] ;
  wire \salidaAux_reg_n_0_[3] ;
  wire \salidaAux_reg_n_0_[4] ;
  wire \salidaAux_reg_n_0_[5] ;
  wire \salidaAux_reg_n_0_[6] ;
  wire \salidaAux_reg_n_0_[7] ;
  wire \salidaAux_reg_n_0_[8] ;
  wire \salidaAux_reg_n_0_[9] ;
  wire selecHorVer_IBUF;

  LUT6 #(
    .INIT(64'h0010100000001000)) 
    B_reg_i_12
       (.I0(\salidaAux_reg_n_0_[4] ),
        .I1(\salidaAux_reg_n_0_[5] ),
        .I2(R_reg_i_5_n_0),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(\salidaAux_reg_n_0_[7] ),
        .I5(\salidaAux_reg_n_0_[8] ),
        .O(\salidaAux_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h0101000301030003)) 
    B_reg_i_3
       (.I0(\salidaAux_reg_n_0_[9] ),
        .I1(B_reg_i_9_n_0),
        .I2(selecHorVer_IBUF),
        .I3(\salidaAux_reg_n_0_[7] ),
        .I4(\salidaAux_reg_n_0_[5] ),
        .I5(\salidaAux_reg_n_0_[4] ),
        .O(\salidaAux_reg[9]_0 ));
  LUT6 #(
    .INIT(64'hC0C000C04000C000)) 
    B_reg_i_7
       (.I0(\salidaAux_reg_n_0_[4] ),
        .I1(R_reg_i_5_n_0),
        .I2(\salidaAux_reg_n_0_[6] ),
        .I3(\salidaAux_reg_n_0_[7] ),
        .I4(\salidaAux_reg_n_0_[5] ),
        .I5(\salidaAux_reg_n_0_[8] ),
        .O(\salidaAux_reg[4]_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    B_reg_i_9
       (.I0(\salidaAux_reg_n_0_[6] ),
        .I1(\salidaAux_reg_n_0_[8] ),
        .O(B_reg_i_9_n_0));
  LUT6 #(
    .INIT(64'hFFFF222A222A222A)) 
    G_reg_i_1
       (.I0(R_reg_i_5_n_0),
        .I1(\salidaAux_reg_n_0_[8] ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(G_reg),
        .I5(G_reg_0),
        .O(\salidaAux_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hBEAFAAAAFEAFAAAA)) 
    R_reg_i_1
       (.I0(R_reg_i_4_n_0),
        .I1(\salidaAux_reg_n_0_[6] ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux_reg_n_0_[8] ),
        .I4(R_reg_i_5_n_0),
        .I5(\salidaAux_reg_n_0_[5] ),
        .O(\salidaAux_reg[6]_0 ));
  LUT6 #(
    .INIT(64'hFFFF11FF0F0F11FF)) 
    R_reg_i_2
       (.I0(\salidaAux_reg_n_0_[8] ),
        .I1(\salidaAux_reg_n_0_[7] ),
        .I2(Q),
        .I3(\salidaAux_reg_n_0_[9] ),
        .I4(selecHorVer_IBUF),
        .I5(R_reg),
        .O(\salidaAux_reg[8]_1 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAABAAA)) 
    R_reg_i_4
       (.I0(R_reg_i_1_0),
        .I1(\salidaAux_reg_n_0_[6] ),
        .I2(R_reg_i_5_n_0),
        .I3(\salidaAux_reg_n_0_[7] ),
        .I4(\salidaAux_reg_n_0_[5] ),
        .I5(R_reg_i_1_1),
        .O(R_reg_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    R_reg_i_5
       (.I0(\salidaAux_reg_n_0_[9] ),
        .I1(selecHorVer_IBUF),
        .O(R_reg_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00FF00F7)) 
    \salidaAux[0]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[9] ),
        .I1(\salidaAux_reg_n_0_[7] ),
        .I2(\salidaAux_reg_n_0_[1] ),
        .I3(\salidaAux_reg_n_0_[0] ),
        .I4(\salidaAux[9]_i_4_n_0 ),
        .O(salidaAux[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \salidaAux[1]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .O(salidaAux[1]));
  LUT3 #(
    .INIT(8'h78)) 
    \salidaAux[2]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[2] ),
        .O(salidaAux[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \salidaAux[3]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[2] ),
        .I3(\salidaAux_reg_n_0_[3] ),
        .O(salidaAux[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \salidaAux[4]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[2] ),
        .I2(\salidaAux_reg_n_0_[0] ),
        .I3(\salidaAux_reg_n_0_[3] ),
        .I4(\salidaAux_reg_n_0_[4] ),
        .O(salidaAux[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \salidaAux[5]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[4] ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux_reg_n_0_[0] ),
        .I3(\salidaAux_reg_n_0_[2] ),
        .I4(\salidaAux_reg_n_0_[1] ),
        .I5(\salidaAux_reg_n_0_[5] ),
        .O(salidaAux[5]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \salidaAux[6]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[4] ),
        .I1(\salidaAux_reg_n_0_[5] ),
        .I2(\salidaAux[8]_i_2__0_n_0 ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .O(salidaAux[6]));
  LUT6 #(
    .INIT(64'hFACA0030FFCF0030)) 
    \salidaAux[7]_i_1__0 
       (.I0(\salidaAux[7]_i_2__0_n_0 ),
        .I1(\salidaAux[7]_i_3__0_n_0 ),
        .I2(\salidaAux_reg_n_0_[1] ),
        .I3(\salidaAux[7]_i_4_n_0 ),
        .I4(\salidaAux_reg_n_0_[7] ),
        .I5(\salidaAux_reg_n_0_[9] ),
        .O(salidaAux[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \salidaAux[7]_i_2__0 
       (.I0(\salidaAux_reg_n_0_[2] ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux_reg_n_0_[5] ),
        .I3(\salidaAux_reg_n_0_[4] ),
        .I4(B_reg_i_9_n_0),
        .I5(\salidaAux_reg_n_0_[0] ),
        .O(\salidaAux[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \salidaAux[7]_i_3__0 
       (.I0(\salidaAux_reg_n_0_[2] ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[3] ),
        .O(\salidaAux[7]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \salidaAux[7]_i_4 
       (.I0(\salidaAux_reg_n_0_[5] ),
        .I1(\salidaAux_reg_n_0_[4] ),
        .I2(\salidaAux_reg_n_0_[6] ),
        .O(\salidaAux[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF7FFF00008000)) 
    \salidaAux[8]_i_1__0 
       (.I0(\salidaAux_reg_n_0_[7] ),
        .I1(\salidaAux_reg_n_0_[6] ),
        .I2(\salidaAux_reg_n_0_[4] ),
        .I3(\salidaAux_reg_n_0_[5] ),
        .I4(\salidaAux[8]_i_2__0_n_0 ),
        .I5(\salidaAux_reg_n_0_[8] ),
        .O(salidaAux[8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \salidaAux[8]_i_2__0 
       (.I0(\salidaAux_reg_n_0_[3] ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[2] ),
        .I3(\salidaAux_reg_n_0_[1] ),
        .O(\salidaAux[8]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF70707050)) 
    \salidaAux[9]_i_3__0 
       (.I0(\salidaAux_reg_n_0_[7] ),
        .I1(\salidaAux_reg_n_0_[1] ),
        .I2(\salidaAux_reg_n_0_[9] ),
        .I3(\salidaAux_reg_n_0_[0] ),
        .I4(\salidaAux[9]_i_4_n_0 ),
        .I5(\salidaAux[9]_i_5_n_0 ),
        .O(salidaAux[9]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \salidaAux[9]_i_4 
       (.I0(\salidaAux_reg_n_0_[6] ),
        .I1(\salidaAux_reg_n_0_[8] ),
        .I2(\salidaAux_reg_n_0_[4] ),
        .I3(\salidaAux_reg_n_0_[5] ),
        .I4(\salidaAux_reg_n_0_[3] ),
        .I5(\salidaAux_reg_n_0_[2] ),
        .O(\salidaAux[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hC8C8CCCC04000000)) 
    \salidaAux[9]_i_5 
       (.I0(\salidaAux[7]_i_3__0_n_0 ),
        .I1(\salidaAux_reg_n_0_[1] ),
        .I2(\salidaAux[7]_i_4_n_0 ),
        .I3(\salidaAux_reg_n_0_[7] ),
        .I4(\salidaAux_reg_n_0_[8] ),
        .I5(\salidaAux_reg_n_0_[9] ),
        .O(\salidaAux[9]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[0] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[0]),
        .Q(\salidaAux_reg_n_0_[0] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[1] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[1]),
        .Q(\salidaAux_reg_n_0_[1] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[2] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[2]),
        .Q(\salidaAux_reg_n_0_[2] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[3] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[3]),
        .Q(\salidaAux_reg_n_0_[3] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[4] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[4]),
        .Q(\salidaAux_reg_n_0_[4] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[5] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[5]),
        .Q(\salidaAux_reg_n_0_[5] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[6] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[6]),
        .Q(\salidaAux_reg_n_0_[6] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[7] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[7]),
        .Q(\salidaAux_reg_n_0_[7] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[8] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[8]),
        .Q(\salidaAux_reg_n_0_[8] ),
        .R(\salidaAux_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[9] 
       (.C(CLK),
        .CE(\salidaAux_reg[0]_1 ),
        .D(salidaAux[9]),
        .Q(\salidaAux_reg_n_0_[9] ),
        .R(\salidaAux_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "contador" *) 
module contador__parameterized1
   (aumAnt_reg_0,
    \salidaAux_reg[7]_0 ,
    Q,
    \salidaAux_reg[3]_0 ,
    \salidaAux_reg[8]_0 ,
    \salidaAux_reg[3]_1 ,
    \salidaAux_reg[6]_0 ,
    \salidaAux_reg[7]_1 ,
    \salidaAux_reg[3]_2 ,
    aumAnt_reg_1,
    CLK,
    B_reg,
    B_reg_i_1_0,
    selecHorVer_IBUF,
    \FSM_sequential_state_reg[2] ,
    B_reg_i_1_1,
    SR,
    E);
  output aumAnt_reg_0;
  output \salidaAux_reg[7]_0 ;
  output [0:0]Q;
  output \salidaAux_reg[3]_0 ;
  output \salidaAux_reg[8]_0 ;
  output \salidaAux_reg[3]_1 ;
  output \salidaAux_reg[6]_0 ;
  output \salidaAux_reg[7]_1 ;
  output \salidaAux_reg[3]_2 ;
  input aumAnt_reg_1;
  input CLK;
  input B_reg;
  input B_reg_i_1_0;
  input selecHorVer_IBUF;
  input [0:0]\FSM_sequential_state_reg[2] ;
  input B_reg_i_1_1;
  input [0:0]SR;
  input [0:0]E;

  wire B_reg;
  wire B_reg_i_10_n_0;
  wire B_reg_i_11_n_0;
  wire B_reg_i_13_n_0;
  wire B_reg_i_1_0;
  wire B_reg_i_1_1;
  wire B_reg_i_2_n_0;
  wire B_reg_i_4_n_0;
  wire B_reg_i_5_n_0;
  wire B_reg_i_6_n_0;
  wire B_reg_i_8_n_0;
  wire CLK;
  wire [0:0]E;
  wire [0:0]\FSM_sequential_state_reg[2] ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire aumAnt_reg_0;
  wire aumAnt_reg_1;
  wire [9:0]salidaAux;
  wire \salidaAux[3]_i_2_n_0 ;
  wire \salidaAux[3]_i_3_n_0 ;
  wire \salidaAux[7]_i_2_n_0 ;
  wire \salidaAux[7]_i_3_n_0 ;
  wire \salidaAux[8]_i_2_n_0 ;
  wire \salidaAux[8]_i_3_n_0 ;
  wire \salidaAux[9]_i_4__0_n_0 ;
  wire \salidaAux_reg[3]_0 ;
  wire \salidaAux_reg[3]_1 ;
  wire \salidaAux_reg[3]_2 ;
  wire \salidaAux_reg[6]_0 ;
  wire \salidaAux_reg[7]_0 ;
  wire \salidaAux_reg[7]_1 ;
  wire \salidaAux_reg[8]_0 ;
  wire \salidaAux_reg_n_0_[0] ;
  wire \salidaAux_reg_n_0_[1] ;
  wire \salidaAux_reg_n_0_[2] ;
  wire \salidaAux_reg_n_0_[3] ;
  wire \salidaAux_reg_n_0_[4] ;
  wire \salidaAux_reg_n_0_[5] ;
  wire \salidaAux_reg_n_0_[6] ;
  wire \salidaAux_reg_n_0_[7] ;
  wire \salidaAux_reg_n_0_[9] ;
  wire selecHorVer_IBUF;

  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    B_reg_i_1
       (.I0(B_reg_i_2_n_0),
        .I1(B_reg),
        .I2(B_reg_i_4_n_0),
        .I3(B_reg_i_5_n_0),
        .I4(B_reg_i_6_n_0),
        .O(\salidaAux_reg[7]_0 ));
  LUT6 #(
    .INIT(64'h0404040404444444)) 
    B_reg_i_10
       (.I0(\salidaAux_reg_n_0_[6] ),
        .I1(\salidaAux_reg[8]_0 ),
        .I2(\salidaAux_reg_n_0_[4] ),
        .I3(\salidaAux_reg_n_0_[7] ),
        .I4(\salidaAux_reg_n_0_[2] ),
        .I5(\salidaAux_reg_n_0_[3] ),
        .O(B_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'h0000000008080800)) 
    B_reg_i_11
       (.I0(Q),
        .I1(selecHorVer_IBUF),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux_reg_n_0_[3] ),
        .I4(\salidaAux_reg_n_0_[4] ),
        .I5(\salidaAux[3]_i_3_n_0 ),
        .O(B_reg_i_11_n_0));
  LUT6 #(
    .INIT(64'h00000000F0100000)) 
    B_reg_i_13
       (.I0(Q),
        .I1(\salidaAux_reg_n_0_[2] ),
        .I2(\salidaAux_reg_n_0_[5] ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(selecHorVer_IBUF),
        .I5(\salidaAux_reg_n_0_[7] ),
        .O(B_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'hAAABAAAAAAFFAAAA)) 
    B_reg_i_2
       (.I0(B_reg_i_1_1),
        .I1(\salidaAux_reg_n_0_[7] ),
        .I2(B_reg_i_8_n_0),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(selecHorVer_IBUF),
        .I5(\salidaAux_reg_n_0_[5] ),
        .O(B_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000010001000100)) 
    B_reg_i_4
       (.I0(\salidaAux_reg_n_0_[6] ),
        .I1(\salidaAux_reg_n_0_[2] ),
        .I2(\salidaAux_reg_n_0_[4] ),
        .I3(selecHorVer_IBUF),
        .I4(\salidaAux_reg_n_0_[7] ),
        .I5(\salidaAux_reg_n_0_[3] ),
        .O(B_reg_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    B_reg_i_5
       (.I0(Q),
        .I1(selecHorVer_IBUF),
        .I2(\salidaAux_reg_n_0_[4] ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(\salidaAux_reg_n_0_[5] ),
        .I5(\salidaAux_reg_n_0_[7] ),
        .O(B_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'hFFFEFEFEFEFEFEFE)) 
    B_reg_i_6
       (.I0(B_reg_i_10_n_0),
        .I1(B_reg_i_11_n_0),
        .I2(B_reg_i_1_0),
        .I3(\salidaAux_reg_n_0_[4] ),
        .I4(\salidaAux_reg_n_0_[3] ),
        .I5(B_reg_i_13_n_0),
        .O(B_reg_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'hE)) 
    B_reg_i_8
       (.I0(\salidaAux_reg_n_0_[3] ),
        .I1(\salidaAux_reg_n_0_[4] ),
        .O(B_reg_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h0001FFFF)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(\salidaAux[3]_i_2_n_0 ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux_reg_n_0_[1] ),
        .I3(\salidaAux_reg_n_0_[0] ),
        .I4(\FSM_sequential_state_reg[2] ),
        .O(\salidaAux_reg[3]_2 ));
  LUT2 #(
    .INIT(4'h2)) 
    G_reg_i_2
       (.I0(selecHorVer_IBUF),
        .I1(Q),
        .O(\salidaAux_reg[8]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    G_reg_i_3
       (.I0(\salidaAux_reg_n_0_[7] ),
        .I1(\salidaAux_reg_n_0_[5] ),
        .I2(\salidaAux_reg_n_0_[6] ),
        .I3(\salidaAux_reg_n_0_[4] ),
        .O(\salidaAux_reg[7]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    R_reg_i_6
       (.I0(\salidaAux_reg_n_0_[6] ),
        .I1(\salidaAux_reg_n_0_[5] ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .O(\salidaAux_reg[6]_0 ));
  LUT6 #(
    .INIT(64'hD000333300000000)) 
    R_reg_i_7
       (.I0(\salidaAux_reg_n_0_[3] ),
        .I1(\salidaAux_reg_n_0_[7] ),
        .I2(\salidaAux_reg_n_0_[5] ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(\salidaAux_reg_n_0_[4] ),
        .I5(\salidaAux_reg[8]_0 ),
        .O(\salidaAux_reg[3]_0 ));
  LUT6 #(
    .INIT(64'h00001FFF00000000)) 
    R_reg_i_8
       (.I0(\salidaAux_reg_n_0_[3] ),
        .I1(\salidaAux_reg_n_0_[4] ),
        .I2(\salidaAux_reg_n_0_[5] ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(\salidaAux_reg_n_0_[7] ),
        .I5(selecHorVer_IBUF),
        .O(\salidaAux_reg[3]_1 ));
  FDRE #(
    .INIT(1'b0)) 
    aumAnt_reg
       (.C(CLK),
        .CE(1'b1),
        .D(aumAnt_reg_1),
        .Q(aumAnt_reg_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h0F07)) 
    \salidaAux[0]_i_1 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux_reg_n_0_[0] ),
        .I3(\salidaAux[3]_i_2_n_0 ),
        .O(salidaAux[0]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0FB0)) 
    \salidaAux[1]_i_1 
       (.I0(\salidaAux[3]_i_2_n_0 ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux_reg_n_0_[1] ),
        .I3(\salidaAux_reg_n_0_[0] ),
        .O(salidaAux[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \salidaAux[2]_i_1 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[2] ),
        .O(salidaAux[2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h62C8CCCC)) 
    \salidaAux[3]_i_1 
       (.I0(\salidaAux_reg_n_0_[0] ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux[3]_i_2_n_0 ),
        .I3(\salidaAux_reg_n_0_[2] ),
        .I4(\salidaAux_reg_n_0_[1] ),
        .O(salidaAux[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFDFFFF)) 
    \salidaAux[3]_i_2 
       (.I0(Q),
        .I1(\salidaAux_reg_n_0_[4] ),
        .I2(\salidaAux_reg_n_0_[2] ),
        .I3(\salidaAux_reg_n_0_[9] ),
        .I4(\salidaAux_reg_n_0_[7] ),
        .I5(\salidaAux[3]_i_3_n_0 ),
        .O(\salidaAux[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \salidaAux[3]_i_3 
       (.I0(\salidaAux_reg_n_0_[5] ),
        .I1(\salidaAux_reg_n_0_[6] ),
        .O(\salidaAux[3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \salidaAux[4]_i_1 
       (.I0(\salidaAux_reg_n_0_[2] ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .I2(\salidaAux_reg_n_0_[1] ),
        .I3(\salidaAux_reg_n_0_[0] ),
        .I4(\salidaAux_reg_n_0_[4] ),
        .O(salidaAux[4]));
  LUT6 #(
    .INIT(64'hABBB0000ABBBFFFF)) 
    \salidaAux[5]_i_1 
       (.I0(\salidaAux[7]_i_2_n_0 ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux_reg_n_0_[6] ),
        .I4(\salidaAux_reg_n_0_[5] ),
        .I5(\salidaAux[9]_i_4__0_n_0 ),
        .O(salidaAux[5]));
  LUT6 #(
    .INIT(64'hABABFFFF00FF0000)) 
    \salidaAux[6]_i_1 
       (.I0(\salidaAux[7]_i_2_n_0 ),
        .I1(\salidaAux_reg_n_0_[0] ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux[9]_i_4__0_n_0 ),
        .I4(\salidaAux_reg_n_0_[5] ),
        .I5(\salidaAux_reg_n_0_[6] ),
        .O(salidaAux[6]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hA3F0F0F0)) 
    \salidaAux[7]_i_1 
       (.I0(\salidaAux[7]_i_2_n_0 ),
        .I1(\salidaAux[9]_i_4__0_n_0 ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux_reg_n_0_[5] ),
        .I4(\salidaAux_reg_n_0_[6] ),
        .O(salidaAux[7]));
  LUT6 #(
    .INIT(64'hF0FFFFFFFFFFFFFB)) 
    \salidaAux[7]_i_2 
       (.I0(\salidaAux_reg_n_0_[9] ),
        .I1(Q),
        .I2(\salidaAux[7]_i_3_n_0 ),
        .I3(\salidaAux_reg_n_0_[4] ),
        .I4(\salidaAux_reg_n_0_[2] ),
        .I5(\salidaAux_reg_n_0_[0] ),
        .O(\salidaAux[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \salidaAux[7]_i_3 
       (.I0(\salidaAux_reg_n_0_[1] ),
        .I1(\salidaAux_reg_n_0_[3] ),
        .O(\salidaAux[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF00FF00CF55CC55)) 
    \salidaAux[8]_i_1 
       (.I0(\salidaAux[9]_i_4__0_n_0 ),
        .I1(\salidaAux[8]_i_2_n_0 ),
        .I2(\salidaAux_reg_n_0_[0] ),
        .I3(Q),
        .I4(\salidaAux[8]_i_3_n_0 ),
        .I5(\salidaAux_reg[6]_0 ),
        .O(salidaAux[8]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h2AFFFFFF)) 
    \salidaAux[8]_i_2 
       (.I0(\salidaAux_reg_n_0_[0] ),
        .I1(\salidaAux_reg_n_0_[2] ),
        .I2(\salidaAux_reg_n_0_[4] ),
        .I3(\salidaAux_reg_n_0_[3] ),
        .I4(\salidaAux_reg_n_0_[1] ),
        .O(\salidaAux[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \salidaAux[8]_i_3 
       (.I0(\salidaAux_reg_n_0_[4] ),
        .I1(\salidaAux_reg_n_0_[2] ),
        .I2(\salidaAux_reg_n_0_[9] ),
        .O(\salidaAux[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    \salidaAux[9]_i_3 
       (.I0(Q),
        .I1(\salidaAux[9]_i_4__0_n_0 ),
        .I2(\salidaAux_reg_n_0_[7] ),
        .I3(\salidaAux_reg_n_0_[5] ),
        .I4(\salidaAux_reg_n_0_[6] ),
        .I5(\salidaAux_reg_n_0_[9] ),
        .O(salidaAux[9]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \salidaAux[9]_i_4__0 
       (.I0(\salidaAux_reg_n_0_[0] ),
        .I1(\salidaAux_reg_n_0_[1] ),
        .I2(\salidaAux_reg_n_0_[3] ),
        .I3(\salidaAux_reg_n_0_[4] ),
        .I4(\salidaAux_reg_n_0_[2] ),
        .O(\salidaAux[9]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[0]),
        .Q(\salidaAux_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[1]),
        .Q(\salidaAux_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[2]),
        .Q(\salidaAux_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[3]),
        .Q(\salidaAux_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[4]),
        .Q(\salidaAux_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[5]),
        .Q(\salidaAux_reg_n_0_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[6]),
        .Q(\salidaAux_reg_n_0_[6] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[7]),
        .Q(\salidaAux_reg_n_0_[7] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[8]),
        .Q(Q),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \salidaAux_reg[9] 
       (.C(CLK),
        .CE(E),
        .D(salidaAux[9]),
        .Q(\salidaAux_reg_n_0_[9] ),
        .R(SR));
endmodule

module generadorBandas
   (Rout_OBUF,
    Gout_OBUF,
    Bout_OBUF,
    Rout,
    Bout,
    aumento,
    Gout,
    Bout_0);
  output Rout_OBUF;
  output Gout_OBUF;
  output Bout_OBUF;
  input Rout;
  input Bout;
  input aumento;
  input Gout;
  input Bout_0;

  wire Bout;
  wire Bout_0;
  wire Bout_OBUF;
  wire Gout;
  wire Gout_OBUF;
  wire Rout;
  wire Rout_OBUF;
  wire aumento;

  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    B_reg
       (.CLR(aumento),
        .D(Bout_0),
        .G(Bout),
        .GE(1'b1),
        .Q(Bout_OBUF));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    G_reg
       (.CLR(aumento),
        .D(Gout),
        .G(Bout),
        .GE(1'b1),
        .Q(Gout_OBUF));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    R_reg
       (.CLR(aumento),
        .D(Rout),
        .G(Bout),
        .GE(1'b1),
        .Q(Rout_OBUF));
endmodule

module generadorSenales
   (E,
    VSync_OBUF,
    auxEstados);
  output [0:0]E;
  output VSync_OBUF;
  input [1:0]auxEstados;

  wire [0:0]E;
  wire VSync_OBUF;
  wire [1:0]auxEstados;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    VSync_OBUF_inst_i_1
       (.I0(auxEstados[0]),
        .I1(auxEstados[1]),
        .O(VSync_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \salidaAux[9]_i_2 
       (.I0(auxEstados[0]),
        .I1(auxEstados[1]),
        .O(E));
endmodule

module maqEstVGA
   (\FSM_sequential_state_reg[0]_0 ,
    \E_reg[1]_0 ,
    auxEstados,
    SR,
    \E_reg[1]_1 ,
    aumento,
    E,
    HSync_OBUF,
    auxClk25Mhz_BUFG,
    reset_IBUF,
    aumAnt_reg,
    \FSM_sequential_state_reg[2]_0 );
  output [0:0]\FSM_sequential_state_reg[0]_0 ;
  output \E_reg[1]_0 ;
  output [1:0]auxEstados;
  output [0:0]SR;
  output [0:0]\E_reg[1]_1 ;
  output aumento;
  output [0:0]E;
  output HSync_OBUF;
  input auxClk25Mhz_BUFG;
  input reset_IBUF;
  input aumAnt_reg;
  input \FSM_sequential_state_reg[2]_0 ;

  wire [0:0]E;
  wire \E[0]_i_1_n_0 ;
  wire \E[1]_i_1_n_0 ;
  wire \E_reg[1]_0 ;
  wire [0:0]\E_reg[1]_1 ;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[1]_i_2_n_0 ;
  wire \FSM_sequential_state[1]_i_3_n_0 ;
  wire \FSM_sequential_state[1]_i_4_n_0 ;
  wire \FSM_sequential_state[1]_i_5_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_3_n_0 ;
  wire \FSM_sequential_state[2]_i_4_n_0 ;
  wire \FSM_sequential_state[2]_i_5_n_0 ;
  wire [0:0]\FSM_sequential_state_reg[0]_0 ;
  wire \FSM_sequential_state_reg[2]_0 ;
  wire HSync_OBUF;
  wire [0:0]SR;
  wire aumAnt_reg;
  wire aumento;
  wire auxClk25Mhz_BUFG;
  wire [1:0]auxEstados;
  wire \contador[0]_i_1_n_0 ;
  wire \contador[0]_i_3_n_0 ;
  wire [14:0]contador_reg;
  wire \contador_reg[0]_i_2_n_0 ;
  wire \contador_reg[0]_i_2_n_1 ;
  wire \contador_reg[0]_i_2_n_2 ;
  wire \contador_reg[0]_i_2_n_3 ;
  wire \contador_reg[0]_i_2_n_4 ;
  wire \contador_reg[0]_i_2_n_5 ;
  wire \contador_reg[0]_i_2_n_6 ;
  wire \contador_reg[0]_i_2_n_7 ;
  wire \contador_reg[12]_i_1_n_2 ;
  wire \contador_reg[12]_i_1_n_3 ;
  wire \contador_reg[12]_i_1_n_5 ;
  wire \contador_reg[12]_i_1_n_6 ;
  wire \contador_reg[12]_i_1_n_7 ;
  wire \contador_reg[4]_i_1_n_0 ;
  wire \contador_reg[4]_i_1_n_1 ;
  wire \contador_reg[4]_i_1_n_2 ;
  wire \contador_reg[4]_i_1_n_3 ;
  wire \contador_reg[4]_i_1_n_4 ;
  wire \contador_reg[4]_i_1_n_5 ;
  wire \contador_reg[4]_i_1_n_6 ;
  wire \contador_reg[4]_i_1_n_7 ;
  wire \contador_reg[8]_i_1_n_0 ;
  wire \contador_reg[8]_i_1_n_1 ;
  wire \contador_reg[8]_i_1_n_2 ;
  wire \contador_reg[8]_i_1_n_3 ;
  wire \contador_reg[8]_i_1_n_4 ;
  wire \contador_reg[8]_i_1_n_5 ;
  wire \contador_reg[8]_i_1_n_6 ;
  wire \contador_reg[8]_i_1_n_7 ;
  wire resetCont1;
  wire resetCont1__0;
  wire resetCont1_reg_i_10_n_0;
  wire resetCont1_reg_i_11_n_0;
  wire resetCont1_reg_i_12_n_0;
  wire resetCont1_reg_i_1_n_0;
  wire resetCont1_reg_i_3_n_0;
  wire resetCont1_reg_i_4_n_0;
  wire resetCont1_reg_i_5_n_0;
  wire resetCont1_reg_i_6_n_0;
  wire resetCont1_reg_i_7_n_0;
  wire resetCont1_reg_i_8_n_0;
  wire resetCont1_reg_i_9_n_0;
  wire reset_IBUF;
  wire [2:1]state;
  wire [3:2]\NLW_contador_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_contador_reg[12]_i_1_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h00AE)) 
    \E[0]_i_1 
       (.I0(\FSM_sequential_state_reg[0]_0 ),
        .I1(state[2]),
        .I2(state[1]),
        .I3(reset_IBUF),
        .O(\E[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0032)) 
    \E[1]_i_1 
       (.I0(state[2]),
        .I1(\FSM_sequential_state_reg[0]_0 ),
        .I2(state[1]),
        .I3(reset_IBUF),
        .O(\E[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \E_reg[0] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\E[0]_i_1_n_0 ),
        .Q(auxEstados[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \E_reg[1] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\E[1]_i_1_n_0 ),
        .Q(auxEstados[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00006266)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(\FSM_sequential_state_reg[0]_0 ),
        .I1(resetCont1_reg_i_1_n_0),
        .I2(\FSM_sequential_state[1]_i_2_n_0 ),
        .I3(state[1]),
        .I4(reset_IBUF),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00006A62)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state[1]),
        .I1(resetCont1_reg_i_1_n_0),
        .I2(\FSM_sequential_state_reg[0]_0 ),
        .I3(\FSM_sequential_state[1]_i_2_n_0 ),
        .I4(reset_IBUF),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h80000000FFFFFFFF)) 
    \FSM_sequential_state[1]_i_2 
       (.I0(\FSM_sequential_state[1]_i_3_n_0 ),
        .I1(contador_reg[0]),
        .I2(contador_reg[1]),
        .I3(contador_reg[2]),
        .I4(\FSM_sequential_state[1]_i_4_n_0 ),
        .I5(state[2]),
        .O(\FSM_sequential_state[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00010000)) 
    \FSM_sequential_state[1]_i_3 
       (.I0(contador_reg[11]),
        .I1(contador_reg[12]),
        .I2(contador_reg[13]),
        .I3(contador_reg[14]),
        .I4(\FSM_sequential_state[1]_i_5_n_0 ),
        .O(\FSM_sequential_state[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_state[1]_i_4 
       (.I0(contador_reg[5]),
        .I1(contador_reg[6]),
        .I2(contador_reg[4]),
        .I3(contador_reg[3]),
        .O(\FSM_sequential_state[1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_state[1]_i_5 
       (.I0(contador_reg[10]),
        .I1(contador_reg[9]),
        .I2(contador_reg[8]),
        .I3(contador_reg[7]),
        .O(\FSM_sequential_state[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEEE2A22)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(state[2]),
        .I1(resetCont1_reg_i_1_n_0),
        .I2(state[1]),
        .I3(\FSM_sequential_state_reg[2]_0 ),
        .I4(\FSM_sequential_state[2]_i_3_n_0 ),
        .I5(reset_IBUF),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF08080808080808)) 
    \FSM_sequential_state[2]_i_3 
       (.I0(state[1]),
        .I1(\FSM_sequential_state_reg[0]_0 ),
        .I2(state[2]),
        .I3(\FSM_sequential_state[2]_i_4_n_0 ),
        .I4(\FSM_sequential_state[2]_i_5_n_0 ),
        .I5(resetCont1_reg_i_9_n_0),
        .O(\FSM_sequential_state[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \FSM_sequential_state[2]_i_4 
       (.I0(contador_reg[3]),
        .I1(contador_reg[4]),
        .I2(contador_reg[1]),
        .I3(contador_reg[2]),
        .I4(contador_reg[5]),
        .I5(contador_reg[6]),
        .O(\FSM_sequential_state[2]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \FSM_sequential_state[2]_i_5 
       (.I0(\FSM_sequential_state_reg[0]_0 ),
        .I1(state[2]),
        .I2(contador_reg[0]),
        .I3(contador_reg[13]),
        .I4(contador_reg[8]),
        .O(\FSM_sequential_state[2]_i_5_n_0 ));
  (* FSM_ENCODED_STATES = "st2_vbackporch:001,st4_hbackporch:011,st5_vidact:100,st6_hfrontporch:101,st7_vfrontporcha:110,st3_hsyncpulse:010,st1_vsyncpulse:000,st8_vfrontporchb:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(\FSM_sequential_state_reg[0]_0 ),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "st2_vbackporch:001,st4_hbackporch:011,st5_vidact:100,st6_hfrontporch:101,st7_vfrontporcha:110,st3_hsyncpulse:010,st1_vsyncpulse:000,st8_vfrontporchb:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "st2_vbackporch:001,st4_hbackporch:011,st5_vidact:100,st6_hfrontporch:101,st7_vfrontporcha:110,st3_hsyncpulse:010,st1_vsyncpulse:000,st8_vfrontporchb:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[2] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'hB)) 
    HSync_OBUF_inst_i_1
       (.I0(auxEstados[0]),
        .I1(auxEstados[1]),
        .O(HSync_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h7)) 
    R_reg_i_3
       (.I0(auxEstados[1]),
        .I1(auxEstados[0]),
        .O(aumento));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h76)) 
    aumAnt_i_1
       (.I0(auxEstados[1]),
        .I1(auxEstados[0]),
        .I2(aumAnt_reg),
        .O(\E_reg[1]_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \contador[0]_i_1 
       (.I0(reset_IBUF),
        .I1(resetCont1),
        .O(\contador[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \contador[0]_i_3 
       (.I0(contador_reg[0]),
        .O(\contador[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[0] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[0]_i_2_n_7 ),
        .Q(contador_reg[0]),
        .R(\contador[0]_i_1_n_0 ));
  CARRY4 \contador_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\contador_reg[0]_i_2_n_0 ,\contador_reg[0]_i_2_n_1 ,\contador_reg[0]_i_2_n_2 ,\contador_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\contador_reg[0]_i_2_n_4 ,\contador_reg[0]_i_2_n_5 ,\contador_reg[0]_i_2_n_6 ,\contador_reg[0]_i_2_n_7 }),
        .S({contador_reg[3:1],\contador[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[10] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[8]_i_1_n_5 ),
        .Q(contador_reg[10]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[11] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[8]_i_1_n_4 ),
        .Q(contador_reg[11]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[12] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[12]_i_1_n_7 ),
        .Q(contador_reg[12]),
        .R(\contador[0]_i_1_n_0 ));
  CARRY4 \contador_reg[12]_i_1 
       (.CI(\contador_reg[8]_i_1_n_0 ),
        .CO({\NLW_contador_reg[12]_i_1_CO_UNCONNECTED [3:2],\contador_reg[12]_i_1_n_2 ,\contador_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_contador_reg[12]_i_1_O_UNCONNECTED [3],\contador_reg[12]_i_1_n_5 ,\contador_reg[12]_i_1_n_6 ,\contador_reg[12]_i_1_n_7 }),
        .S({1'b0,contador_reg[14:12]}));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[13] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[12]_i_1_n_6 ),
        .Q(contador_reg[13]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[14] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[12]_i_1_n_5 ),
        .Q(contador_reg[14]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[1] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[0]_i_2_n_6 ),
        .Q(contador_reg[1]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[2] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[0]_i_2_n_5 ),
        .Q(contador_reg[2]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[3] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[0]_i_2_n_4 ),
        .Q(contador_reg[3]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[4] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[4]_i_1_n_7 ),
        .Q(contador_reg[4]),
        .R(\contador[0]_i_1_n_0 ));
  CARRY4 \contador_reg[4]_i_1 
       (.CI(\contador_reg[0]_i_2_n_0 ),
        .CO({\contador_reg[4]_i_1_n_0 ,\contador_reg[4]_i_1_n_1 ,\contador_reg[4]_i_1_n_2 ,\contador_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\contador_reg[4]_i_1_n_4 ,\contador_reg[4]_i_1_n_5 ,\contador_reg[4]_i_1_n_6 ,\contador_reg[4]_i_1_n_7 }),
        .S(contador_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[5] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[4]_i_1_n_6 ),
        .Q(contador_reg[5]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[6] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[4]_i_1_n_5 ),
        .Q(contador_reg[6]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[7] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[4]_i_1_n_4 ),
        .Q(contador_reg[7]),
        .R(\contador[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[8] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[8]_i_1_n_7 ),
        .Q(contador_reg[8]),
        .R(\contador[0]_i_1_n_0 ));
  CARRY4 \contador_reg[8]_i_1 
       (.CI(\contador_reg[4]_i_1_n_0 ),
        .CO({\contador_reg[8]_i_1_n_0 ,\contador_reg[8]_i_1_n_1 ,\contador_reg[8]_i_1_n_2 ,\contador_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\contador_reg[8]_i_1_n_4 ,\contador_reg[8]_i_1_n_5 ,\contador_reg[8]_i_1_n_6 ,\contador_reg[8]_i_1_n_7 }),
        .S(contador_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \contador_reg[9] 
       (.C(auxClk25Mhz_BUFG),
        .CE(1'b1),
        .D(\contador_reg[8]_i_1_n_6 ),
        .Q(contador_reg[9]),
        .R(\contador[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    resetCont1_reg
       (.CLR(1'b0),
        .D(resetCont1_reg_i_1_n_0),
        .G(resetCont1__0),
        .GE(1'b1),
        .Q(resetCont1));
  LUT6 #(
    .INIT(64'hAAAAA800A800A800)) 
    resetCont1_reg_i_1
       (.I0(resetCont1_reg_i_3_n_0),
        .I1(resetCont1_reg_i_4_n_0),
        .I2(resetCont1_reg_i_5_n_0),
        .I3(\FSM_sequential_state_reg[0]_0 ),
        .I4(resetCont1_reg_i_6_n_0),
        .I5(resetCont1_reg_i_7_n_0),
        .O(resetCont1_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    resetCont1_reg_i_10
       (.I0(contador_reg[9]),
        .I1(contador_reg[11]),
        .I2(contador_reg[4]),
        .I3(contador_reg[7]),
        .I4(contador_reg[5]),
        .I5(contador_reg[12]),
        .O(resetCont1_reg_i_10_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    resetCont1_reg_i_11
       (.I0(contador_reg[7]),
        .I1(contador_reg[4]),
        .I2(contador_reg[11]),
        .I3(contador_reg[9]),
        .O(resetCont1_reg_i_11_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    resetCont1_reg_i_12
       (.I0(contador_reg[14]),
        .I1(contador_reg[10]),
        .I2(contador_reg[6]),
        .I3(contador_reg[12]),
        .O(resetCont1_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00020000)) 
    resetCont1_reg_i_2
       (.I0(resetCont1_reg_i_8_n_0),
        .I1(contador_reg[0]),
        .I2(contador_reg[8]),
        .I3(contador_reg[13]),
        .I4(resetCont1_reg_i_9_n_0),
        .I5(resetCont1_reg_i_1_n_0),
        .O(resetCont1__0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    resetCont1_reg_i_3
       (.I0(contador_reg[2]),
        .I1(contador_reg[3]),
        .I2(contador_reg[0]),
        .I3(contador_reg[1]),
        .I4(contador_reg[13]),
        .I5(contador_reg[8]),
        .O(resetCont1_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000800000020000)) 
    resetCont1_reg_i_4
       (.I0(resetCont1_reg_i_10_n_0),
        .I1(state[1]),
        .I2(state[2]),
        .I3(contador_reg[6]),
        .I4(contador_reg[14]),
        .I5(contador_reg[10]),
        .O(resetCont1_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'h00800800)) 
    resetCont1_reg_i_5
       (.I0(resetCont1_reg_i_11_n_0),
        .I1(resetCont1_reg_i_12_n_0),
        .I2(contador_reg[5]),
        .I3(state[2]),
        .I4(state[1]),
        .O(resetCont1_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    resetCont1_reg_i_6
       (.I0(contador_reg[7]),
        .I1(contador_reg[11]),
        .I2(contador_reg[4]),
        .I3(\FSM_sequential_state_reg[0]_0 ),
        .I4(contador_reg[14]),
        .I5(contador_reg[12]),
        .O(resetCont1_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'h0000000C18000000)) 
    resetCont1_reg_i_7
       (.I0(state[2]),
        .I1(contador_reg[6]),
        .I2(contador_reg[10]),
        .I3(contador_reg[9]),
        .I4(contador_reg[5]),
        .I5(state[1]),
        .O(resetCont1_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    resetCont1_reg_i_8
       (.I0(contador_reg[3]),
        .I1(contador_reg[4]),
        .I2(contador_reg[1]),
        .I3(contador_reg[2]),
        .I4(contador_reg[6]),
        .I5(contador_reg[5]),
        .O(resetCont1_reg_i_8_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    resetCont1_reg_i_9
       (.I0(contador_reg[10]),
        .I1(contador_reg[11]),
        .I2(contador_reg[7]),
        .I3(contador_reg[9]),
        .I4(contador_reg[14]),
        .I5(contador_reg[12]),
        .O(resetCont1_reg_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \salidaAux[9]_i_1 
       (.I0(auxEstados[1]),
        .I1(auxEstados[0]),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \salidaAux[9]_i_1__0 
       (.I0(auxEstados[1]),
        .I1(auxEstados[0]),
        .O(\E_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h07)) 
    \salidaAux[9]_i_2 
       (.I0(auxEstados[1]),
        .I1(auxEstados[0]),
        .I2(aumAnt_reg),
        .O(E));
endmodule

(* NotValidForBitStream *)
module topDriverVGA
   (clk,
    reset,
    selecHorVer,
    VSync,
    HSync,
    Rout,
    Gout,
    Bout);
  input clk;
  input reset;
  input selecHorVer;
  output VSync;
  output HSync;
  output Rout;
  output Gout;
  output Bout;

  wire Bout;
  wire Bout_OBUF;
  wire Gout;
  wire Gout_OBUF;
  wire HSync;
  wire HSync_OBUF;
  wire Rout;
  wire Rout_OBUF;
  wire VSync;
  wire VSync_OBUF;
  wire aumento;
  wire auxClk25Mhz;
  wire auxClk25Mhz_BUFG;
  wire [1:0]auxEstados;
  wire auxVidAct;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire maqEst_n_1;
  wire maqEst_n_7;
  wire reset;
  wire resetPxx;
  wire resetPxy;
  wire reset_IBUF;
  wire selecHorVer;
  wire selecHorVer_IBUF;
  wire selecPx_n_0;
  wire selecPx_n_1;
  wire selecPx_n_2;
  wire selecPx_n_3;
  wire selecPx_n_4;
  wire selecPx_n_5;
  wire [0:0]state;

initial begin
 $sdf_annotate("tb_top_time_synth.sdf",,,,"tool_control");
end
  OBUF Bout_OBUF_inst
       (.I(Bout_OBUF),
        .O(Bout));
  OBUF Gout_OBUF_inst
       (.I(Gout_OBUF),
        .O(Gout));
  OBUF HSync_OBUF_inst
       (.I(HSync_OBUF),
        .O(HSync));
  OBUF Rout_OBUF_inst
       (.I(Rout_OBUF),
        .O(Rout));
  OBUF VSync_OBUF_inst
       (.I(VSync_OBUF),
        .O(VSync));
  BUFG auxClk25Mhz_BUFG_inst
       (.I(auxClk25Mhz),
        .O(auxClk25Mhz_BUFG));
  clockGen clk125Ma25M
       (.auxClk25Mhz(auxClk25Mhz),
        .clk_IBUF_BUFG(clk_IBUF_BUFG));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  generadorBandas genBand
       (.Bout(selecPx_n_5),
        .Bout_0(selecPx_n_1),
        .Bout_OBUF(Bout_OBUF),
        .Gout(selecPx_n_3),
        .Gout_OBUF(Gout_OBUF),
        .Rout(selecPx_n_2),
        .Rout_OBUF(Rout_OBUF),
        .aumento(aumento));
  generadorSenales genSel
       (.E(auxVidAct),
        .VSync_OBUF(VSync_OBUF),
        .auxEstados(auxEstados));
  maqEstVGA maqEst
       (.E(maqEst_n_7),
        .\E_reg[1]_0 (maqEst_n_1),
        .\E_reg[1]_1 (resetPxy),
        .\FSM_sequential_state_reg[0]_0 (state),
        .\FSM_sequential_state_reg[2]_0 (selecPx_n_4),
        .HSync_OBUF(HSync_OBUF),
        .SR(resetPxx),
        .aumAnt_reg(selecPx_n_0),
        .aumento(aumento),
        .auxClk25Mhz_BUFG(auxClk25Mhz_BUFG),
        .auxEstados(auxEstados),
        .reset_IBUF(reset_IBUF));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  IBUF selecHorVer_IBUF_inst
       (.I(selecHorVer),
        .O(selecHorVer_IBUF));
  SeleccionadorPixel selecPx
       (.CLK(auxClk25Mhz_BUFG),
        .E(maqEst_n_7),
        .\FSM_sequential_state_reg[2] (state),
        .SR(resetPxy),
        .aumAnt_reg(selecPx_n_0),
        .aumAnt_reg_0(maqEst_n_1),
        .\salidaAux_reg[0] (resetPxx),
        .\salidaAux_reg[0]_0 (auxVidAct),
        .\salidaAux_reg[3] (selecPx_n_4),
        .\salidaAux_reg[6] (selecPx_n_2),
        .\salidaAux_reg[7] (selecPx_n_1),
        .\salidaAux_reg[8] (selecPx_n_3),
        .\salidaAux_reg[8]_0 (selecPx_n_5),
        .selecHorVer_IBUF(selecHorVer_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
