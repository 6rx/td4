#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.2 (64-bit)
#
# Filename    : simulate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for simulating the design by launching the simulator
#
# Generated by Vivado on Tue Apr 28 15:27:07 -03 2020
# SW Build 2708876 on Wed Nov  6 21:39:14 MST 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: simulate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xsim tb_top_time_synth -key {Post-Synthesis:sim_1:Timing:tb_top} -tclbatch tb_top.tcl -view /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.sim/tb_top_behav.wcfg -log simulate.log"
xsim tb_top_time_synth -key {Post-Synthesis:sim_1:Timing:tb_top} -tclbatch tb_top.tcl -view /home/nacho/repos/sexto/td4/tp2gab/td4_tp2_vga.sim/tb_top_behav.wcfg -log simulate.log

