// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue May  5 16:57:20 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/tp1/resolucion/vivado/td4_tp1_encoder.sim/sim_1/impl/timing/xsim/enc_dec_tb_time_impl.v
// Design      : encoder_decode
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module clk_divisor
   (CLK,
    clk_15k_IBUF_BUFG,
    reset_IBUF);
  output CLK;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;

  wire CLK;
  wire clk_15k_IBUF_BUFG;
  wire reset_IBUF;
  wire \s_counter[0]_i_1__0_n_0 ;
  wire \s_counter[1]_i_1__0_n_0 ;
  wire \s_counter[2]_i_1_n_0 ;
  wire [2:1]s_counter_reg;
  wire \s_counter_reg_n_0_[0] ;
  wire s_out_clk_i_1_n_0;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h15)) 
    \s_counter[0]_i_1__0 
       (.I0(\s_counter_reg_n_0_[0] ),
        .I1(s_counter_reg[2]),
        .I2(s_counter_reg[1]),
        .O(\s_counter[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h1A)) 
    \s_counter[1]_i_1__0 
       (.I0(\s_counter_reg_n_0_[0] ),
        .I1(s_counter_reg[2]),
        .I2(s_counter_reg[1]),
        .O(\s_counter[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h2C)) 
    \s_counter[2]_i_1 
       (.I0(\s_counter_reg_n_0_[0] ),
        .I1(s_counter_reg[2]),
        .I2(s_counter_reg[1]),
        .O(\s_counter[2]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[0]_i_1__0_n_0 ),
        .Q(\s_counter_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[1]_i_1__0_n_0 ),
        .Q(s_counter_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[2]_i_1_n_0 ),
        .Q(s_counter_reg[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    s_out_clk_i_1
       (.I0(s_counter_reg[2]),
        .I1(s_counter_reg[1]),
        .I2(CLK),
        .O(s_out_clk_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    s_out_clk_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_out_clk_i_1_n_0),
        .Q(CLK));
endmodule

module count_decider
   (last_enable,
    s_increment_0,
    s_decrement_0,
    increment_reg_0,
    s_count_enable,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    increment_reg_1,
    E,
    increment1,
    decrement_reg_0);
  output last_enable;
  output s_increment_0;
  output s_decrement_0;
  output [0:0]increment_reg_0;
  input s_count_enable;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input increment_reg_1;
  input [0:0]E;
  input increment1;
  input decrement_reg_0;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire decrement_reg_0;
  wire increment1;
  wire [0:0]increment_reg_0;
  wire increment_reg_1;
  wire last_enable;
  wire reset_IBUF;
  wire s_count_enable;
  wire s_decrement_0;
  wire s_increment_0;

  FDRE #(
    .INIT(1'b0)) 
    decrement_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(decrement_reg_0),
        .Q(s_decrement_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    increment_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .D(increment1),
        .Q(s_increment_0),
        .R(increment_reg_1));
  FDCE #(
    .INIT(1'b0)) 
    last_enable_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_count_enable),
        .Q(last_enable));
  LUT2 #(
    .INIT(4'hE)) 
    \s_count[3]_i_1 
       (.I0(s_increment_0),
        .I1(s_decrement_0),
        .O(increment_reg_0));
endmodule

(* ORIG_REF_NAME = "count_decider" *) 
module count_decider_0
   (last_enable,
    s_increment_1,
    decrement_reg_0,
    s_overflow_0,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    increment_reg_0,
    E,
    increment1,
    s_up_down);
  output last_enable;
  output s_increment_1;
  output [0:0]decrement_reg_0;
  input s_overflow_0;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input increment_reg_0;
  input [0:0]E;
  input increment1;
  input s_up_down;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire decrement_i_1__0_n_0;
  wire [0:0]decrement_reg_0;
  wire increment1;
  wire increment_reg_0;
  wire last_enable;
  wire reset_IBUF;
  wire s_decrement_1;
  wire s_increment_1;
  wire s_overflow_0;
  wire s_up_down;

  LUT5 #(
    .INIT(32'hCCCC0050)) 
    decrement_i_1__0
       (.I0(s_up_down),
        .I1(s_decrement_1),
        .I2(s_overflow_0),
        .I3(last_enable),
        .I4(reset_IBUF),
        .O(decrement_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decrement_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(decrement_i_1__0_n_0),
        .Q(s_decrement_1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    increment_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .D(increment1),
        .Q(s_increment_1),
        .R(increment_reg_0));
  FDCE #(
    .INIT(1'b0)) 
    last_enable_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_overflow_0),
        .Q(last_enable));
  LUT2 #(
    .INIT(4'hE)) 
    \s_count[3]_i_1__0 
       (.I0(s_decrement_1),
        .I1(s_increment_1),
        .O(decrement_reg_0));
endmodule

(* ORIG_REF_NAME = "count_decider" *) 
module count_decider_1
   (last_enable,
    s_increment_2,
    decrement_reg_0,
    s_overflow_1,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    increment_reg_0,
    E,
    increment1,
    s_up_down);
  output last_enable;
  output s_increment_2;
  output [0:0]decrement_reg_0;
  input s_overflow_1;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input increment_reg_0;
  input [0:0]E;
  input increment1;
  input s_up_down;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire decrement_i_1__1_n_0;
  wire [0:0]decrement_reg_0;
  wire increment1;
  wire increment_reg_0;
  wire last_enable;
  wire reset_IBUF;
  wire s_decrement_2;
  wire s_increment_2;
  wire s_overflow_1;
  wire s_up_down;

  LUT5 #(
    .INIT(32'hCCCC0050)) 
    decrement_i_1__1
       (.I0(s_up_down),
        .I1(s_decrement_2),
        .I2(s_overflow_1),
        .I3(last_enable),
        .I4(reset_IBUF),
        .O(decrement_i_1__1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decrement_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(decrement_i_1__1_n_0),
        .Q(s_decrement_2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    increment_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .D(increment1),
        .Q(s_increment_2),
        .R(increment_reg_0));
  FDCE #(
    .INIT(1'b0)) 
    last_enable_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_overflow_1),
        .Q(last_enable));
  LUT2 #(
    .INIT(4'hE)) 
    \s_count[3]_i_1__1 
       (.I0(s_decrement_2),
        .I1(s_increment_2),
        .O(decrement_reg_0));
endmodule

(* ORIG_REF_NAME = "count_decider" *) 
module count_decider_2
   (last_enable,
    increment_reg_0,
    s_overflow_2,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    increment_reg_1,
    E,
    increment1,
    s_up_down,
    \s_count_reg[0] );
  output last_enable;
  output increment_reg_0;
  input s_overflow_2;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input increment_reg_1;
  input [0:0]E;
  input increment1;
  input s_up_down;
  input \s_count_reg[0] ;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire decrement_i_1__2_n_0;
  wire increment1;
  wire increment_reg_0;
  wire increment_reg_1;
  wire last_enable;
  wire reset_IBUF;
  wire \s_count_reg[0] ;
  wire s_decrement_3;
  wire s_increment_3;
  wire s_overflow_2;
  wire s_up_down;

  LUT5 #(
    .INIT(32'hCCCC0050)) 
    decrement_i_1__2
       (.I0(s_up_down),
        .I1(s_decrement_3),
        .I2(s_overflow_2),
        .I3(last_enable),
        .I4(reset_IBUF),
        .O(decrement_i_1__2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decrement_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(decrement_i_1__2_n_0),
        .Q(s_decrement_3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    increment_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .D(increment1),
        .Q(s_increment_3),
        .R(increment_reg_1));
  FDCE #(
    .INIT(1'b0)) 
    last_enable_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_overflow_2),
        .Q(last_enable));
  LUT3 #(
    .INIT(8'h1E)) 
    \s_count[0]_i_1__2 
       (.I0(s_increment_3),
        .I1(s_decrement_3),
        .I2(\s_count_reg[0] ),
        .O(increment_reg_0));
endmodule

module counter_7seg
   (s_overflow_0,
    increment1,
    \s_count_reg[3] ,
    \s_count_reg[3]_0 ,
    \s_count_reg[3]_1 ,
    \s_count_reg[3]_2 ,
    \s_count_reg[3]_3 ,
    \s_count_reg[3]_4 ,
    \s_count_reg[3]_5 ,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    last_enable,
    s_increment_0);
  output s_overflow_0;
  output increment1;
  output \s_count_reg[3] ;
  output \s_count_reg[3]_0 ;
  output \s_count_reg[3]_1 ;
  output \s_count_reg[3]_2 ;
  output \s_count_reg[3]_3 ;
  output \s_count_reg[3]_4 ;
  output \s_count_reg[3]_5 ;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input last_enable;
  input s_increment_0;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire increment1;
  wire last_enable;
  wire reset_IBUF;
  wire \s_count_reg[3] ;
  wire \s_count_reg[3]_0 ;
  wire \s_count_reg[3]_1 ;
  wire \s_count_reg[3]_2 ;
  wire \s_count_reg[3]_3 ;
  wire \s_count_reg[3]_4 ;
  wire \s_count_reg[3]_5 ;
  wire s_increment_0;
  wire s_overflow_0;

  decimal_counter_6 counter
       (.E(E),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1),
        .last_enable(last_enable),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[3]_0 (\s_count_reg[3] ),
        .\s_count_reg[3]_1 (\s_count_reg[3]_0 ),
        .\s_count_reg[3]_2 (\s_count_reg[3]_1 ),
        .\s_count_reg[3]_3 (\s_count_reg[3]_2 ),
        .\s_count_reg[3]_4 (\s_count_reg[3]_3 ),
        .\s_count_reg[3]_5 (\s_count_reg[3]_4 ),
        .\s_count_reg[3]_6 (\s_count_reg[3]_5 ),
        .s_increment_0(s_increment_0),
        .s_overflow_0(s_overflow_0));
endmodule

(* ORIG_REF_NAME = "counter_7seg" *) 
module counter_7seg_3
   (s_overflow_1,
    increment1,
    \s_count_reg[2] ,
    \s_count_reg[2]_0 ,
    \s_count_reg[0] ,
    \s_counter_reg[1] ,
    \s_count_reg[0]_0 ,
    \s_count_reg[0]_1 ,
    \s_count_reg[0]_2 ,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    last_enable,
    Q,
    s_increment_1,
    \segments_out_reg[2] );
  output s_overflow_1;
  output increment1;
  output \s_count_reg[2] ;
  output \s_count_reg[2]_0 ;
  output \s_count_reg[0] ;
  output \s_counter_reg[1] ;
  output \s_count_reg[0]_0 ;
  output \s_count_reg[0]_1 ;
  output \s_count_reg[0]_2 ;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input last_enable;
  input [0:0]Q;
  input s_increment_1;
  input \segments_out_reg[2] ;

  wire [0:0]E;
  wire [0:0]Q;
  wire clk_15k_IBUF_BUFG;
  wire increment1;
  wire last_enable;
  wire reset_IBUF;
  wire \s_count_reg[0] ;
  wire \s_count_reg[0]_0 ;
  wire \s_count_reg[0]_1 ;
  wire \s_count_reg[0]_2 ;
  wire \s_count_reg[2] ;
  wire \s_count_reg[2]_0 ;
  wire \s_counter_reg[1] ;
  wire s_increment_1;
  wire s_overflow_1;
  wire \segments_out_reg[2] ;

  decimal_counter_5 counter
       (.E(E),
        .Q(Q),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1),
        .last_enable(last_enable),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[0]_0 (\s_count_reg[0] ),
        .\s_count_reg[0]_1 (\s_count_reg[0]_0 ),
        .\s_count_reg[0]_2 (\s_count_reg[0]_1 ),
        .\s_count_reg[0]_3 (\s_count_reg[0]_2 ),
        .\s_count_reg[2]_0 (\s_count_reg[2] ),
        .\s_count_reg[2]_1 (\s_count_reg[2]_0 ),
        .\s_counter_reg[1] (\s_counter_reg[1] ),
        .s_increment_1(s_increment_1),
        .s_overflow_1(s_overflow_1),
        .\segments_out_reg[2] (\segments_out_reg[2] ));
endmodule

(* ORIG_REF_NAME = "counter_7seg" *) 
module counter_7seg_4
   (s_overflow_2,
    increment1,
    \s_count_reg[3] ,
    \s_count_reg[3]_0 ,
    \s_count_reg[3]_1 ,
    \s_count_reg[3]_2 ,
    \s_count_reg[3]_3 ,
    \s_count_reg[3]_4 ,
    \s_count_reg[3]_5 ,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    last_enable,
    s_increment_2);
  output s_overflow_2;
  output increment1;
  output \s_count_reg[3] ;
  output \s_count_reg[3]_0 ;
  output \s_count_reg[3]_1 ;
  output \s_count_reg[3]_2 ;
  output \s_count_reg[3]_3 ;
  output \s_count_reg[3]_4 ;
  output \s_count_reg[3]_5 ;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input last_enable;
  input s_increment_2;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire increment1;
  wire last_enable;
  wire reset_IBUF;
  wire \s_count_reg[3] ;
  wire \s_count_reg[3]_0 ;
  wire \s_count_reg[3]_1 ;
  wire \s_count_reg[3]_2 ;
  wire \s_count_reg[3]_3 ;
  wire \s_count_reg[3]_4 ;
  wire \s_count_reg[3]_5 ;
  wire s_increment_2;
  wire s_overflow_2;

  decimal_counter counter
       (.E(E),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1),
        .last_enable(last_enable),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[3]_0 (\s_count_reg[3] ),
        .\s_count_reg[3]_1 (\s_count_reg[3]_0 ),
        .\s_count_reg[3]_2 (\s_count_reg[3]_1 ),
        .\s_count_reg[3]_3 (\s_count_reg[3]_2 ),
        .\s_count_reg[3]_4 (\s_count_reg[3]_3 ),
        .\s_count_reg[3]_5 (\s_count_reg[3]_4 ),
        .\s_count_reg[3]_6 (\s_count_reg[3]_5 ),
        .s_increment_2(s_increment_2),
        .s_overflow_2(s_overflow_2));
endmodule

(* ORIG_REF_NAME = "counter_7seg" *) 
module counter_7seg__parameterized2
   (\s_count_reg[0] ,
    \s_count_reg[0]_0 ,
    clk_15k_IBUF_BUFG,
    reset_IBUF);
  output \s_count_reg[0] ;
  input \s_count_reg[0]_0 ;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;

  wire clk_15k_IBUF_BUFG;
  wire reset_IBUF;
  wire \s_count_reg[0] ;
  wire \s_count_reg[0]_0 ;

  decimal_counter__parameterized1 counter
       (.clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[0]_0 (\s_count_reg[0] ),
        .\s_count_reg[0]_1 (\s_count_reg[0]_0 ));
endmodule

module decimal_counter
   (s_overflow_2,
    increment1,
    \s_count_reg[3]_0 ,
    \s_count_reg[3]_1 ,
    \s_count_reg[3]_2 ,
    \s_count_reg[3]_3 ,
    \s_count_reg[3]_4 ,
    \s_count_reg[3]_5 ,
    \s_count_reg[3]_6 ,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    last_enable,
    s_increment_2);
  output s_overflow_2;
  output increment1;
  output \s_count_reg[3]_0 ;
  output \s_count_reg[3]_1 ;
  output \s_count_reg[3]_2 ;
  output \s_count_reg[3]_3 ;
  output \s_count_reg[3]_4 ;
  output \s_count_reg[3]_5 ;
  output \s_count_reg[3]_6 ;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input last_enable;
  input s_increment_2;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire increment1;
  wire last_enable;
  wire overflow_i_1__1_n_0;
  wire reset_IBUF;
  wire \s_count[0]_i_1__1_n_0 ;
  wire \s_count[1]_i_1__1_n_0 ;
  wire \s_count[2]_i_1_n_0 ;
  wire \s_count[3]_i_2__1_n_0 ;
  wire \s_count_reg[3]_0 ;
  wire \s_count_reg[3]_1 ;
  wire \s_count_reg[3]_2 ;
  wire \s_count_reg[3]_3 ;
  wire \s_count_reg[3]_4 ;
  wire \s_count_reg[3]_5 ;
  wire \s_count_reg[3]_6 ;
  wire \s_count_reg_n_0_[0] ;
  wire \s_count_reg_n_0_[1] ;
  wire \s_count_reg_n_0_[2] ;
  wire \s_count_reg_n_0_[3] ;
  wire s_increment_2;
  wire s_overflow_2;

  LUT2 #(
    .INIT(4'h2)) 
    increment_i_1__1
       (.I0(s_overflow_2),
        .I1(last_enable),
        .O(increment1));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hAAA80001)) 
    overflow_i_1__1
       (.I0(s_increment_2),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[3] ),
        .O(overflow_i_1__1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    overflow_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(overflow_i_1__1_n_0),
        .Q(s_overflow_2));
  LUT5 #(
    .INIT(32'h00001FFF)) 
    \s_count[0]_i_1__1 
       (.I0(\s_count_reg_n_0_[1] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(s_increment_2),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h52522524)) 
    \s_count[1]_i_1__1 
       (.I0(s_increment_2),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[1] ),
        .O(\s_count[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h57207204)) 
    \s_count[2]_i_1 
       (.I0(s_increment_2),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[1] ),
        .O(\s_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h64444449)) 
    \s_count[3]_i_2__1 
       (.I0(s_increment_2),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[3]_i_2__1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[0]_i_1__1_n_0 ),
        .Q(\s_count_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[1]_i_1__1_n_0 ),
        .Q(\s_count_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[2]_i_1_n_0 ),
        .Q(\s_count_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[3] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[3]_i_2__1_n_0 ),
        .Q(\s_count_reg_n_0_[3] ));
  LUT4 #(
    .INIT(16'h4025)) 
    \segments_out[0]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_0 ));
  LUT4 #(
    .INIT(16'h4874)) 
    \segments_out[1]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[2] ),
        .O(\s_count_reg[3]_4 ));
  LUT4 #(
    .INIT(16'h5710)) 
    \segments_out[2]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[1] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[0] ),
        .O(\s_count_reg[3]_5 ));
  LUT4 #(
    .INIT(16'hC014)) 
    \segments_out[3]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_1 ));
  LUT4 #(
    .INIT(16'h8098)) 
    \segments_out[4]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[0] ),
        .O(\s_count_reg[3]_2 ));
  LUT4 #(
    .INIT(16'hB860)) 
    \segments_out[5]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_3 ));
  LUT4 #(
    .INIT(16'h0894)) 
    \segments_out[6]_i_3 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_6 ));
endmodule

(* ORIG_REF_NAME = "decimal_counter" *) 
module decimal_counter_5
   (s_overflow_1,
    increment1,
    \s_count_reg[2]_0 ,
    \s_count_reg[2]_1 ,
    \s_count_reg[0]_0 ,
    \s_counter_reg[1] ,
    \s_count_reg[0]_1 ,
    \s_count_reg[0]_2 ,
    \s_count_reg[0]_3 ,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    last_enable,
    Q,
    s_increment_1,
    \segments_out_reg[2] );
  output s_overflow_1;
  output increment1;
  output \s_count_reg[2]_0 ;
  output \s_count_reg[2]_1 ;
  output \s_count_reg[0]_0 ;
  output \s_counter_reg[1] ;
  output \s_count_reg[0]_1 ;
  output \s_count_reg[0]_2 ;
  output \s_count_reg[0]_3 ;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input last_enable;
  input [0:0]Q;
  input s_increment_1;
  input \segments_out_reg[2] ;

  wire [0:0]E;
  wire [0:0]Q;
  wire clk_15k_IBUF_BUFG;
  wire increment1;
  wire last_enable;
  wire overflow_i_1__0_n_0;
  wire reset_IBUF;
  wire \s_count[0]_i_1__0_n_0 ;
  wire \s_count[1]_i_1__0_n_0 ;
  wire \s_count[2]_i_1__1_n_0 ;
  wire \s_count[3]_i_2__0_n_0 ;
  wire \s_count_reg[0]_0 ;
  wire \s_count_reg[0]_1 ;
  wire \s_count_reg[0]_2 ;
  wire \s_count_reg[0]_3 ;
  wire \s_count_reg[2]_0 ;
  wire \s_count_reg[2]_1 ;
  wire \s_count_reg_n_0_[0] ;
  wire \s_count_reg_n_0_[1] ;
  wire \s_count_reg_n_0_[2] ;
  wire \s_count_reg_n_0_[3] ;
  wire \s_counter_reg[1] ;
  wire s_increment_1;
  wire s_overflow_1;
  wire \segments_out_reg[2] ;

  LUT2 #(
    .INIT(4'h2)) 
    increment_i_1__0
       (.I0(s_overflow_1),
        .I1(last_enable),
        .O(increment1));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAA80001)) 
    overflow_i_1__0
       (.I0(s_increment_1),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[3] ),
        .O(overflow_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    overflow_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(overflow_i_1__0_n_0),
        .Q(s_overflow_1));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h00001FFF)) 
    \s_count[0]_i_1__0 
       (.I0(\s_count_reg_n_0_[1] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(s_increment_1),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h55222254)) 
    \s_count[1]_i_1__0 
       (.I0(s_increment_1),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[1]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'h57722004)) 
    \s_count[2]_i_1__1 
       (.I0(s_increment_1),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[2] ),
        .O(\s_count[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h64444449)) 
    \s_count[3]_i_2__0 
       (.I0(s_increment_1),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[2] ),
        .O(\s_count[3]_i_2__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[0]_i_1__0_n_0 ),
        .Q(\s_count_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[1]_i_1__0_n_0 ),
        .Q(\s_count_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[2]_i_1__1_n_0 ),
        .Q(\s_count_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[3] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[3]_i_2__0_n_0 ),
        .Q(\s_count_reg_n_0_[3] ));
  LUT5 #(
    .INIT(32'hAABAEBAB)) 
    \segments_out[0]_i_2 
       (.I0(Q),
        .I1(\s_count_reg_n_0_[1] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[3] ),
        .O(\s_counter_reg[1] ));
  LUT6 #(
    .INIT(64'h8B8B88BBB88B8888)) 
    \segments_out[1]_i_2 
       (.I0(\segments_out_reg[2] ),
        .I1(Q),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[0] ),
        .I5(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[0]_1 ));
  LUT6 #(
    .INIT(64'h8B8B8BBB888B8888)) 
    \segments_out[2]_i_2 
       (.I0(\segments_out_reg[2] ),
        .I1(Q),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[2] ),
        .I5(\s_count_reg_n_0_[0] ),
        .O(\s_count_reg[0]_3 ));
  LUT6 #(
    .INIT(64'hBB888888888B8B88)) 
    \segments_out[3]_i_2 
       (.I0(\segments_out_reg[2] ),
        .I1(Q),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[0] ),
        .I5(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[0]_0 ));
  LUT5 #(
    .INIT(32'h00008A04)) 
    \segments_out[4]_i_2 
       (.I0(\s_count_reg_n_0_[2] ),
        .I1(\s_count_reg_n_0_[1] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[3] ),
        .I4(Q),
        .O(\s_count_reg[2]_1 ));
  LUT5 #(
    .INIT(32'h0000E228)) 
    \segments_out[5]_i_2 
       (.I0(\s_count_reg_n_0_[2] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[3] ),
        .I4(Q),
        .O(\s_count_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h88B88888B88B8B88)) 
    \segments_out[6]_i_2 
       (.I0(\segments_out_reg[2] ),
        .I1(Q),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[0] ),
        .I5(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[0]_2 ));
endmodule

(* ORIG_REF_NAME = "decimal_counter" *) 
module decimal_counter_6
   (s_overflow_0,
    increment1,
    \s_count_reg[3]_0 ,
    \s_count_reg[3]_1 ,
    \s_count_reg[3]_2 ,
    \s_count_reg[3]_3 ,
    \s_count_reg[3]_4 ,
    \s_count_reg[3]_5 ,
    \s_count_reg[3]_6 ,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    last_enable,
    s_increment_0);
  output s_overflow_0;
  output increment1;
  output \s_count_reg[3]_0 ;
  output \s_count_reg[3]_1 ;
  output \s_count_reg[3]_2 ;
  output \s_count_reg[3]_3 ;
  output \s_count_reg[3]_4 ;
  output \s_count_reg[3]_5 ;
  output \s_count_reg[3]_6 ;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input last_enable;
  input s_increment_0;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire increment1;
  wire last_enable;
  wire overflow_i_1_n_0;
  wire reset_IBUF;
  wire \s_count[0]_i_1_n_0 ;
  wire \s_count[1]_i_1_n_0 ;
  wire \s_count[2]_i_1__0_n_0 ;
  wire \s_count[3]_i_2_n_0 ;
  wire \s_count_reg[3]_0 ;
  wire \s_count_reg[3]_1 ;
  wire \s_count_reg[3]_2 ;
  wire \s_count_reg[3]_3 ;
  wire \s_count_reg[3]_4 ;
  wire \s_count_reg[3]_5 ;
  wire \s_count_reg[3]_6 ;
  wire \s_count_reg_n_0_[0] ;
  wire \s_count_reg_n_0_[1] ;
  wire \s_count_reg_n_0_[2] ;
  wire \s_count_reg_n_0_[3] ;
  wire s_increment_0;
  wire s_overflow_0;

  LUT2 #(
    .INIT(4'h2)) 
    increment_i_1
       (.I0(s_overflow_0),
        .I1(last_enable),
        .O(increment1));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hAAA80001)) 
    overflow_i_1
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[3] ),
        .O(overflow_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    overflow_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(overflow_i_1_n_0),
        .Q(s_overflow_0));
  LUT5 #(
    .INIT(32'h00001FFF)) 
    \s_count[0]_i_1 
       (.I0(\s_count_reg_n_0_[1] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[3] ),
        .I3(s_increment_0),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h55222254)) 
    \s_count[1]_i_1 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[1] ),
        .O(\s_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h57722004)) 
    \s_count[2]_i_1__0 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[2] ),
        .O(\s_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h64444449)) 
    \s_count[3]_i_2 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[2] ),
        .O(\s_count[3]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[0]_i_1_n_0 ),
        .Q(\s_count_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[1]_i_1_n_0 ),
        .Q(\s_count_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[2]_i_1__0_n_0 ),
        .Q(\s_count_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[3] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[3]_i_2_n_0 ),
        .Q(\s_count_reg_n_0_[3] ));
  LUT4 #(
    .INIT(16'h4025)) 
    \segments_out[0]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_1 ));
  LUT4 #(
    .INIT(16'h5930)) 
    \segments_out[1]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[0] ),
        .O(\s_count_reg[3]_2 ));
  LUT4 #(
    .INIT(16'h5710)) 
    \segments_out[2]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[1] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[0] ),
        .O(\s_count_reg[3]_6 ));
  LUT4 #(
    .INIT(16'hC014)) 
    \segments_out[3]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_0 ));
  LUT4 #(
    .INIT(16'hA210)) 
    \segments_out[4]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[2] ),
        .O(\s_count_reg[3]_5 ));
  LUT4 #(
    .INIT(16'hB680)) 
    \segments_out[5]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[2] ),
        .O(\s_count_reg[3]_3 ));
  LUT4 #(
    .INIT(16'h2094)) 
    \segments_out[6]_i_4 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(\s_count_reg[3]_4 ));
endmodule

(* ORIG_REF_NAME = "decimal_counter" *) 
module decimal_counter__parameterized1
   (\s_count_reg[0]_0 ,
    \s_count_reg[0]_1 ,
    clk_15k_IBUF_BUFG,
    reset_IBUF);
  output \s_count_reg[0]_0 ;
  input \s_count_reg[0]_1 ;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;

  wire clk_15k_IBUF_BUFG;
  wire reset_IBUF;
  wire \s_count_reg[0]_0 ;
  wire \s_count_reg[0]_1 ;

  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_count_reg[0]_1 ),
        .Q(\s_count_reg[0]_0 ));
endmodule

module decoder
   (s_up_down,
    s_count_enable,
    up_down_reg_0,
    increment1,
    up_down_reg_1,
    p_0_in,
    enc_b_IBUF,
    CLK,
    s_decrement_0,
    last_enable,
    reset_IBUF);
  output s_up_down;
  output s_count_enable;
  output up_down_reg_0;
  output increment1;
  output up_down_reg_1;
  input p_0_in;
  input enc_b_IBUF;
  input CLK;
  input s_decrement_0;
  input last_enable;
  input reset_IBUF;

  wire CLK;
  wire count_enable_i_1_n_0;
  wire enc_b_IBUF;
  wire increment1;
  wire last_enable;
  wire p_0_in;
  wire reset_IBUF;
  wire s_count_enable;
  wire s_decrement_0;
  wire [1:0]s_step_counter;
  wire \s_step_counter[0]_i_1_n_0 ;
  wire \s_step_counter[1]_i_1_n_0 ;
  wire s_up_down;
  wire up_down_reg_0;
  wire up_down_reg_1;

  LUT4 #(
    .INIT(16'hF808)) 
    count_enable_i_1
       (.I0(s_step_counter[0]),
        .I1(s_step_counter[1]),
        .I2(reset_IBUF),
        .I3(s_count_enable),
        .O(count_enable_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    count_enable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(count_enable_i_1_n_0),
        .Q(s_count_enable),
        .R(1'b0));
  (* \PinAttr:I1:HOLD_DETOUR  = "179" *) 
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hCCCC0050)) 
    decrement_i_1
       (.I0(s_up_down),
        .I1(s_decrement_0),
        .I2(s_count_enable),
        .I3(last_enable),
        .I4(reset_IBUF),
        .O(up_down_reg_0));
  LUT2 #(
    .INIT(4'h1)) 
    increment_i_1__2
       (.I0(s_up_down),
        .I1(reset_IBUF),
        .O(up_down_reg_1));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    increment_i_2
       (.I0(s_count_enable),
        .I1(last_enable),
        .O(increment1));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \s_step_counter[0]_i_1 
       (.I0(s_step_counter[0]),
        .O(\s_step_counter[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \s_step_counter[1]_i_1 
       (.I0(s_step_counter[0]),
        .I1(s_step_counter[1]),
        .O(\s_step_counter[1]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_step_counter_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_step_counter[0]_i_1_n_0 ),
        .Q(s_step_counter[0]));
  FDCE #(
    .INIT(1'b0)) 
    \s_step_counter_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_step_counter[1]_i_1_n_0 ),
        .Q(s_step_counter[1]));
  FDRE #(
    .INIT(1'b0)) 
    up_down_reg
       (.C(CLK),
        .CE(p_0_in),
        .D(enc_b_IBUF),
        .Q(s_up_down),
        .R(1'b0));
endmodule

(* ECO_CHECKSUM = "aa6efd67" *) (* digits = "4" *) (* max_ms_digit = "1" *) 
(* NotValidForBitStream *)
module encoder_decode
   (digits_7seg,
    anodes_7seg,
    enc_a,
    enc_b,
    reset,
    clk_15k);
  output [6:0]digits_7seg;
  output [3:0]anodes_7seg;
  input enc_a;
  input enc_b;
  input reset;
  input clk_15k;

  wire [3:0]anodes_7seg;
  wire [3:0]anodes_7seg_OBUF;
  wire clk_15k;
  wire clk_15k_IBUF;
  wire clk_15k_IBUF_BUFG;
  wire decoder_ab_n_2;
  wire decoder_ab_n_4;
  wire [6:0]digits_7seg;
  wire [6:0]digits_7seg_OBUF;
  wire enc_a;
  wire enc_a_IBUF;
  wire enc_a_IBUF_BUFG;
  wire enc_b;
  wire enc_b_IBUF;
  wire \generador_decider[0].updown_decider/increment1 ;
  wire \generador_decider[0].updown_decider/last_enable ;
  wire \generador_decider[3].updown_decider/p_0_in ;
  wire reset;
  wire reset_IBUF;
  wire s_count_enable;
  wire s_decrement_0;
  wire s_up_down;

initial begin
 $sdf_annotate("enc_dec_tb_time_impl.sdf",,,,"tool_control");
end
  OBUF \anodes_7seg_OBUF[0]_inst 
       (.I(anodes_7seg_OBUF[0]),
        .O(anodes_7seg[0]));
  OBUF \anodes_7seg_OBUF[1]_inst 
       (.I(anodes_7seg_OBUF[1]),
        .O(anodes_7seg[1]));
  OBUF \anodes_7seg_OBUF[2]_inst 
       (.I(anodes_7seg_OBUF[2]),
        .O(anodes_7seg[2]));
  OBUF \anodes_7seg_OBUF[3]_inst 
       (.I(anodes_7seg_OBUF[3]),
        .O(anodes_7seg[3]));
  BUFG clk_15k_IBUF_BUFG_inst
       (.I(clk_15k_IBUF),
        .O(clk_15k_IBUF_BUFG));
  IBUF clk_15k_IBUF_inst
       (.I(clk_15k),
        .O(clk_15k_IBUF));
  full_counter counter_4_disp
       (.Q(digits_7seg_OBUF),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .decrement_reg(decoder_ab_n_2),
        .increment1(\generador_decider[0].updown_decider/increment1 ),
        .increment_reg(decoder_ab_n_4),
        .last_enable(\generador_decider[0].updown_decider/last_enable ),
        .p_0_in(\generador_decider[3].updown_decider/p_0_in ),
        .reset_IBUF(reset_IBUF),
        .\s_anodes_reg[3] (anodes_7seg_OBUF),
        .s_count_enable(s_count_enable),
        .s_decrement_0(s_decrement_0),
        .s_up_down(s_up_down));
  decoder decoder_ab
       (.CLK(enc_a_IBUF_BUFG),
        .enc_b_IBUF(enc_b_IBUF),
        .increment1(\generador_decider[0].updown_decider/increment1 ),
        .last_enable(\generador_decider[0].updown_decider/last_enable ),
        .p_0_in(\generador_decider[3].updown_decider/p_0_in ),
        .reset_IBUF(reset_IBUF),
        .s_count_enable(s_count_enable),
        .s_decrement_0(s_decrement_0),
        .s_up_down(s_up_down),
        .up_down_reg_0(decoder_ab_n_2),
        .up_down_reg_1(decoder_ab_n_4));
  OBUF \digits_7seg_OBUF[0]_inst 
       (.I(digits_7seg_OBUF[0]),
        .O(digits_7seg[0]));
  OBUF \digits_7seg_OBUF[1]_inst 
       (.I(digits_7seg_OBUF[1]),
        .O(digits_7seg[1]));
  OBUF \digits_7seg_OBUF[2]_inst 
       (.I(digits_7seg_OBUF[2]),
        .O(digits_7seg[2]));
  OBUF \digits_7seg_OBUF[3]_inst 
       (.I(digits_7seg_OBUF[3]),
        .O(digits_7seg[3]));
  OBUF \digits_7seg_OBUF[4]_inst 
       (.I(digits_7seg_OBUF[4]),
        .O(digits_7seg[4]));
  OBUF \digits_7seg_OBUF[5]_inst 
       (.I(digits_7seg_OBUF[5]),
        .O(digits_7seg[5]));
  OBUF \digits_7seg_OBUF[6]_inst 
       (.I(digits_7seg_OBUF[6]),
        .O(digits_7seg[6]));
  BUFG enc_a_IBUF_BUFG_inst
       (.I(enc_a_IBUF),
        .O(enc_a_IBUF_BUFG));
  IBUF enc_a_IBUF_inst
       (.I(enc_a),
        .O(enc_a_IBUF));
  IBUF enc_b_IBUF_inst
       (.I(enc_b),
        .O(enc_b_IBUF));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
endmodule

module full_counter
   (last_enable,
    p_0_in,
    s_decrement_0,
    Q,
    \s_anodes_reg[3] ,
    s_count_enable,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    increment_reg,
    increment1,
    decrement_reg,
    s_up_down);
  output last_enable;
  output p_0_in;
  output s_decrement_0;
  output [6:0]Q;
  output [3:0]\s_anodes_reg[3] ;
  input s_count_enable;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input increment_reg;
  input increment1;
  input decrement_reg;
  input s_up_down;

  wire [6:0]Q;
  wire clk;
  wire clk_15k_IBUF_BUFG;
  wire decrement_reg;
  wire \generador_decider[0].updown_decider_n_3 ;
  wire \generador_decider[1].updown_decider_n_2 ;
  wire \generador_decider[2].updown_decider_n_2 ;
  wire \generador_decider[3].updown_decider_n_1 ;
  wire \generador_display[0].display_n_2 ;
  wire \generador_display[0].display_n_3 ;
  wire \generador_display[0].display_n_4 ;
  wire \generador_display[0].display_n_5 ;
  wire \generador_display[0].display_n_6 ;
  wire \generador_display[0].display_n_7 ;
  wire \generador_display[0].display_n_8 ;
  wire \generador_display[1].display_n_2 ;
  wire \generador_display[1].display_n_3 ;
  wire \generador_display[1].display_n_4 ;
  wire \generador_display[1].display_n_5 ;
  wire \generador_display[1].display_n_6 ;
  wire \generador_display[1].display_n_7 ;
  wire \generador_display[1].display_n_8 ;
  wire \generador_display[2].display_n_2 ;
  wire \generador_display[2].display_n_3 ;
  wire \generador_display[2].display_n_4 ;
  wire \generador_display[2].display_n_5 ;
  wire \generador_display[2].display_n_6 ;
  wire \generador_display[2].display_n_7 ;
  wire \generador_display[2].display_n_8 ;
  wire increment1;
  wire increment1_3;
  wire increment1_4;
  wire increment1_5;
  wire increment_reg;
  wire last_display_n_0;
  wire last_enable;
  wire last_enable_0;
  wire last_enable_1;
  wire last_enable_2;
  wire p_0_in;
  wire reset_IBUF;
  wire [3:0]\s_anodes_reg[3] ;
  wire s_count_enable;
  wire [1:1]s_counter;
  wire s_decrement_0;
  wire s_increment_0;
  wire s_increment_1;
  wire s_increment_2;
  wire s_overflow_0;
  wire s_overflow_1;
  wire s_overflow_2;
  wire s_up_down;

  mux_displays display_mux
       (.CLK(clk),
        .E(p_0_in),
        .Q(s_counter),
        .reset_IBUF(reset_IBUF),
        .\s_anodes_reg[3]_0 (\s_anodes_reg[3] ),
        .\segments_out_reg[0]_0 (\generador_display[1].display_n_5 ),
        .\segments_out_reg[0]_1 (\generador_display[2].display_n_2 ),
        .\segments_out_reg[0]_2 (\generador_display[0].display_n_3 ),
        .\segments_out_reg[1]_0 (\generador_display[1].display_n_6 ),
        .\segments_out_reg[1]_1 (\generador_display[2].display_n_6 ),
        .\segments_out_reg[1]_2 (\generador_display[0].display_n_4 ),
        .\segments_out_reg[2]_0 (\generador_display[1].display_n_8 ),
        .\segments_out_reg[2]_1 (\generador_display[2].display_n_7 ),
        .\segments_out_reg[2]_2 (\generador_display[0].display_n_8 ),
        .\segments_out_reg[3]_0 (\generador_display[1].display_n_4 ),
        .\segments_out_reg[3]_1 (\generador_display[2].display_n_3 ),
        .\segments_out_reg[3]_2 (\generador_display[0].display_n_2 ),
        .\segments_out_reg[4]_0 (\generador_display[1].display_n_3 ),
        .\segments_out_reg[4]_1 (\generador_display[2].display_n_4 ),
        .\segments_out_reg[4]_2 (\generador_display[0].display_n_7 ),
        .\segments_out_reg[5]_0 (\generador_display[1].display_n_2 ),
        .\segments_out_reg[5]_1 (\generador_display[2].display_n_5 ),
        .\segments_out_reg[5]_2 (\generador_display[0].display_n_5 ),
        .\segments_out_reg[6]_0 (Q),
        .\segments_out_reg[6]_1 (\generador_display[1].display_n_7 ),
        .\segments_out_reg[6]_2 (\generador_display[2].display_n_8 ),
        .\segments_out_reg[6]_3 (\generador_display[0].display_n_6 ));
  clk_divisor divisor_15_1
       (.CLK(clk),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .reset_IBUF(reset_IBUF));
  count_decider \generador_decider[0].updown_decider 
       (.E(p_0_in),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .decrement_reg_0(decrement_reg),
        .increment1(increment1),
        .increment_reg_0(\generador_decider[0].updown_decider_n_3 ),
        .increment_reg_1(increment_reg),
        .last_enable(last_enable),
        .reset_IBUF(reset_IBUF),
        .s_count_enable(s_count_enable),
        .s_decrement_0(s_decrement_0),
        .s_increment_0(s_increment_0));
  count_decider_0 \generador_decider[1].updown_decider 
       (.E(p_0_in),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .decrement_reg_0(\generador_decider[1].updown_decider_n_2 ),
        .increment1(increment1_3),
        .increment_reg_0(increment_reg),
        .last_enable(last_enable_0),
        .reset_IBUF(reset_IBUF),
        .s_increment_1(s_increment_1),
        .s_overflow_0(s_overflow_0),
        .s_up_down(s_up_down));
  count_decider_1 \generador_decider[2].updown_decider 
       (.E(p_0_in),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .decrement_reg_0(\generador_decider[2].updown_decider_n_2 ),
        .increment1(increment1_4),
        .increment_reg_0(increment_reg),
        .last_enable(last_enable_1),
        .reset_IBUF(reset_IBUF),
        .s_increment_2(s_increment_2),
        .s_overflow_1(s_overflow_1),
        .s_up_down(s_up_down));
  count_decider_2 \generador_decider[3].updown_decider 
       (.E(p_0_in),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1_5),
        .increment_reg_0(\generador_decider[3].updown_decider_n_1 ),
        .increment_reg_1(increment_reg),
        .last_enable(last_enable_2),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[0] (last_display_n_0),
        .s_overflow_2(s_overflow_2),
        .s_up_down(s_up_down));
  counter_7seg \generador_display[0].display 
       (.E(\generador_decider[0].updown_decider_n_3 ),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1_3),
        .last_enable(last_enable_0),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[3] (\generador_display[0].display_n_2 ),
        .\s_count_reg[3]_0 (\generador_display[0].display_n_3 ),
        .\s_count_reg[3]_1 (\generador_display[0].display_n_4 ),
        .\s_count_reg[3]_2 (\generador_display[0].display_n_5 ),
        .\s_count_reg[3]_3 (\generador_display[0].display_n_6 ),
        .\s_count_reg[3]_4 (\generador_display[0].display_n_7 ),
        .\s_count_reg[3]_5 (\generador_display[0].display_n_8 ),
        .s_increment_0(s_increment_0),
        .s_overflow_0(s_overflow_0));
  counter_7seg_3 \generador_display[1].display 
       (.E(\generador_decider[1].updown_decider_n_2 ),
        .Q(s_counter),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1_4),
        .last_enable(last_enable_1),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[0] (\generador_display[1].display_n_4 ),
        .\s_count_reg[0]_0 (\generador_display[1].display_n_6 ),
        .\s_count_reg[0]_1 (\generador_display[1].display_n_7 ),
        .\s_count_reg[0]_2 (\generador_display[1].display_n_8 ),
        .\s_count_reg[2] (\generador_display[1].display_n_2 ),
        .\s_count_reg[2]_0 (\generador_display[1].display_n_3 ),
        .\s_counter_reg[1] (\generador_display[1].display_n_5 ),
        .s_increment_1(s_increment_1),
        .s_overflow_1(s_overflow_1),
        .\segments_out_reg[2] (last_display_n_0));
  counter_7seg_4 \generador_display[2].display 
       (.E(\generador_decider[2].updown_decider_n_2 ),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .increment1(increment1_5),
        .last_enable(last_enable_2),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[3] (\generador_display[2].display_n_2 ),
        .\s_count_reg[3]_0 (\generador_display[2].display_n_3 ),
        .\s_count_reg[3]_1 (\generador_display[2].display_n_4 ),
        .\s_count_reg[3]_2 (\generador_display[2].display_n_5 ),
        .\s_count_reg[3]_3 (\generador_display[2].display_n_6 ),
        .\s_count_reg[3]_4 (\generador_display[2].display_n_7 ),
        .\s_count_reg[3]_5 (\generador_display[2].display_n_8 ),
        .s_increment_2(s_increment_2),
        .s_overflow_2(s_overflow_2));
  counter_7seg__parameterized2 last_display
       (.clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .reset_IBUF(reset_IBUF),
        .\s_count_reg[0] (last_display_n_0),
        .\s_count_reg[0]_0 (\generador_decider[3].updown_decider_n_1 ));
endmodule

module mux_displays
   (E,
    Q,
    \segments_out_reg[6]_0 ,
    \s_anodes_reg[3]_0 ,
    reset_IBUF,
    \segments_out_reg[3]_0 ,
    \segments_out_reg[3]_1 ,
    \segments_out_reg[3]_2 ,
    \segments_out_reg[0]_0 ,
    \segments_out_reg[0]_1 ,
    \segments_out_reg[0]_2 ,
    \segments_out_reg[1]_0 ,
    \segments_out_reg[1]_1 ,
    \segments_out_reg[1]_2 ,
    \segments_out_reg[5]_0 ,
    \segments_out_reg[5]_1 ,
    \segments_out_reg[5]_2 ,
    \segments_out_reg[6]_1 ,
    \segments_out_reg[6]_2 ,
    \segments_out_reg[6]_3 ,
    \segments_out_reg[4]_0 ,
    \segments_out_reg[4]_1 ,
    \segments_out_reg[4]_2 ,
    \segments_out_reg[2]_0 ,
    \segments_out_reg[2]_1 ,
    \segments_out_reg[2]_2 ,
    CLK);
  output [0:0]E;
  output [0:0]Q;
  output [6:0]\segments_out_reg[6]_0 ;
  output [3:0]\s_anodes_reg[3]_0 ;
  input reset_IBUF;
  input \segments_out_reg[3]_0 ;
  input \segments_out_reg[3]_1 ;
  input \segments_out_reg[3]_2 ;
  input \segments_out_reg[0]_0 ;
  input \segments_out_reg[0]_1 ;
  input \segments_out_reg[0]_2 ;
  input \segments_out_reg[1]_0 ;
  input \segments_out_reg[1]_1 ;
  input \segments_out_reg[1]_2 ;
  input \segments_out_reg[5]_0 ;
  input \segments_out_reg[5]_1 ;
  input \segments_out_reg[5]_2 ;
  input \segments_out_reg[6]_1 ;
  input \segments_out_reg[6]_2 ;
  input \segments_out_reg[6]_3 ;
  input \segments_out_reg[4]_0 ;
  input \segments_out_reg[4]_1 ;
  input \segments_out_reg[4]_2 ;
  input \segments_out_reg[2]_0 ;
  input \segments_out_reg[2]_1 ;
  input \segments_out_reg[2]_2 ;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire [0:0]Q;
  wire reset_IBUF;
  wire \s_anodes[0]_i_1_n_0 ;
  wire \s_anodes[1]_i_1_n_0 ;
  wire \s_anodes[2]_i_1_n_0 ;
  wire \s_anodes[3]_i_1_n_0 ;
  wire [3:0]\s_anodes_reg[3]_0 ;
  wire [0:0]s_counter;
  wire \s_counter[0]_i_1_n_0 ;
  wire \s_counter[1]_i_1_n_0 ;
  wire \segments_out[0]_i_1_n_0 ;
  wire \segments_out[1]_i_1_n_0 ;
  wire \segments_out[2]_i_1_n_0 ;
  wire \segments_out[3]_i_1_n_0 ;
  wire \segments_out[4]_i_1_n_0 ;
  wire \segments_out[5]_i_1_n_0 ;
  wire \segments_out[6]_i_1_n_0 ;
  wire \segments_out_reg[0]_0 ;
  wire \segments_out_reg[0]_1 ;
  wire \segments_out_reg[0]_2 ;
  wire \segments_out_reg[1]_0 ;
  wire \segments_out_reg[1]_1 ;
  wire \segments_out_reg[1]_2 ;
  wire \segments_out_reg[2]_0 ;
  wire \segments_out_reg[2]_1 ;
  wire \segments_out_reg[2]_2 ;
  wire \segments_out_reg[3]_0 ;
  wire \segments_out_reg[3]_1 ;
  wire \segments_out_reg[3]_2 ;
  wire \segments_out_reg[4]_0 ;
  wire \segments_out_reg[4]_1 ;
  wire \segments_out_reg[4]_2 ;
  wire \segments_out_reg[5]_0 ;
  wire \segments_out_reg[5]_1 ;
  wire \segments_out_reg[5]_2 ;
  wire [6:0]\segments_out_reg[6]_0 ;
  wire \segments_out_reg[6]_1 ;
  wire \segments_out_reg[6]_2 ;
  wire \segments_out_reg[6]_3 ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \s_anodes[0]_i_1 
       (.I0(Q),
        .I1(s_counter),
        .O(\s_anodes[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_anodes[1]_i_1 
       (.I0(s_counter),
        .I1(Q),
        .O(\s_anodes[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_anodes[2]_i_1 
       (.I0(Q),
        .I1(s_counter),
        .O(\s_anodes[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_anodes[3]_i_1 
       (.I0(Q),
        .I1(s_counter),
        .O(\s_anodes[3]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_anodes_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_anodes[0]_i_1_n_0 ),
        .Q(\s_anodes_reg[3]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \s_anodes_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_anodes[1]_i_1_n_0 ),
        .Q(\s_anodes_reg[3]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \s_anodes_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_anodes[2]_i_1_n_0 ),
        .Q(\s_anodes_reg[3]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \s_anodes_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_anodes[3]_i_1_n_0 ),
        .Q(\s_anodes_reg[3]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \s_counter[0]_i_1 
       (.I0(s_counter),
        .O(\s_counter[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \s_counter[1]_i_1 
       (.I0(s_counter),
        .I1(Q),
        .O(\s_counter[1]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[0]_i_1_n_0 ),
        .Q(s_counter));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[1]_i_1_n_0 ),
        .Q(Q));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[0]_i_1 
       (.I0(\segments_out_reg[0]_0 ),
        .I1(s_counter),
        .I2(\segments_out_reg[0]_1 ),
        .I3(Q),
        .I4(\segments_out_reg[0]_2 ),
        .O(\segments_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[1]_i_1 
       (.I0(\segments_out_reg[1]_0 ),
        .I1(s_counter),
        .I2(\segments_out_reg[1]_1 ),
        .I3(Q),
        .I4(\segments_out_reg[1]_2 ),
        .O(\segments_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[2]_i_1 
       (.I0(\segments_out_reg[2]_0 ),
        .I1(s_counter),
        .I2(\segments_out_reg[2]_1 ),
        .I3(Q),
        .I4(\segments_out_reg[2]_2 ),
        .O(\segments_out[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[3]_i_1 
       (.I0(\segments_out_reg[3]_0 ),
        .I1(s_counter),
        .I2(\segments_out_reg[3]_1 ),
        .I3(Q),
        .I4(\segments_out_reg[3]_2 ),
        .O(\segments_out[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[4]_i_1 
       (.I0(\segments_out_reg[4]_0 ),
        .I1(s_counter),
        .I2(\segments_out_reg[4]_1 ),
        .I3(Q),
        .I4(\segments_out_reg[4]_2 ),
        .O(\segments_out[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[5]_i_1 
       (.I0(\segments_out_reg[5]_0 ),
        .I1(s_counter),
        .I2(\segments_out_reg[5]_1 ),
        .I3(Q),
        .I4(\segments_out_reg[5]_2 ),
        .O(\segments_out[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \segments_out[6]_i_1 
       (.I0(\segments_out_reg[6]_1 ),
        .I1(s_counter),
        .I2(\segments_out_reg[6]_2 ),
        .I3(Q),
        .I4(\segments_out_reg[6]_3 ),
        .O(\segments_out[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[0]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[1]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[2]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[3]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[4]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[5]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \segments_out_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(\segments_out[6]_i_1_n_0 ),
        .Q(\segments_out_reg[6]_0 [6]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    up_down_i_1
       (.I0(reset_IBUF),
        .O(E));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
