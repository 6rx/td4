-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sun May 17 02:20:48 2020
-- Host        : snsv running 64-bit unknown
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/nacho/repos/sexto/td4/tp1/resolucion/vivado/td4_tp1_encoder.sim/sim_1/synth/func/xsim/tb_motor_encoder_func_synth.vhd
-- Design      : motor_encoder_control
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity clk_divisor is
  port (
    CLK : out STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end clk_divisor;

architecture STRUCTURE of clk_divisor is
  signal \^clk\ : STD_LOGIC;
  signal \s_counter[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \s_counter[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \s_counter[2]_i_1_n_0\ : STD_LOGIC;
  signal s_counter_reg : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal \s_counter_reg_n_0_[0]\ : STD_LOGIC;
  signal s_out_clk_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \s_counter[0]_i_1__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \s_counter[1]_i_1__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \s_counter[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of s_out_clk_i_1 : label is "soft_lutpair4";
begin
  CLK <= \^clk\;
\s_counter[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => s_counter_reg(2),
      I1 => s_counter_reg(1),
      I2 => \s_counter_reg_n_0_[0]\,
      O => \s_counter[0]_i_1__0_n_0\
    );
\s_counter[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1A"
    )
        port map (
      I0 => \s_counter_reg_n_0_[0]\,
      I1 => s_counter_reg(2),
      I2 => s_counter_reg(1),
      O => \s_counter[1]_i_1__0_n_0\
    );
\s_counter[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4A"
    )
        port map (
      I0 => s_counter_reg(2),
      I1 => \s_counter_reg_n_0_[0]\,
      I2 => s_counter_reg(1),
      O => \s_counter[2]_i_1_n_0\
    );
\s_counter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_counter[0]_i_1__0_n_0\,
      Q => \s_counter_reg_n_0_[0]\
    );
\s_counter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_counter[1]_i_1__0_n_0\,
      Q => s_counter_reg(1)
    );
\s_counter_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_counter[2]_i_1_n_0\,
      Q => s_counter_reg(2)
    );
s_out_clk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => s_counter_reg(2),
      I1 => s_counter_reg(1),
      I2 => \^clk\,
      O => s_out_clk_i_1_n_0
    );
s_out_clk_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => s_out_clk_i_1_n_0,
      Q => \^clk\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity count_decider is
  port (
    last_enable : out STD_LOGIC;
    last_enable_reg_0 : out STD_LOGIC;
    s_count_enable : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end count_decider;

architecture STRUCTURE of count_decider is
  signal \^last_enable\ : STD_LOGIC;
begin
  last_enable <= \^last_enable\;
last_enable_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => s_count_enable,
      Q => \^last_enable\
    );
\s_count[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^last_enable\,
      I1 => s_count_enable,
      O => last_enable_reg_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity count_decider_0 is
  port (
    last_enable : out STD_LOGIC;
    s_overflow_0 : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of count_decider_0 : entity is "count_decider";
end count_decider_0;

architecture STRUCTURE of count_decider_0 is
begin
last_enable_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => s_overflow_0,
      Q => last_enable
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity count_decider_1 is
  port (
    last_enable : out STD_LOGIC;
    s_overflow_1 : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of count_decider_1 : entity is "count_decider";
end count_decider_1;

architecture STRUCTURE of count_decider_1 is
begin
last_enable_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => s_overflow_1,
      Q => last_enable
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity count_decider_2 is
  port (
    last_enable_reg_0 : out STD_LOGIC;
    s_overflow_2 : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    \s_count_reg[0]\ : in STD_LOGIC;
    last_enable : in STD_LOGIC;
    \s_count_reg[0]_0\ : in STD_LOGIC;
    \s_count_reg[0]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of count_decider_2 : entity is "count_decider";
end count_decider_2;

architecture STRUCTURE of count_decider_2 is
  signal last_enable_0 : STD_LOGIC;
begin
last_enable_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => s_overflow_2,
      Q => last_enable_0
    );
\s_count[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => last_enable_0,
      I1 => \s_count_reg[0]\,
      I2 => last_enable,
      I3 => \s_count_reg[0]_0\,
      I4 => \s_count_reg[0]_1\,
      O => last_enable_reg_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decimal_counter is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    s_overflow_2 : out STD_LOGIC;
    \s_count_reg[2]_0\ : out STD_LOGIC;
    \s_count_reg[3]_0\ : out STD_LOGIC;
    \s_count_reg[3]_1\ : out STD_LOGIC;
    \s_count_reg[3]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_counter_reg[0]\ : out STD_LOGIC;
    \s_counter_reg[0]_0\ : out STD_LOGIC;
    \s_count_reg[1]_0\ : out STD_LOGIC;
    \s_count_reg[1]_1\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[1]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \s_count_reg[2]_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \s_count_reg[2]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[0]_0\ : in STD_LOGIC;
    last_enable_reg : in STD_LOGIC;
    \segments_out_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \segments_out_reg[2]\ : in STD_LOGIC;
    \segments_out_reg[4]_0\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[5]\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    current_count0_carry : in STD_LOGIC;
    current_count0_carry_0 : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_15k_IBUF_BUFG : in STD_LOGIC
  );
end decimal_counter;

architecture STRUCTURE of decimal_counter is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \s_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \s_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \s_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \s_count[3]_i_2_n_0\ : STD_LOGIC;
  signal \^s_count_reg[2]_0\ : STD_LOGIC;
  signal \s_count_reg_n_0_[3]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \s_count[0]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_count[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_count[2]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_count[3]_i_2\ : label is "soft_lutpair11";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  \s_count_reg[2]_0\ <= \^s_count_reg[2]_0\;
\current_count0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => O(0),
      I1 => CO(0),
      O => \s_count_reg[2]_2\(0)
    );
current_count0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(2),
      I1 => current_count0_carry_0,
      O => \s_count_reg[2]_1\(1)
    );
current_count0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => current_count0_carry,
      O => \s_count_reg[2]_1\(0)
    );
\current_count2__0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      O => S(1)
    );
\current_count2__0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(2),
      O => S(0)
    );
\current_count2__0_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(1),
      O => DI(0)
    );
\current_count2__0_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(1),
      I1 => \s_count_reg_n_0_[3]\,
      O => \s_count_reg[1]_2\(3)
    );
\current_count2__0_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      O => \s_count_reg[1]_2\(2)
    );
\current_count2__0_carry_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \^q\(0),
      I1 => \s_count_reg_n_0_[3]\,
      I2 => \^q\(1),
      O => \s_count_reg[1]_2\(1)
    );
\current_count2__0_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(0),
      O => \s_count_reg[1]_2\(0)
    );
last_enable_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^s_count_reg[2]_0\,
      I1 => last_enable_reg,
      O => s_overflow_2
    );
\last_enable_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFFFFFFFFFFFE"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \s_count_reg_n_0_[3]\,
      I5 => s_up_down,
      O => \^s_count_reg[2]_0\
    );
\s_count[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FF1F"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \s_count_reg_n_0_[3]\,
      I3 => \s_count_reg[0]_0\,
      I4 => \^q\(0),
      O => \s_count[0]_i_1_n_0\
    );
\s_count[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99980606"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \s_count_reg_n_0_[3]\,
      I3 => \^q\(2),
      I4 => \s_count_reg[0]_0\,
      O => \s_count[1]_i_1_n_0\
    );
\s_count[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ABB11008"
    )
        port map (
      I0 => \s_count_reg[0]_0\,
      I1 => \s_count_reg_n_0_[3]\,
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(2),
      O => \s_count[2]_i_1_n_0\
    );
\s_count[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA94002"
    )
        port map (
      I0 => \s_count_reg[0]_0\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \s_count_reg_n_0_[3]\,
      O => \s_count[3]_i_2_n_0\
    );
\s_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => E(0),
      CLR => reset_IBUF,
      D => \s_count[0]_i_1_n_0\,
      Q => \^q\(0)
    );
\s_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => E(0),
      CLR => reset_IBUF,
      D => \s_count[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\s_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => E(0),
      CLR => reset_IBUF,
      D => \s_count[2]_i_1_n_0\,
      Q => \^q\(2)
    );
\s_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => E(0),
      CLR => reset_IBUF,
      D => \s_count[3]_i_2_n_0\,
      Q => \s_count_reg_n_0_[3]\
    );
\segments_out[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000180500000000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \s_count_reg_n_0_[3]\,
      I3 => \^q\(2),
      I4 => \segments_out_reg[4]\(0),
      I5 => \segments_out_reg[4]\(1),
      O => \s_count_reg[1]_0\
    );
\segments_out[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000428E0000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \s_count_reg_n_0_[3]\,
      I4 => \segments_out_reg[4]\(1),
      I5 => \segments_out_reg[4]\(0),
      O => \s_count_reg[1]_1\
    );
\segments_out[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"454CFFFF454C0000"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \segments_out_reg[4]\(1),
      I5 => \segments_out_reg[2]\,
      O => \s_count_reg[3]_0\
    );
\segments_out[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000040040400"
    )
        port map (
      I0 => \segments_out_reg[4]\(0),
      I1 => \segments_out_reg[4]\(1),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \^q\(2),
      I5 => \s_count_reg_n_0_[3]\,
      O => \s_counter_reg[0]_0\
    );
\segments_out[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8908FFFF89080000"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \segments_out_reg[4]\(1),
      I5 => \segments_out_reg[4]_0\,
      O => \s_count_reg[3]_1\
    );
\segments_out[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFA4C80000"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => D(0),
      I5 => \segments_out_reg[5]\,
      O => \s_count_reg[3]_2\(0)
    );
\segments_out[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040400000040040"
    )
        port map (
      I0 => \segments_out_reg[4]\(0),
      I1 => \segments_out_reg[4]\(1),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(2),
      I5 => \s_count_reg_n_0_[3]\,
      O => \s_counter_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decimal_counter_5 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    last_enable_reg : out STD_LOGIC;
    s_overflow_1 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \s_count_reg[3]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_counter_reg[1]\ : out STD_LOGIC;
    \s_count_reg[3]_1\ : out STD_LOGIC;
    \s_count_reg[2]_0\ : out STD_LOGIC;
    \s_counter_reg[1]_0\ : out STD_LOGIC;
    up_down_reg : out STD_LOGIC;
    \s_count_reg[3]_2\ : out STD_LOGIC;
    \s_counter_reg[1]_1\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[3]_3\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \s_count_reg[3]_4\ : out STD_LOGIC;
    last_enable : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    \s_count_reg[3]_5\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[0]\ : in STD_LOGIC;
    \segments_out_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \segments_out_reg[1]\ : in STD_LOGIC;
    \current_count0__20_carry\ : in STD_LOGIC;
    \current_count0__20_carry_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \current_count0__20_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \s_count_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decimal_counter_5 : entity is "decimal_counter";
end decimal_counter_5;

architecture STRUCTURE of decimal_counter_5 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^last_enable_reg\ : STD_LOGIC;
  signal \s_count[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \s_count[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \s_count[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \s_count[3]_i_2__1_n_0\ : STD_LOGIC;
  signal \s_count[3]_i_5_n_0\ : STD_LOGIC;
  signal \s_count[3]_i_6_n_0\ : STD_LOGIC;
  signal \s_count_reg_n_0_[2]\ : STD_LOGIC;
  signal \s_count_reg_n_0_[3]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \s_count[2]_i_3\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_count[3]_i_6\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \segments_out[2]_i_5\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \segments_out[4]_i_5\ : label is "soft_lutpair10";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  last_enable_reg <= \^last_enable_reg\;
\current_count0__20_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"57FFA800"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \s_count_reg_n_0_[2]\,
      I4 => \current_count0__20_carry__0\(3),
      O => \s_count_reg[3]_3\(3)
    );
\current_count0__20_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BD5542AA"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \s_count_reg_n_0_[2]\,
      I4 => \current_count0__20_carry__0\(2),
      O => \s_count_reg[3]_3\(2)
    );
\current_count0__20_carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B9D5462A"
    )
        port map (
      I0 => \s_count_reg_n_0_[2]\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \s_count_reg_n_0_[3]\,
      I4 => \current_count0__20_carry__0\(1),
      O => \s_count_reg[3]_3\(1)
    );
\current_count0__20_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69999666"
    )
        port map (
      I0 => \^q\(1),
      I1 => \s_count_reg_n_0_[3]\,
      I2 => \s_count_reg_n_0_[2]\,
      I3 => \^q\(0),
      I4 => \current_count0__20_carry__0\(0),
      O => \s_count_reg[3]_3\(0)
    );
\current_count0__20_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \s_count_reg_n_0_[2]\,
      I1 => \^q\(0),
      O => DI(0)
    );
\current_count0__20_carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^q\(0),
      I1 => \s_count_reg_n_0_[2]\,
      I2 => \current_count0__20_carry\,
      I3 => \current_count0__20_carry_0\(1),
      O => S(1)
    );
\current_count0__20_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \current_count0__20_carry_0\(0),
      O => S(0)
    );
\last_enable_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000801"
    )
        port map (
      I0 => s_up_down,
      I1 => \s_count_reg_n_0_[3]\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \s_count_reg_n_0_[2]\,
      I5 => \s_count_reg[3]_5\,
      O => s_overflow_1
    );
\s_count[0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BBBFFFFF"
    )
        port map (
      I0 => \s_count_reg[3]_5\,
      I1 => s_up_down,
      I2 => \^q\(1),
      I3 => \s_count_reg_n_0_[2]\,
      I4 => \s_count_reg_n_0_[3]\,
      I5 => \^q\(0),
      O => \s_count[0]_i_1__1_n_0\
    );
\s_count[1]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB04040404BBB0"
    )
        port map (
      I0 => \s_count_reg[3]_5\,
      I1 => s_up_down,
      I2 => \s_count_reg_n_0_[3]\,
      I3 => \s_count_reg_n_0_[2]\,
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \s_count[1]_i_1__1_n_0\
    );
\s_count[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBFBF04040000B0"
    )
        port map (
      I0 => \s_count_reg[3]_5\,
      I1 => s_up_down,
      I2 => \s_count_reg_n_0_[3]\,
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \s_count_reg_n_0_[2]\,
      O => \s_count[2]_i_1__1_n_0\
    );
\s_count[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFF7FE"
    )
        port map (
      I0 => s_up_down,
      I1 => \s_count_reg_n_0_[3]\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \s_count_reg_n_0_[2]\,
      O => up_down_reg
    );
\s_count[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^last_enable_reg\,
      O => E(0)
    );
\s_count[3]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B4B0B0B0B0B0B04B"
    )
        port map (
      I0 => \s_count_reg[3]_5\,
      I1 => s_up_down,
      I2 => \s_count_reg_n_0_[3]\,
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \s_count_reg_n_0_[2]\,
      O => \s_count[3]_i_2__1_n_0\
    );
\s_count[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEFFFEFAA"
    )
        port map (
      I0 => last_enable,
      I1 => \s_count[3]_i_5_n_0\,
      I2 => \s_count_reg_n_0_[3]\,
      I3 => s_up_down,
      I4 => \s_count[3]_i_6_n_0\,
      I5 => \s_count_reg[3]_5\,
      O => \^last_enable_reg\
    );
\s_count[3]_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => \s_count_reg[3]_5\,
      I1 => \s_count_reg_n_0_[3]\,
      I2 => \s_count[3]_i_5_n_0\,
      I3 => last_enable,
      I4 => s_up_down,
      O => \s_count_reg[3]_4\
    );
\s_count[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \s_count_reg_n_0_[2]\,
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \s_count[3]_i_5_n_0\
    );
\s_count[3]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \s_count_reg_n_0_[2]\,
      O => \s_count[3]_i_6_n_0\
    );
\s_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[0]_0\(0),
      CLR => reset_IBUF,
      D => \s_count[0]_i_1__1_n_0\,
      Q => \^q\(0)
    );
\s_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[0]_0\(0),
      CLR => reset_IBUF,
      D => \s_count[1]_i_1__1_n_0\,
      Q => \^q\(1)
    );
\s_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[0]_0\(0),
      CLR => reset_IBUF,
      D => \s_count[2]_i_1__1_n_0\,
      Q => \s_count_reg_n_0_[2]\
    );
\s_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[0]_0\(0),
      CLR => reset_IBUF,
      D => \s_count[3]_i_2__1_n_0\,
      Q => \s_count_reg_n_0_[3]\
    );
\segments_out[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF42050000"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \s_count_reg_n_0_[2]\,
      I4 => D(0),
      I5 => \segments_out_reg[0]\,
      O => \s_count_reg[3]_0\(0)
    );
\segments_out[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0280A220"
    )
        port map (
      I0 => D(0),
      I1 => \s_count_reg_n_0_[2]\,
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \s_count_reg_n_0_[3]\,
      I5 => \segments_out_reg[1]\,
      O => \s_count_reg[2]_0\
    );
\segments_out[2]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5170"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \s_count_reg_n_0_[2]\,
      O => \s_count_reg[3]_2\
    );
\segments_out[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000040040400"
    )
        port map (
      I0 => \segments_out_reg[3]\(1),
      I1 => \segments_out_reg[3]\(0),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \s_count_reg_n_0_[2]\,
      I5 => \s_count_reg_n_0_[3]\,
      O => \s_counter_reg[1]_1\
    );
\segments_out[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8098"
    )
        port map (
      I0 => \s_count_reg_n_0_[3]\,
      I1 => \s_count_reg_n_0_[2]\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => \s_count_reg[3]_1\
    );
\segments_out[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4404400004400000"
    )
        port map (
      I0 => \segments_out_reg[3]\(1),
      I1 => \segments_out_reg[3]\(0),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \s_count_reg_n_0_[2]\,
      I5 => \s_count_reg_n_0_[3]\,
      O => \s_counter_reg[1]\
    );
\segments_out[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440000000040040"
    )
        port map (
      I0 => \segments_out_reg[3]\(1),
      I1 => \segments_out_reg[3]\(0),
      I2 => \s_count_reg_n_0_[2]\,
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \s_count_reg_n_0_[3]\,
      O => \s_counter_reg[1]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decimal_counter_6 is
  port (
    count_out_OBUF : out STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    last_enable_reg : out STD_LOGIC;
    \s_counter_reg[0]\ : out STD_LOGIC;
    \s_count_reg[0]_0\ : out STD_LOGIC;
    \s_count_reg[3]_0\ : out STD_LOGIC;
    \s_count_reg[3]_1\ : out STD_LOGIC;
    \s_count_reg[0]_1\ : out STD_LOGIC;
    \s_count_reg[2]_0\ : out STD_LOGIC;
    \s_count_reg[1]_0\ : out STD_LOGIC;
    s_overflow_0 : out STD_LOGIC;
    p_0_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    last_enable_0 : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    \s_count_reg[0]_2\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    \s_count_reg[2]_1\ : in STD_LOGIC;
    \segments_out_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[6]\ : in STD_LOGIC;
    \segments_out_reg[3]\ : in STD_LOGIC;
    last_enable : in STD_LOGIC;
    s_count_enable : in STD_LOGIC;
    \s_count_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_15k_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decimal_counter_6 : entity is "decimal_counter";
end decimal_counter_6;

architecture STRUCTURE of decimal_counter_6 is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^count_out_obuf\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \count_out_OBUF[10]_inst_i_1_n_3\ : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_1_n_0\ : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_1_n_1\ : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_1_n_2\ : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_1_n_3\ : STD_LOGIC;
  signal \count_out_OBUF[8]_inst_i_1_n_0\ : STD_LOGIC;
  signal \count_out_OBUF[8]_inst_i_1_n_1\ : STD_LOGIC;
  signal \count_out_OBUF[8]_inst_i_1_n_2\ : STD_LOGIC;
  signal \count_out_OBUF[8]_inst_i_1_n_3\ : STD_LOGIC;
  signal last_enable_i_2_n_0 : STD_LOGIC;
  signal last_enable_i_3_n_0 : STD_LOGIC;
  signal \^last_enable_reg\ : STD_LOGIC;
  signal \s_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \s_count[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \s_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \s_count[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \NLW_count_out_OBUF[10]_inst_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_count_out_OBUF[10]_inst_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_out_OBUF[4]_inst_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of last_enable_i_2 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of last_enable_i_3 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \s_count[0]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \s_count[1]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \s_count[2]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \s_count[3]_i_2__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \segments_out[2]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \segments_out[4]_i_4\ : label is "soft_lutpair8";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  count_out_OBUF(9 downto 0) <= \^count_out_obuf\(9 downto 0);
  last_enable_reg <= \^last_enable_reg\;
\count_out_OBUF[10]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_out_OBUF[8]_inst_i_1_n_0\,
      CO(3 downto 1) => \NLW_count_out_OBUF[10]_inst_i_1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \count_out_OBUF[10]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_count_out_OBUF[10]_inst_i_1_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => \^count_out_obuf\(9 downto 8),
      S(3 downto 2) => B"00",
      S(1 downto 0) => p_0_in(6 downto 5)
    );
\count_out_OBUF[4]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_out_OBUF[4]_inst_i_1_n_0\,
      CO(2) => \count_out_OBUF[4]_inst_i_1_n_1\,
      CO(1) => \count_out_OBUF[4]_inst_i_1_n_2\,
      CO(0) => \count_out_OBUF[4]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^q\(2 downto 0),
      O(3 downto 1) => \^count_out_obuf\(3 downto 1),
      O(0) => \NLW_count_out_OBUF[4]_inst_i_1_O_UNCONNECTED\(0),
      S(3) => p_0_in(0),
      S(2 downto 0) => S(2 downto 0)
    );
\count_out_OBUF[8]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_out_OBUF[4]_inst_i_1_n_0\,
      CO(3) => \count_out_OBUF[8]_inst_i_1_n_0\,
      CO(2) => \count_out_OBUF[8]_inst_i_1_n_1\,
      CO(1) => \count_out_OBUF[8]_inst_i_1_n_2\,
      CO(0) => \count_out_OBUF[8]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^count_out_obuf\(7 downto 4),
      S(3 downto 0) => p_0_in(4 downto 1)
    );
\last_enable_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000101010"
    )
        port map (
      I0 => reset_IBUF,
      I1 => last_enable,
      I2 => s_count_enable,
      I3 => s_up_down,
      I4 => last_enable_i_2_n_0,
      I5 => last_enable_i_3_n_0,
      O => s_overflow_0
    );
last_enable_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^count_out_obuf\(0),
      O => last_enable_i_2_n_0
    );
last_enable_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^count_out_obuf\(0),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(1),
      O => last_enable_i_3_n_0
    );
\s_count[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01333333"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^count_out_obuf\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \s_count_reg[2]_1\,
      O => \s_count[0]_i_1__0_n_0\
    );
\s_count[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"550A0A54"
    )
        port map (
      I0 => \s_count_reg[2]_1\,
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => \^count_out_obuf\(0),
      I4 => \^q\(0),
      O => \s_count[1]_i_1__0_n_0\
    );
\s_count[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"57722004"
    )
        port map (
      I0 => \s_count_reg[2]_1\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^count_out_obuf\(0),
      I4 => \^q\(1),
      O => \s_count[2]_i_1__0_n_0\
    );
\s_count[3]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^last_enable_reg\,
      O => E(0)
    );
\s_count[3]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"64444449"
    )
        port map (
      I0 => \s_count_reg[2]_1\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^count_out_obuf\(0),
      I4 => \^q\(1),
      O => \s_count[3]_i_2__0_n_0\
    );
\s_count[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFAEE"
    )
        port map (
      I0 => last_enable_0,
      I1 => last_enable_i_3_n_0,
      I2 => last_enable_i_2_n_0,
      I3 => s_up_down,
      I4 => \s_count_reg[0]_2\,
      I5 => reset_IBUF,
      O => \^last_enable_reg\
    );
\s_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[3]_2\(0),
      CLR => reset_IBUF,
      D => \s_count[0]_i_1__0_n_0\,
      Q => \^count_out_obuf\(0)
    );
\s_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[3]_2\(0),
      CLR => reset_IBUF,
      D => \s_count[1]_i_1__0_n_0\,
      Q => \^q\(0)
    );
\s_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[3]_2\(0),
      CLR => reset_IBUF,
      D => \s_count[2]_i_1__0_n_0\,
      Q => \^q\(1)
    );
\s_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => \s_count_reg[3]_2\(0),
      CLR => reset_IBUF,
      D => \s_count[3]_i_2__0_n_0\,
      Q => \^q\(2)
    );
\segments_out[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100001000010001"
    )
        port map (
      I0 => \segments_out_reg[1]\(0),
      I1 => \segments_out_reg[1]\(1),
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^count_out_obuf\(0),
      I5 => \^q\(1),
      O => \s_counter_reg[0]\
    );
\segments_out[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000003590"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^count_out_obuf\(0),
      I3 => \^q\(0),
      I4 => \segments_out_reg[1]\(1),
      I5 => \segments_out_reg[1]\(0),
      O => \s_count_reg[2]_0\
    );
\segments_out[2]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02BA"
    )
        port map (
      I0 => \^count_out_obuf\(0),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      O => \s_count_reg[0]_0\
    );
\segments_out[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFC1040000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^count_out_obuf\(0),
      I4 => D(0),
      I5 => \segments_out_reg[3]\,
      O => \s_count_reg[3]_1\
    );
\segments_out[4]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D004"
    )
        port map (
      I0 => \^count_out_obuf\(0),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      O => \s_count_reg[0]_1\
    );
\segments_out[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000B860"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^count_out_obuf\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \segments_out_reg[1]\(1),
      I5 => \segments_out_reg[1]\(0),
      O => \s_count_reg[1]_0\
    );
\segments_out[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF08028200"
    )
        port map (
      I0 => D(0),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^count_out_obuf\(0),
      I4 => \^q\(1),
      I5 => \segments_out_reg[6]\,
      O => \s_count_reg[3]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decimal_counter__parameterized1\ is
  port (
    O : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[2]_0\ : out STD_LOGIC;
    \s_count_reg[1]_0\ : out STD_LOGIC;
    \s_count_reg[0]_0\ : out STD_LOGIC;
    \s_count_reg[0]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \s_count_reg[2]_1\ : out STD_LOGIC;
    \s_count_reg[1]_1\ : out STD_LOGIC;
    \s_count_reg[2]_2\ : out STD_LOGIC;
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \s_count_reg[1]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[0]\ : in STD_LOGIC;
    \segments_out_reg[1]\ : in STD_LOGIC;
    \segments_out_reg[0]_0\ : in STD_LOGIC;
    \segments_out_reg[6]\ : in STD_LOGIC;
    \segments_out_reg[6]_0\ : in STD_LOGIC;
    \segments_out_reg[3]\ : in STD_LOGIC;
    \segments_out_reg[3]_0\ : in STD_LOGIC;
    \segments_out_reg[1]_0\ : in STD_LOGIC;
    \segments_out_reg[1]_1\ : in STD_LOGIC;
    \segments_out_reg[5]\ : in STD_LOGIC;
    \segments_out_reg[5]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[4]\ : in STD_LOGIC;
    \segments_out_reg[2]\ : in STD_LOGIC;
    \current_count0_carry__0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_up_down : in STD_LOGIC;
    \s_count_reg[0]_2\ : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decimal_counter__parameterized1\ : entity is "decimal_counter";
end \decimal_counter__parameterized1\;

architecture STRUCTURE of \decimal_counter__parameterized1\ is
  signal \current_count0_carry__0_i_5_n_3\ : STD_LOGIC;
  signal \current_count0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \s_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \s_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \s_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \^s_count_reg[0]_0\ : STD_LOGIC;
  signal \^s_count_reg[1]_0\ : STD_LOGIC;
  signal \s_count_reg_n_0_[2]\ : STD_LOGIC;
  signal \NLW_current_count0_carry__0_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_current_count0_carry__0_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \s_count[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_count[2]_i_1\ : label is "soft_lutpair13";
begin
  \s_count_reg[0]_0\ <= \^s_count_reg[0]_0\;
  \s_count_reg[1]_0\ <= \^s_count_reg[1]_0\;
\current_count0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \current_count0_carry__0\(0),
      I1 => CO(0),
      O => \s_count_reg[1]_2\(0)
    );
\current_count0_carry__0_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 1) => \NLW_current_count0_carry__0_i_5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \current_count0_carry__0_i_5_n_3\,
      CYINIT => CO(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_current_count0_carry__0_i_5_O_UNCONNECTED\(3 downto 2),
      O(1) => O(0),
      O(0) => \NLW_current_count0_carry__0_i_5_O_UNCONNECTED\(0),
      S(3 downto 2) => B"00",
      S(1) => \current_count0_carry__0_i_6_n_0\,
      S(0) => '1'
    );
\current_count0_carry__0_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^s_count_reg[0]_0\,
      O => \current_count0_carry__0_i_6_n_0\
    );
\current_count1__0_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \s_count_reg_n_0_[2]\,
      I1 => \^s_count_reg[0]_0\,
      O => DI(0)
    );
\current_count1__0_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \s_count_reg_n_0_[2]\,
      O => S(2)
    );
\current_count1__0_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \^s_count_reg[0]_0\,
      I1 => \s_count_reg_n_0_[2]\,
      I2 => \^s_count_reg[1]_0\,
      O => S(1)
    );
\current_count1__0_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^s_count_reg[0]_0\,
      I1 => \s_count_reg_n_0_[2]\,
      O => S(0)
    );
\s_count[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00001F"
    )
        port map (
      I0 => \^s_count_reg[1]_0\,
      I1 => \s_count_reg_n_0_[2]\,
      I2 => s_up_down,
      I3 => \s_count_reg[0]_2\,
      I4 => \^s_count_reg[0]_0\,
      O => \s_count[0]_i_1_n_0\
    );
\s_count[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF440010"
    )
        port map (
      I0 => s_up_down,
      I1 => \^s_count_reg[0]_0\,
      I2 => \s_count_reg_n_0_[2]\,
      I3 => \s_count_reg[0]_2\,
      I4 => \^s_count_reg[1]_0\,
      O => \s_count[1]_i_1_n_0\
    );
\s_count[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF540000"
    )
        port map (
      I0 => s_up_down,
      I1 => \^s_count_reg[1]_0\,
      I2 => \^s_count_reg[0]_0\,
      I3 => \s_count_reg[0]_2\,
      I4 => \s_count_reg_n_0_[2]\,
      O => \s_count[2]_i_1_n_0\
    );
\s_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_count[0]_i_1_n_0\,
      Q => \^s_count_reg[0]_0\
    );
\s_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_count[1]_i_1_n_0\,
      Q => \^s_count_reg[1]_0\
    );
\s_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_15k_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_count[2]_i_1_n_0\,
      Q => \s_count_reg_n_0_[2]\
    );
\segments_out[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEAAEAAAE"
    )
        port map (
      I0 => \segments_out_reg[0]\,
      I1 => \segments_out_reg[1]\,
      I2 => \s_count_reg_n_0_[2]\,
      I3 => \^s_count_reg[1]_0\,
      I4 => \^s_count_reg[0]_0\,
      I5 => \segments_out_reg[0]_0\,
      O => \s_count_reg[2]_0\
    );
\segments_out[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF80A8"
    )
        port map (
      I0 => \segments_out_reg[1]\,
      I1 => \^s_count_reg[0]_0\,
      I2 => \^s_count_reg[1]_0\,
      I3 => \s_count_reg_n_0_[2]\,
      I4 => \segments_out_reg[1]_0\,
      I5 => \segments_out_reg[1]_1\,
      O => \s_count_reg[0]_1\(0)
    );
\segments_out[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F2FFF200"
    )
        port map (
      I0 => \s_count_reg_n_0_[2]\,
      I1 => \^s_count_reg[1]_0\,
      I2 => \^s_count_reg[0]_0\,
      I3 => Q(0),
      I4 => \segments_out_reg[2]\,
      O => \s_count_reg[2]_2\
    );
\segments_out[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F00FE0F0EEEEEEEE"
    )
        port map (
      I0 => \segments_out_reg[3]\,
      I1 => \segments_out_reg[3]_0\,
      I2 => \^s_count_reg[0]_0\,
      I3 => \^s_count_reg[1]_0\,
      I4 => \s_count_reg_n_0_[2]\,
      I5 => \segments_out_reg[1]\,
      O => \s_count_reg[0]_1\(1)
    );
\segments_out[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02FF0200"
    )
        port map (
      I0 => \^s_count_reg[1]_0\,
      I1 => \s_count_reg_n_0_[2]\,
      I2 => \^s_count_reg[0]_0\,
      I3 => Q(0),
      I4 => \segments_out_reg[4]\,
      O => \s_count_reg[1]_1\
    );
\segments_out[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAEAAEAAA"
    )
        port map (
      I0 => \segments_out_reg[5]\,
      I1 => \s_count_reg_n_0_[2]\,
      I2 => \^s_count_reg[1]_0\,
      I3 => \segments_out_reg[1]\,
      I4 => \^s_count_reg[0]_0\,
      I5 => \segments_out_reg[5]_0\,
      O => \s_count_reg[2]_1\
    );
\segments_out[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAAAAEEA"
    )
        port map (
      I0 => \segments_out_reg[6]\,
      I1 => \segments_out_reg[1]\,
      I2 => \^s_count_reg[0]_0\,
      I3 => \s_count_reg_n_0_[2]\,
      I4 => \^s_count_reg[1]_0\,
      I5 => \segments_out_reg[6]_0\,
      O => \s_count_reg[0]_1\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decoder is
  port (
    s_up_down : out STD_LOGIC;
    s_count_enable : out STD_LOGIC;
    up_down_reg_0 : out STD_LOGIC;
    count_enable_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    enc_b_IBUF : in STD_LOGIC;
    CLK : in STD_LOGIC;
    last_enable : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decoder;

architecture STRUCTURE of decoder is
  signal count_enable_i_1_n_0 : STD_LOGIC;
  signal \^s_count_enable\ : STD_LOGIC;
  signal s_step_counter : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \s_step_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \s_step_counter[1]_i_1_n_0\ : STD_LOGIC;
  signal \^s_up_down\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of count_enable_i_1 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_count[3]_i_1__1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_step_counter[0]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_step_counter[1]_i_1\ : label is "soft_lutpair15";
begin
  s_count_enable <= \^s_count_enable\;
  s_up_down <= \^s_up_down\;
count_enable_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F808"
    )
        port map (
      I0 => s_step_counter(0),
      I1 => s_step_counter(1),
      I2 => AR(0),
      I3 => \^s_count_enable\,
      O => count_enable_i_1_n_0
    );
count_enable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => count_enable_i_1_n_0,
      Q => \^s_count_enable\,
      R => '0'
    );
\s_count[3]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^s_count_enable\,
      I1 => last_enable,
      O => count_enable_reg_0(0)
    );
\s_count[3]_i_3__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_up_down\,
      I1 => \^s_count_enable\,
      I2 => last_enable,
      O => up_down_reg_0
    );
\s_step_counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_step_counter(0),
      O => \s_step_counter[0]_i_1_n_0\
    );
\s_step_counter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_step_counter(0),
      I1 => s_step_counter(1),
      O => \s_step_counter[1]_i_1_n_0\
    );
\s_step_counter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \s_step_counter[0]_i_1_n_0\,
      Q => s_step_counter(0)
    );
\s_step_counter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \s_step_counter[1]_i_1_n_0\,
      Q => s_step_counter(1)
    );
up_down_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => enc_b_IBUF,
      Q => \^s_up_down\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mux_displays is
  port (
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_counter_reg[0]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \segments_out_reg[6]_0\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \s_anodes_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \segments_out_reg[2]_0\ : in STD_LOGIC;
    \segments_out_reg[2]_1\ : in STD_LOGIC;
    \segments_out_reg[4]_0\ : in STD_LOGIC;
    \segments_out_reg[4]_1\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \segments_out_reg[6]_1\ : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
end mux_displays;

architecture STRUCTURE of mux_displays is
  signal \^d\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \s_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \s_counter[1]_i_1_n_0\ : STD_LOGIC;
  signal \^s_counter_reg[0]_0\ : STD_LOGIC;
  signal \segments_out_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \segments_out_reg[4]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \s_anodes[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \s_anodes[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \s_anodes[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \s_anodes[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \s_counter[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \s_counter[1]_i_1\ : label is "soft_lutpair0";
begin
  D(2 downto 0) <= \^d\(2 downto 0);
  E(0) <= \^e\(0);
  Q(1 downto 0) <= \^q\(1 downto 0);
  \s_counter_reg[0]_0\ <= \^s_counter_reg[0]_0\;
\s_anodes[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \^d\(0)
    );
\s_anodes[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \^d\(1)
    );
\s_anodes[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \^d\(2)
    );
\s_anodes[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \^s_counter_reg[0]_0\
    );
\s_anodes_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => reset_IBUF,
      D => \^d\(0),
      Q => \s_anodes_reg[3]_0\(0)
    );
\s_anodes_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => reset_IBUF,
      D => \^d\(1),
      Q => \s_anodes_reg[3]_0\(1)
    );
\s_anodes_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => reset_IBUF,
      D => \^d\(2),
      Q => \s_anodes_reg[3]_0\(2)
    );
\s_anodes_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => reset_IBUF,
      D => \^s_counter_reg[0]_0\,
      Q => \s_anodes_reg[3]_0\(3)
    );
\s_counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \s_counter[0]_i_1_n_0\
    );
\s_counter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \s_counter[1]_i_1_n_0\
    );
\s_counter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_counter[0]_i_1_n_0\,
      Q => \^q\(0)
    );
\s_counter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => reset_IBUF,
      D => \s_counter[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\segments_out[6]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^e\(0)
    );
\segments_out_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[6]_1\(0),
      Q => \segments_out_reg[6]_0\(0),
      R => '0'
    );
\segments_out_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[6]_1\(1),
      Q => \segments_out_reg[6]_0\(1),
      R => '0'
    );
\segments_out_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[2]_i_1_n_0\,
      Q => \segments_out_reg[6]_0\(2),
      R => '0'
    );
\segments_out_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \segments_out_reg[2]_0\,
      I1 => \segments_out_reg[2]_1\,
      O => \segments_out_reg[2]_i_1_n_0\,
      S => \^q\(0)
    );
\segments_out_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[6]_1\(2),
      Q => \segments_out_reg[6]_0\(3),
      R => '0'
    );
\segments_out_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[4]_i_1_n_0\,
      Q => \segments_out_reg[6]_0\(4),
      R => '0'
    );
\segments_out_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \segments_out_reg[4]_0\,
      I1 => \segments_out_reg[4]_1\,
      O => \segments_out_reg[4]_i_1_n_0\,
      S => \^q\(0)
    );
\segments_out_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[6]_1\(3),
      Q => \segments_out_reg[6]_0\(5),
      R => '0'
    );
\segments_out_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \^e\(0),
      D => \segments_out_reg[6]_1\(4),
      Q => \segments_out_reg[6]_0\(6),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity counter_7seg is
  port (
    count_out_OBUF : out STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    last_enable_reg : out STD_LOGIC;
    \s_counter_reg[0]\ : out STD_LOGIC;
    \s_count_reg[0]\ : out STD_LOGIC;
    \s_count_reg[3]\ : out STD_LOGIC;
    \s_count_reg[3]_0\ : out STD_LOGIC;
    \s_count_reg[0]_0\ : out STD_LOGIC;
    \s_count_reg[2]\ : out STD_LOGIC;
    \s_count_reg[1]\ : out STD_LOGIC;
    s_overflow_0 : out STD_LOGIC;
    p_0_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    last_enable_0 : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    \s_count_reg[0]_1\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    \s_count_reg[2]_0\ : in STD_LOGIC;
    \segments_out_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[6]\ : in STD_LOGIC;
    \segments_out_reg[3]\ : in STD_LOGIC;
    last_enable : in STD_LOGIC;
    s_count_enable : in STD_LOGIC;
    \s_count_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_15k_IBUF_BUFG : in STD_LOGIC
  );
end counter_7seg;

architecture STRUCTURE of counter_7seg is
begin
counter: entity work.decimal_counter_6
     port map (
      D(0) => D(0),
      E(0) => E(0),
      Q(2 downto 0) => Q(2 downto 0),
      S(2 downto 0) => S(2 downto 0),
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      count_out_OBUF(9 downto 0) => count_out_OBUF(9 downto 0),
      last_enable => last_enable,
      last_enable_0 => last_enable_0,
      last_enable_reg => last_enable_reg,
      p_0_in(6 downto 0) => p_0_in(6 downto 0),
      reset_IBUF => reset_IBUF,
      s_count_enable => s_count_enable,
      \s_count_reg[0]_0\ => \s_count_reg[0]\,
      \s_count_reg[0]_1\ => \s_count_reg[0]_0\,
      \s_count_reg[0]_2\ => \s_count_reg[0]_1\,
      \s_count_reg[1]_0\ => \s_count_reg[1]\,
      \s_count_reg[2]_0\ => \s_count_reg[2]\,
      \s_count_reg[2]_1\ => \s_count_reg[2]_0\,
      \s_count_reg[3]_0\ => \s_count_reg[3]\,
      \s_count_reg[3]_1\ => \s_count_reg[3]_0\,
      \s_count_reg[3]_2\(0) => \s_count_reg[3]_1\(0),
      \s_counter_reg[0]\ => \s_counter_reg[0]\,
      s_overflow_0 => s_overflow_0,
      s_up_down => s_up_down,
      \segments_out_reg[1]\(1 downto 0) => \segments_out_reg[1]\(1 downto 0),
      \segments_out_reg[3]\ => \segments_out_reg[3]\,
      \segments_out_reg[6]\ => \segments_out_reg[6]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity counter_7seg_3 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    last_enable_reg : out STD_LOGIC;
    s_overflow_1 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \s_count_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_counter_reg[1]\ : out STD_LOGIC;
    \s_count_reg[3]_0\ : out STD_LOGIC;
    \s_count_reg[2]\ : out STD_LOGIC;
    \s_counter_reg[1]_0\ : out STD_LOGIC;
    up_down_reg : out STD_LOGIC;
    \s_count_reg[3]_1\ : out STD_LOGIC;
    \s_counter_reg[1]_1\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[3]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \s_count_reg[3]_3\ : out STD_LOGIC;
    last_enable : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    \s_count_reg[3]_4\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[0]\ : in STD_LOGIC;
    \segments_out_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \segments_out_reg[1]\ : in STD_LOGIC;
    \current_count0__20_carry\ : in STD_LOGIC;
    \current_count0__20_carry_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \current_count0__20_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \s_count_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of counter_7seg_3 : entity is "counter_7seg";
end counter_7seg_3;

architecture STRUCTURE of counter_7seg_3 is
begin
counter: entity work.decimal_counter_5
     port map (
      D(0) => D(0),
      DI(0) => DI(0),
      E(0) => E(0),
      Q(1 downto 0) => Q(1 downto 0),
      S(1 downto 0) => S(1 downto 0),
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      \current_count0__20_carry\ => \current_count0__20_carry\,
      \current_count0__20_carry_0\(1 downto 0) => \current_count0__20_carry_0\(1 downto 0),
      \current_count0__20_carry__0\(3 downto 0) => \current_count0__20_carry__0\(3 downto 0),
      last_enable => last_enable,
      last_enable_reg => last_enable_reg,
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]_0\(0) => \s_count_reg[0]\(0),
      \s_count_reg[2]_0\ => \s_count_reg[2]\,
      \s_count_reg[3]_0\(0) => \s_count_reg[3]\(0),
      \s_count_reg[3]_1\ => \s_count_reg[3]_0\,
      \s_count_reg[3]_2\ => \s_count_reg[3]_1\,
      \s_count_reg[3]_3\(3 downto 0) => \s_count_reg[3]_2\(3 downto 0),
      \s_count_reg[3]_4\ => \s_count_reg[3]_3\,
      \s_count_reg[3]_5\ => \s_count_reg[3]_4\,
      \s_counter_reg[1]\ => \s_counter_reg[1]\,
      \s_counter_reg[1]_0\ => \s_counter_reg[1]_0\,
      \s_counter_reg[1]_1\ => \s_counter_reg[1]_1\,
      s_overflow_1 => s_overflow_1,
      s_up_down => s_up_down,
      \segments_out_reg[0]\ => \segments_out_reg[0]\,
      \segments_out_reg[1]\ => \segments_out_reg[1]\,
      \segments_out_reg[3]\(1 downto 0) => \segments_out_reg[3]\(1 downto 0),
      up_down_reg => up_down_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity counter_7seg_4 is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    s_overflow_2 : out STD_LOGIC;
    \s_count_reg[2]\ : out STD_LOGIC;
    \s_count_reg[3]\ : out STD_LOGIC;
    \s_count_reg[3]_0\ : out STD_LOGIC;
    \s_count_reg[3]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_counter_reg[0]\ : out STD_LOGIC;
    \s_counter_reg[0]_0\ : out STD_LOGIC;
    \s_count_reg[1]\ : out STD_LOGIC;
    \s_count_reg[1]_0\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[1]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \s_count_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \s_count_reg[2]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[0]\ : in STD_LOGIC;
    last_enable_reg : in STD_LOGIC;
    \segments_out_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \segments_out_reg[2]\ : in STD_LOGIC;
    \segments_out_reg[4]_0\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[5]\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    current_count0_carry : in STD_LOGIC;
    current_count0_carry_0 : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_15k_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of counter_7seg_4 : entity is "counter_7seg";
end counter_7seg_4;

architecture STRUCTURE of counter_7seg_4 is
begin
counter: entity work.decimal_counter
     port map (
      CO(0) => CO(0),
      D(0) => D(0),
      DI(0) => DI(0),
      E(0) => E(0),
      O(0) => O(0),
      Q(2 downto 0) => Q(2 downto 0),
      S(1 downto 0) => S(1 downto 0),
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      current_count0_carry => current_count0_carry,
      current_count0_carry_0 => current_count0_carry_0,
      last_enable_reg => last_enable_reg,
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]_0\ => \s_count_reg[0]\,
      \s_count_reg[1]_0\ => \s_count_reg[1]\,
      \s_count_reg[1]_1\ => \s_count_reg[1]_0\,
      \s_count_reg[1]_2\(3 downto 0) => \s_count_reg[1]_1\(3 downto 0),
      \s_count_reg[2]_0\ => \s_count_reg[2]\,
      \s_count_reg[2]_1\(1 downto 0) => \s_count_reg[2]_0\(1 downto 0),
      \s_count_reg[2]_2\(0) => \s_count_reg[2]_1\(0),
      \s_count_reg[3]_0\ => \s_count_reg[3]\,
      \s_count_reg[3]_1\ => \s_count_reg[3]_0\,
      \s_count_reg[3]_2\(0) => \s_count_reg[3]_1\(0),
      \s_counter_reg[0]\ => \s_counter_reg[0]\,
      \s_counter_reg[0]_0\ => \s_counter_reg[0]_0\,
      s_overflow_2 => s_overflow_2,
      s_up_down => s_up_down,
      \segments_out_reg[2]\ => \segments_out_reg[2]\,
      \segments_out_reg[4]\(1 downto 0) => \segments_out_reg[4]\(1 downto 0),
      \segments_out_reg[4]_0\ => \segments_out_reg[4]_0\,
      \segments_out_reg[5]\ => \segments_out_reg[5]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \counter_7seg__parameterized2\ is
  port (
    O : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s_count_reg[2]\ : out STD_LOGIC;
    \s_count_reg[1]\ : out STD_LOGIC;
    \s_count_reg[0]\ : out STD_LOGIC;
    \s_count_reg[0]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \s_count_reg[2]_0\ : out STD_LOGIC;
    \s_count_reg[1]_0\ : out STD_LOGIC;
    \s_count_reg[2]_1\ : out STD_LOGIC;
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \s_count_reg[1]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[0]\ : in STD_LOGIC;
    \segments_out_reg[1]\ : in STD_LOGIC;
    \segments_out_reg[0]_0\ : in STD_LOGIC;
    \segments_out_reg[6]\ : in STD_LOGIC;
    \segments_out_reg[6]_0\ : in STD_LOGIC;
    \segments_out_reg[3]\ : in STD_LOGIC;
    \segments_out_reg[3]_0\ : in STD_LOGIC;
    \segments_out_reg[1]_0\ : in STD_LOGIC;
    \segments_out_reg[1]_1\ : in STD_LOGIC;
    \segments_out_reg[5]\ : in STD_LOGIC;
    \segments_out_reg[5]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \segments_out_reg[4]\ : in STD_LOGIC;
    \segments_out_reg[2]\ : in STD_LOGIC;
    \current_count0_carry__0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_up_down : in STD_LOGIC;
    \s_count_reg[0]_1\ : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \counter_7seg__parameterized2\ : entity is "counter_7seg";
end \counter_7seg__parameterized2\;

architecture STRUCTURE of \counter_7seg__parameterized2\ is
begin
counter: entity work.\decimal_counter__parameterized1\
     port map (
      CO(0) => CO(0),
      DI(0) => DI(0),
      O(0) => O(0),
      Q(0) => Q(0),
      S(2 downto 0) => S(2 downto 0),
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      \current_count0_carry__0\(0) => \current_count0_carry__0\(0),
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]_0\ => \s_count_reg[0]\,
      \s_count_reg[0]_1\(2 downto 0) => \s_count_reg[0]_0\(2 downto 0),
      \s_count_reg[0]_2\ => \s_count_reg[0]_1\,
      \s_count_reg[1]_0\ => \s_count_reg[1]\,
      \s_count_reg[1]_1\ => \s_count_reg[1]_0\,
      \s_count_reg[1]_2\(0) => \s_count_reg[1]_1\(0),
      \s_count_reg[2]_0\ => \s_count_reg[2]\,
      \s_count_reg[2]_1\ => \s_count_reg[2]_0\,
      \s_count_reg[2]_2\ => \s_count_reg[2]_1\,
      s_up_down => s_up_down,
      \segments_out_reg[0]\ => \segments_out_reg[0]\,
      \segments_out_reg[0]_0\ => \segments_out_reg[0]_0\,
      \segments_out_reg[1]\ => \segments_out_reg[1]\,
      \segments_out_reg[1]_0\ => \segments_out_reg[1]_0\,
      \segments_out_reg[1]_1\ => \segments_out_reg[1]_1\,
      \segments_out_reg[2]\ => \segments_out_reg[2]\,
      \segments_out_reg[3]\ => \segments_out_reg[3]\,
      \segments_out_reg[3]_0\ => \segments_out_reg[3]_0\,
      \segments_out_reg[4]\ => \segments_out_reg[4]\,
      \segments_out_reg[5]\ => \segments_out_reg[5]\,
      \segments_out_reg[5]_0\ => \segments_out_reg[5]_0\,
      \segments_out_reg[6]\ => \segments_out_reg[6]\,
      \segments_out_reg[6]_0\ => \segments_out_reg[6]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity full_counter is
  port (
    last_enable : out STD_LOGIC;
    count_out_OBUF : out STD_LOGIC_VECTOR ( 10 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \s_anodes_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_count_enable : in STD_LOGIC;
    clk_15k_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    s_up_down : in STD_LOGIC;
    \s_count_reg[2]\ : in STD_LOGIC;
    \s_count_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end full_counter;

architecture STRUCTURE of full_counter is
  signal C : STD_LOGIC_VECTOR ( 10 downto 4 );
  signal clk : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_2_n_0\ : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_3_n_0\ : STD_LOGIC;
  signal \count_out_OBUF[4]_inst_i_4_n_0\ : STD_LOGIC;
  signal \current_count0__20_carry__0_n_0\ : STD_LOGIC;
  signal \current_count0__20_carry__0_n_1\ : STD_LOGIC;
  signal \current_count0__20_carry__0_n_2\ : STD_LOGIC;
  signal \current_count0__20_carry__0_n_3\ : STD_LOGIC;
  signal \current_count0__20_carry__1_n_2\ : STD_LOGIC;
  signal \current_count0__20_carry__1_n_3\ : STD_LOGIC;
  signal \current_count0__20_carry_n_0\ : STD_LOGIC;
  signal \current_count0__20_carry_n_1\ : STD_LOGIC;
  signal \current_count0__20_carry_n_2\ : STD_LOGIC;
  signal \current_count0__20_carry_n_3\ : STD_LOGIC;
  signal \current_count0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \current_count0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \current_count0_carry__0_n_1\ : STD_LOGIC;
  signal \current_count0_carry__0_n_2\ : STD_LOGIC;
  signal \current_count0_carry__0_n_3\ : STD_LOGIC;
  signal current_count0_carry_i_1_n_0 : STD_LOGIC;
  signal current_count0_carry_i_2_n_0 : STD_LOGIC;
  signal current_count0_carry_n_0 : STD_LOGIC;
  signal current_count0_carry_n_1 : STD_LOGIC;
  signal current_count0_carry_n_2 : STD_LOGIC;
  signal current_count0_carry_n_3 : STD_LOGIC;
  signal current_count1 : STD_LOGIC_VECTOR ( 10 downto 5 );
  signal \current_count1__0_carry_n_0\ : STD_LOGIC;
  signal \current_count1__0_carry_n_2\ : STD_LOGIC;
  signal \current_count1__0_carry_n_3\ : STD_LOGIC;
  signal current_count2 : STD_LOGIC_VECTOR ( 10 downto 5 );
  signal \current_count2__0_carry__0_n_3\ : STD_LOGIC;
  signal \current_count2__0_carry_n_0\ : STD_LOGIC;
  signal \current_count2__0_carry_n_1\ : STD_LOGIC;
  signal \current_count2__0_carry_n_2\ : STD_LOGIC;
  signal \current_count2__0_carry_n_3\ : STD_LOGIC;
  signal current_count3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal display_mux_n_3 : STD_LOGIC;
  signal display_mux_n_4 : STD_LOGIC;
  signal display_mux_n_5 : STD_LOGIC;
  signal display_mux_n_6 : STD_LOGIC;
  signal \generador_decider[0].updown_decider_n_1\ : STD_LOGIC;
  signal \generador_decider[3].updown_decider_n_0\ : STD_LOGIC;
  signal \generador_display[0].display_n_10\ : STD_LOGIC;
  signal \generador_display[0].display_n_11\ : STD_LOGIC;
  signal \generador_display[0].display_n_12\ : STD_LOGIC;
  signal \generador_display[0].display_n_13\ : STD_LOGIC;
  signal \generador_display[0].display_n_14\ : STD_LOGIC;
  signal \generador_display[0].display_n_15\ : STD_LOGIC;
  signal \generador_display[0].display_n_16\ : STD_LOGIC;
  signal \generador_display[0].display_n_17\ : STD_LOGIC;
  signal \generador_display[0].display_n_18\ : STD_LOGIC;
  signal \generador_display[0].display_n_19\ : STD_LOGIC;
  signal \generador_display[0].display_n_20\ : STD_LOGIC;
  signal \generador_display[0].display_n_21\ : STD_LOGIC;
  signal \generador_display[1].display_n_0\ : STD_LOGIC;
  signal \generador_display[1].display_n_1\ : STD_LOGIC;
  signal \generador_display[1].display_n_10\ : STD_LOGIC;
  signal \generador_display[1].display_n_11\ : STD_LOGIC;
  signal \generador_display[1].display_n_12\ : STD_LOGIC;
  signal \generador_display[1].display_n_13\ : STD_LOGIC;
  signal \generador_display[1].display_n_14\ : STD_LOGIC;
  signal \generador_display[1].display_n_16\ : STD_LOGIC;
  signal \generador_display[1].display_n_17\ : STD_LOGIC;
  signal \generador_display[1].display_n_18\ : STD_LOGIC;
  signal \generador_display[1].display_n_19\ : STD_LOGIC;
  signal \generador_display[1].display_n_20\ : STD_LOGIC;
  signal \generador_display[1].display_n_3\ : STD_LOGIC;
  signal \generador_display[1].display_n_4\ : STD_LOGIC;
  signal \generador_display[1].display_n_5\ : STD_LOGIC;
  signal \generador_display[1].display_n_6\ : STD_LOGIC;
  signal \generador_display[1].display_n_7\ : STD_LOGIC;
  signal \generador_display[1].display_n_8\ : STD_LOGIC;
  signal \generador_display[1].display_n_9\ : STD_LOGIC;
  signal \generador_display[2].display_n_0\ : STD_LOGIC;
  signal \generador_display[2].display_n_1\ : STD_LOGIC;
  signal \generador_display[2].display_n_10\ : STD_LOGIC;
  signal \generador_display[2].display_n_11\ : STD_LOGIC;
  signal \generador_display[2].display_n_12\ : STD_LOGIC;
  signal \generador_display[2].display_n_13\ : STD_LOGIC;
  signal \generador_display[2].display_n_14\ : STD_LOGIC;
  signal \generador_display[2].display_n_15\ : STD_LOGIC;
  signal \generador_display[2].display_n_16\ : STD_LOGIC;
  signal \generador_display[2].display_n_17\ : STD_LOGIC;
  signal \generador_display[2].display_n_18\ : STD_LOGIC;
  signal \generador_display[2].display_n_19\ : STD_LOGIC;
  signal \generador_display[2].display_n_2\ : STD_LOGIC;
  signal \generador_display[2].display_n_20\ : STD_LOGIC;
  signal \generador_display[2].display_n_21\ : STD_LOGIC;
  signal \generador_display[2].display_n_4\ : STD_LOGIC;
  signal \generador_display[2].display_n_5\ : STD_LOGIC;
  signal \generador_display[2].display_n_6\ : STD_LOGIC;
  signal \generador_display[2].display_n_7\ : STD_LOGIC;
  signal \generador_display[2].display_n_8\ : STD_LOGIC;
  signal \generador_display[2].display_n_9\ : STD_LOGIC;
  signal last_display_n_1 : STD_LOGIC;
  signal last_display_n_10 : STD_LOGIC;
  signal last_display_n_11 : STD_LOGIC;
  signal last_display_n_12 : STD_LOGIC;
  signal last_display_n_13 : STD_LOGIC;
  signal last_display_n_14 : STD_LOGIC;
  signal last_display_n_2 : STD_LOGIC;
  signal last_display_n_3 : STD_LOGIC;
  signal last_display_n_4 : STD_LOGIC;
  signal last_display_n_5 : STD_LOGIC;
  signal last_display_n_6 : STD_LOGIC;
  signal last_display_n_7 : STD_LOGIC;
  signal last_display_n_8 : STD_LOGIC;
  signal last_display_n_9 : STD_LOGIC;
  signal \^last_enable\ : STD_LOGIC;
  signal last_enable_0 : STD_LOGIC;
  signal last_enable_1 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 10 downto 1 );
  signal s_counter : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_overflow_0 : STD_LOGIC;
  signal s_overflow_1 : STD_LOGIC;
  signal s_overflow_2 : STD_LOGIC;
  signal \NLW_current_count0__20_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_current_count0__20_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_current_count0__20_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_current_count0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_current_count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_current_count1__0_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_current_count1__0_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_current_count2__0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_current_count2__0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
begin
  last_enable <= \^last_enable\;
\count_out_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \generador_display[0].display_n_12\,
      I1 => p_0_in(1),
      O => count_out_OBUF(1)
    );
\count_out_OBUF[4]_inst_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \generador_display[0].display_n_10\,
      I1 => p_0_in(3),
      O => \count_out_OBUF[4]_inst_i_2_n_0\
    );
\count_out_OBUF[4]_inst_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \generador_display[0].display_n_11\,
      I1 => p_0_in(2),
      O => \count_out_OBUF[4]_inst_i_3_n_0\
    );
\count_out_OBUF[4]_inst_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \generador_display[0].display_n_12\,
      I1 => p_0_in(1),
      O => \count_out_OBUF[4]_inst_i_4_n_0\
    );
\current_count0__20_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \current_count0__20_carry_n_0\,
      CO(2) => \current_count0__20_carry_n_1\,
      CO(1) => \current_count0__20_carry_n_2\,
      CO(0) => \current_count0__20_carry_n_3\,
      CYINIT => '0',
      DI(3) => current_count3(3),
      DI(2) => \generador_display[1].display_n_3\,
      DI(1) => \generador_display[1].display_n_4\,
      DI(0) => '0',
      O(3 downto 1) => p_0_in(3 downto 1),
      O(0) => \NLW_current_count0__20_carry_O_UNCONNECTED\(0),
      S(3) => \generador_display[1].display_n_13\,
      S(2) => \generador_display[1].display_n_14\,
      S(1) => \generador_display[1].display_n_4\,
      S(0) => '0'
    );
\current_count0__20_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_count0__20_carry_n_0\,
      CO(3) => \current_count0__20_carry__0_n_0\,
      CO(2) => \current_count0__20_carry__0_n_1\,
      CO(1) => \current_count0__20_carry__0_n_2\,
      CO(0) => \current_count0__20_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => C(7 downto 4),
      O(3 downto 0) => p_0_in(7 downto 4),
      S(3) => \generador_display[1].display_n_16\,
      S(2) => \generador_display[1].display_n_17\,
      S(1) => \generador_display[1].display_n_18\,
      S(0) => \generador_display[1].display_n_19\
    );
\current_count0__20_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_count0__20_carry__0_n_0\,
      CO(3 downto 2) => \NLW_current_count0__20_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \current_count0__20_carry__1_n_2\,
      CO(0) => \current_count0__20_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_current_count0__20_carry__1_O_UNCONNECTED\(3),
      O(2 downto 0) => p_0_in(10 downto 8),
      S(3) => '0',
      S(2 downto 0) => C(10 downto 8)
    );
current_count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => current_count0_carry_n_0,
      CO(2) => current_count0_carry_n_1,
      CO(1) => current_count0_carry_n_2,
      CO(0) => current_count0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 2) => current_count2(6 downto 5),
      DI(1) => \generador_display[2].display_n_0\,
      DI(0) => \generador_display[2].display_n_1\,
      O(3 downto 1) => C(6 downto 4),
      O(0) => NLW_current_count0_carry_O_UNCONNECTED(0),
      S(3) => current_count0_carry_i_1_n_0,
      S(2) => current_count0_carry_i_2_n_0,
      S(1) => \generador_display[2].display_n_19\,
      S(0) => \generador_display[2].display_n_20\
    );
\current_count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => current_count0_carry_n_0,
      CO(3) => \NLW_current_count0_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \current_count0_carry__0_n_1\,
      CO(1) => \current_count0_carry__0_n_2\,
      CO(0) => \current_count0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => current_count2(9 downto 7),
      O(3 downto 0) => C(10 downto 7),
      S(3) => \current_count0_carry__0_i_1_n_0\,
      S(2) => \generador_display[2].display_n_21\,
      S(1) => last_display_n_14,
      S(0) => \current_count0_carry__0_i_4_n_0\
    );
\current_count0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => current_count1(10),
      I1 => current_count2(10),
      O => \current_count0_carry__0_i_1_n_0\
    );
\current_count0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => current_count2(7),
      I1 => current_count1(7),
      O => \current_count0_carry__0_i_4_n_0\
    );
current_count0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => current_count2(6),
      I1 => current_count1(6),
      O => current_count0_carry_i_1_n_0
    );
current_count0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => current_count2(5),
      I1 => current_count1(5),
      O => current_count0_carry_i_2_n_0
    );
\current_count1__0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \current_count1__0_carry_n_0\,
      CO(2) => \NLW_current_count1__0_carry_CO_UNCONNECTED\(2),
      CO(1) => \current_count1__0_carry_n_2\,
      CO(0) => \current_count1__0_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => last_display_n_10,
      DI(0) => '0',
      O(3) => \NLW_current_count1__0_carry_O_UNCONNECTED\(3),
      O(2 downto 0) => current_count1(7 downto 5),
      S(3) => '1',
      S(2) => last_display_n_11,
      S(1) => last_display_n_12,
      S(0) => last_display_n_13
    );
\current_count2__0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \current_count2__0_carry_n_0\,
      CO(2) => \current_count2__0_carry_n_1\,
      CO(1) => \current_count2__0_carry_n_2\,
      CO(0) => \current_count2__0_carry_n_3\,
      CYINIT => '0',
      DI(3) => \generador_display[2].display_n_1\,
      DI(2) => \generador_display[2].display_n_2\,
      DI(1) => \generador_display[2].display_n_14\,
      DI(0) => '0',
      O(3 downto 0) => current_count2(8 downto 5),
      S(3) => \generador_display[2].display_n_15\,
      S(2) => \generador_display[2].display_n_16\,
      S(1) => \generador_display[2].display_n_17\,
      S(0) => \generador_display[2].display_n_18\
    );
\current_count2__0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_count2__0_carry_n_0\,
      CO(3 downto 1) => \NLW_current_count2__0_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \current_count2__0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \generador_display[2].display_n_0\,
      O(3 downto 2) => \NLW_current_count2__0_carry__0_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => current_count2(10 downto 9),
      S(3 downto 2) => B"00",
      S(1) => \generador_display[2].display_n_12\,
      S(0) => \generador_display[2].display_n_13\
    );
display_mux: entity work.mux_displays
     port map (
      CLK => clk,
      D(2) => display_mux_n_4,
      D(1) => display_mux_n_5,
      D(0) => display_mux_n_6,
      E(0) => E(0),
      Q(1 downto 0) => s_counter(1 downto 0),
      reset_IBUF => reset_IBUF,
      \s_anodes_reg[3]_0\(3 downto 0) => \s_anodes_reg[3]\(3 downto 0),
      \s_counter_reg[0]_0\ => display_mux_n_3,
      \segments_out_reg[2]_0\ => \generador_display[2].display_n_5\,
      \segments_out_reg[2]_1\ => last_display_n_9,
      \segments_out_reg[4]_0\ => \generador_display[2].display_n_6\,
      \segments_out_reg[4]_1\ => last_display_n_8,
      \segments_out_reg[6]_0\(6 downto 0) => Q(6 downto 0),
      \segments_out_reg[6]_1\(4) => last_display_n_4,
      \segments_out_reg[6]_1\(3) => \generador_display[2].display_n_7\,
      \segments_out_reg[6]_1\(2) => last_display_n_5,
      \segments_out_reg[6]_1\(1) => last_display_n_6,
      \segments_out_reg[6]_1\(0) => \generador_display[1].display_n_5\
    );
divisor_15_1: entity work.clk_divisor
     port map (
      CLK => clk,
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      reset_IBUF => reset_IBUF
    );
\generador_decider[0].updown_decider\: entity work.count_decider
     port map (
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      last_enable => \^last_enable\,
      last_enable_reg_0 => \generador_decider[0].updown_decider_n_1\,
      reset_IBUF => reset_IBUF,
      s_count_enable => s_count_enable
    );
\generador_decider[1].updown_decider\: entity work.count_decider_0
     port map (
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      last_enable => last_enable_0,
      reset_IBUF => reset_IBUF,
      s_overflow_0 => s_overflow_0
    );
\generador_decider[2].updown_decider\: entity work.count_decider_1
     port map (
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      last_enable => last_enable_1,
      reset_IBUF => reset_IBUF,
      s_overflow_1 => s_overflow_1
    );
\generador_decider[3].updown_decider\: entity work.count_decider_2
     port map (
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      last_enable => last_enable_1,
      last_enable_reg_0 => \generador_decider[3].updown_decider_n_0\,
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]\ => \generador_display[2].display_n_4\,
      \s_count_reg[0]_0\ => \generador_display[1].display_n_10\,
      \s_count_reg[0]_1\ => \generador_display[0].display_n_14\,
      s_overflow_2 => s_overflow_2
    );
\generador_display[0].display\: entity work.counter_7seg
     port map (
      D(0) => display_mux_n_6,
      E(0) => \generador_display[0].display_n_13\,
      Q(2) => \generador_display[0].display_n_10\,
      Q(1) => \generador_display[0].display_n_11\,
      Q(0) => \generador_display[0].display_n_12\,
      S(2) => \count_out_OBUF[4]_inst_i_2_n_0\,
      S(1) => \count_out_OBUF[4]_inst_i_3_n_0\,
      S(0) => \count_out_OBUF[4]_inst_i_4_n_0\,
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      count_out_OBUF(9 downto 1) => count_out_OBUF(10 downto 2),
      count_out_OBUF(0) => count_out_OBUF(0),
      last_enable => \^last_enable\,
      last_enable_0 => last_enable_0,
      last_enable_reg => \generador_display[0].display_n_14\,
      p_0_in(6 downto 0) => p_0_in(10 downto 4),
      reset_IBUF => reset_IBUF,
      s_count_enable => s_count_enable,
      \s_count_reg[0]\ => \generador_display[0].display_n_16\,
      \s_count_reg[0]_0\ => \generador_display[0].display_n_19\,
      \s_count_reg[0]_1\ => \generador_decider[0].updown_decider_n_1\,
      \s_count_reg[1]\ => \generador_display[0].display_n_21\,
      \s_count_reg[2]\ => \generador_display[0].display_n_20\,
      \s_count_reg[2]_0\ => \s_count_reg[2]\,
      \s_count_reg[3]\ => \generador_display[0].display_n_17\,
      \s_count_reg[3]_0\ => \generador_display[0].display_n_18\,
      \s_count_reg[3]_1\(0) => \s_count_reg[3]\(0),
      \s_counter_reg[0]\ => \generador_display[0].display_n_15\,
      s_overflow_0 => s_overflow_0,
      s_up_down => s_up_down,
      \segments_out_reg[1]\(1 downto 0) => s_counter(1 downto 0),
      \segments_out_reg[3]\ => \generador_display[2].display_n_9\,
      \segments_out_reg[6]\ => \generador_display[2].display_n_8\
    );
\generador_display[1].display\: entity work.counter_7seg_3
     port map (
      D(0) => display_mux_n_5,
      DI(0) => current_count3(3),
      E(0) => \generador_display[1].display_n_0\,
      Q(1) => \generador_display[1].display_n_3\,
      Q(0) => \generador_display[1].display_n_4\,
      S(1) => \generador_display[1].display_n_13\,
      S(0) => \generador_display[1].display_n_14\,
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      \current_count0__20_carry\ => last_display_n_3,
      \current_count0__20_carry_0\(1) => \generador_display[2].display_n_1\,
      \current_count0__20_carry_0\(0) => \generador_display[2].display_n_2\,
      \current_count0__20_carry__0\(3 downto 0) => C(7 downto 4),
      last_enable => last_enable_1,
      last_enable_reg => \generador_display[1].display_n_1\,
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]\(0) => \generador_display[0].display_n_13\,
      \s_count_reg[2]\ => \generador_display[1].display_n_8\,
      \s_count_reg[3]\(0) => \generador_display[1].display_n_5\,
      \s_count_reg[3]_0\ => \generador_display[1].display_n_7\,
      \s_count_reg[3]_1\ => \generador_display[1].display_n_11\,
      \s_count_reg[3]_2\(3) => \generador_display[1].display_n_16\,
      \s_count_reg[3]_2\(2) => \generador_display[1].display_n_17\,
      \s_count_reg[3]_2\(1) => \generador_display[1].display_n_18\,
      \s_count_reg[3]_2\(0) => \generador_display[1].display_n_19\,
      \s_count_reg[3]_3\ => \generador_display[1].display_n_20\,
      \s_count_reg[3]_4\ => \generador_display[0].display_n_14\,
      \s_counter_reg[1]\ => \generador_display[1].display_n_6\,
      \s_counter_reg[1]_0\ => \generador_display[1].display_n_9\,
      \s_counter_reg[1]_1\ => \generador_display[1].display_n_12\,
      s_overflow_1 => s_overflow_1,
      s_up_down => s_up_down,
      \segments_out_reg[0]\ => last_display_n_1,
      \segments_out_reg[1]\ => \generador_display[2].display_n_11\,
      \segments_out_reg[3]\(1 downto 0) => s_counter(1 downto 0),
      up_down_reg => \generador_display[1].display_n_10\
    );
\generador_display[2].display\: entity work.counter_7seg_4
     port map (
      CO(0) => \current_count1__0_carry_n_0\,
      D(0) => display_mux_n_4,
      DI(0) => \generador_display[2].display_n_14\,
      E(0) => \generador_display[1].display_n_0\,
      O(0) => current_count2(9),
      Q(2) => \generador_display[2].display_n_0\,
      Q(1) => \generador_display[2].display_n_1\,
      Q(0) => \generador_display[2].display_n_2\,
      S(1) => \generador_display[2].display_n_12\,
      S(0) => \generador_display[2].display_n_13\,
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      current_count0_carry => last_display_n_3,
      current_count0_carry_0 => last_display_n_2,
      last_enable_reg => \generador_display[1].display_n_1\,
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]\ => \generador_display[1].display_n_20\,
      \s_count_reg[1]\ => \generador_display[2].display_n_10\,
      \s_count_reg[1]_0\ => \generador_display[2].display_n_11\,
      \s_count_reg[1]_1\(3) => \generador_display[2].display_n_15\,
      \s_count_reg[1]_1\(2) => \generador_display[2].display_n_16\,
      \s_count_reg[1]_1\(1) => \generador_display[2].display_n_17\,
      \s_count_reg[1]_1\(0) => \generador_display[2].display_n_18\,
      \s_count_reg[2]\ => \generador_display[2].display_n_4\,
      \s_count_reg[2]_0\(1) => \generador_display[2].display_n_19\,
      \s_count_reg[2]_0\(0) => \generador_display[2].display_n_20\,
      \s_count_reg[2]_1\(0) => \generador_display[2].display_n_21\,
      \s_count_reg[3]\ => \generador_display[2].display_n_5\,
      \s_count_reg[3]_0\ => \generador_display[2].display_n_6\,
      \s_count_reg[3]_1\(0) => \generador_display[2].display_n_7\,
      \s_counter_reg[0]\ => \generador_display[2].display_n_8\,
      \s_counter_reg[0]_0\ => \generador_display[2].display_n_9\,
      s_overflow_2 => s_overflow_2,
      s_up_down => s_up_down,
      \segments_out_reg[2]\ => \generador_display[0].display_n_16\,
      \segments_out_reg[4]\(1 downto 0) => s_counter(1 downto 0),
      \segments_out_reg[4]_0\ => \generador_display[0].display_n_19\,
      \segments_out_reg[5]\ => last_display_n_7
    );
last_display: entity work.\counter_7seg__parameterized2\
     port map (
      CO(0) => \current_count1__0_carry_n_0\,
      DI(0) => last_display_n_10,
      O(0) => current_count1(10),
      Q(0) => s_counter(1),
      S(2) => last_display_n_11,
      S(1) => last_display_n_12,
      S(0) => last_display_n_13,
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      \current_count0_carry__0\(0) => current_count2(8),
      reset_IBUF => reset_IBUF,
      \s_count_reg[0]\ => last_display_n_3,
      \s_count_reg[0]_0\(2) => last_display_n_4,
      \s_count_reg[0]_0\(1) => last_display_n_5,
      \s_count_reg[0]_0\(0) => last_display_n_6,
      \s_count_reg[0]_1\ => \generador_decider[3].updown_decider_n_0\,
      \s_count_reg[1]\ => last_display_n_2,
      \s_count_reg[1]_0\ => last_display_n_8,
      \s_count_reg[1]_1\(0) => last_display_n_14,
      \s_count_reg[2]\ => last_display_n_1,
      \s_count_reg[2]_0\ => last_display_n_7,
      \s_count_reg[2]_1\ => last_display_n_9,
      s_up_down => s_up_down,
      \segments_out_reg[0]\ => \generador_display[2].display_n_10\,
      \segments_out_reg[0]_0\ => \generador_display[0].display_n_15\,
      \segments_out_reg[1]\ => display_mux_n_3,
      \segments_out_reg[1]_0\ => \generador_display[0].display_n_20\,
      \segments_out_reg[1]_1\ => \generador_display[1].display_n_8\,
      \segments_out_reg[2]\ => \generador_display[1].display_n_11\,
      \segments_out_reg[3]\ => \generador_display[0].display_n_18\,
      \segments_out_reg[3]_0\ => \generador_display[1].display_n_12\,
      \segments_out_reg[4]\ => \generador_display[1].display_n_7\,
      \segments_out_reg[5]\ => \generador_display[1].display_n_6\,
      \segments_out_reg[5]_0\ => \generador_display[0].display_n_21\,
      \segments_out_reg[6]\ => \generador_display[0].display_n_17\,
      \segments_out_reg[6]_0\ => \generador_display[1].display_n_9\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity motor_encoder_control is
  port (
    digits_7seg : out STD_LOGIC_VECTOR ( 6 downto 0 );
    anodes_7seg : out STD_LOGIC_VECTOR ( 3 downto 0 );
    count_out : out STD_LOGIC_VECTOR ( 10 downto 0 );
    enc_a : in STD_LOGIC;
    enc_b : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk_15k : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of motor_encoder_control : entity is true;
  attribute count_bits : integer;
  attribute count_bits of motor_encoder_control : entity is 11;
  attribute digits : integer;
  attribute digits of motor_encoder_control : entity is 4;
  attribute max_ms_digit : integer;
  attribute max_ms_digit of motor_encoder_control : entity is 1;
end motor_encoder_control;

architecture STRUCTURE of motor_encoder_control is
  signal anodes_7seg_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal clk_15k_IBUF : STD_LOGIC;
  signal clk_15k_IBUF_BUFG : STD_LOGIC;
  signal count_out_OBUF : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal counter_4_disp_n_12 : STD_LOGIC;
  signal decoder_ab_n_2 : STD_LOGIC;
  signal decoder_ab_n_3 : STD_LOGIC;
  signal digits_7seg_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal enc_a_IBUF : STD_LOGIC;
  signal enc_a_IBUF_BUFG : STD_LOGIC;
  signal enc_b_IBUF : STD_LOGIC;
  signal \generador_decider[0].updown_decider/last_enable\ : STD_LOGIC;
  signal reset_IBUF : STD_LOGIC;
  signal s_count_enable : STD_LOGIC;
  signal s_up_down : STD_LOGIC;
begin
\anodes_7seg_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => anodes_7seg_OBUF(0),
      O => anodes_7seg(0)
    );
\anodes_7seg_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => anodes_7seg_OBUF(1),
      O => anodes_7seg(1)
    );
\anodes_7seg_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => anodes_7seg_OBUF(2),
      O => anodes_7seg(2)
    );
\anodes_7seg_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => anodes_7seg_OBUF(3),
      O => anodes_7seg(3)
    );
clk_15k_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_15k_IBUF,
      O => clk_15k_IBUF_BUFG
    );
clk_15k_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk_15k,
      O => clk_15k_IBUF
    );
\count_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(0),
      O => count_out(0)
    );
\count_out_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(10),
      O => count_out(10)
    );
\count_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(1),
      O => count_out(1)
    );
\count_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(2),
      O => count_out(2)
    );
\count_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(3),
      O => count_out(3)
    );
\count_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(4),
      O => count_out(4)
    );
\count_out_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(5),
      O => count_out(5)
    );
\count_out_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(6),
      O => count_out(6)
    );
\count_out_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(7),
      O => count_out(7)
    );
\count_out_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(8),
      O => count_out(8)
    );
\count_out_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_out_OBUF(9),
      O => count_out(9)
    );
counter_4_disp: entity work.full_counter
     port map (
      E(0) => counter_4_disp_n_12,
      Q(6 downto 0) => digits_7seg_OBUF(6 downto 0),
      clk_15k_IBUF_BUFG => clk_15k_IBUF_BUFG,
      count_out_OBUF(10 downto 0) => count_out_OBUF(10 downto 0),
      last_enable => \generador_decider[0].updown_decider/last_enable\,
      reset_IBUF => reset_IBUF,
      \s_anodes_reg[3]\(3 downto 0) => anodes_7seg_OBUF(3 downto 0),
      s_count_enable => s_count_enable,
      \s_count_reg[2]\ => decoder_ab_n_2,
      \s_count_reg[3]\(0) => decoder_ab_n_3,
      s_up_down => s_up_down
    );
decoder_ab: entity work.decoder
     port map (
      AR(0) => reset_IBUF,
      CLK => enc_a_IBUF_BUFG,
      E(0) => counter_4_disp_n_12,
      count_enable_reg_0(0) => decoder_ab_n_3,
      enc_b_IBUF => enc_b_IBUF,
      last_enable => \generador_decider[0].updown_decider/last_enable\,
      s_count_enable => s_count_enable,
      s_up_down => s_up_down,
      up_down_reg_0 => decoder_ab_n_2
    );
\digits_7seg_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(0),
      O => digits_7seg(0)
    );
\digits_7seg_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(1),
      O => digits_7seg(1)
    );
\digits_7seg_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(2),
      O => digits_7seg(2)
    );
\digits_7seg_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(3),
      O => digits_7seg(3)
    );
\digits_7seg_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(4),
      O => digits_7seg(4)
    );
\digits_7seg_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(5),
      O => digits_7seg(5)
    );
\digits_7seg_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digits_7seg_OBUF(6),
      O => digits_7seg(6)
    );
enc_a_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => enc_a_IBUF,
      O => enc_a_IBUF_BUFG
    );
enc_a_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => enc_a,
      O => enc_a_IBUF
    );
enc_b_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => enc_b,
      O => enc_b_IBUF
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
end STRUCTURE;
