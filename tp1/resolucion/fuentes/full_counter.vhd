--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity full_counter is
  generic(
    max_ms_digit : integer := 9;
    count_bits : integer := 11;
    digits : integer := 2); 
  port(
    digits_7seg : out std_logic_vector(6 downto 0);
    anodes_7seg : out std_logic_vector(digits-1 downto 0);
    current_count : out std_logic_vector(count_bits-1 downto 0);
    count_enable : in std_logic;
    up_down : in std_logic;
    reset : in std_logic;
    clk_15k : in std_logic);
end full_counter;

architecture logica of full_counter is
  
  component clk_divisor 
    generic(
      div : integer);
    port(
      reset : in std_logic;
      input_clk : in std_logic;
      output_clk : out std_logic);
  end component;
  
  component bcd2bin
    generic(
      binary_bits : integer;
      digits : integer); 
    port(
      decimal_digits : in std_logic_vector(4*digits-1 downto 0);
      binary : out std_logic_vector(binary_bits-1 downto 0));
  end component;

  
  component counter_7seg
    generic(
      max : integer := 9);
    port(
      clk : in std_logic;
      reset : in std_logic;
      overflow : out std_logic;        
      count : out std_logic_vector(3 downto 0);        
      increment : in std_logic;
      decrement : in std_logic;
      segments : out std_logic_vector(6 downto 0));
  end component;
  
  component count_decider
    port(
      up_down : in std_logic;
      count_enable : in std_logic;
      increment : out std_logic;
      decrement : out std_logic;
      reset : in std_logic;    
      clk : in std_logic);
  end component;

  component mux_displays
    generic(
      digits : integer := digits);
    port (
      data_in : in std_logic_vector((digits*7)-1 downto 0);
      segments_out : out std_logic_vector(6 downto 0);
      anodes_out : out std_logic_vector(digits-1 downto 0);
      reset : in std_logic;
      clk : in std_logic);
  end component;
  
  signal s_clk_1k : std_logic;
  signal s_enable_count : std_logic_vector(digits-1 downto 0);
  signal s_increment : std_logic_vector(digits-1 downto 0);
  signal s_decrement : std_logic_vector(digits-1 downto 0);
  signal s_overflow : std_logic_vector(digits-2 downto 0);  
  signal s_7seg : std_logic_vector((7*digits)-1 downto 0);
  signal s_segments_count : std_logic_vector(4*digits-1 downto 0);
  --signal s_current_count : std_logic_vector(count_bits-1 downto 0);

begin
            
  divisor_15_1 : clk_divisor
    generic map(
      div => 15)
    port map(
      reset => reset,
      input_clk => clk_15k,
      output_clk => s_clk_1k);      
  
  display_mux : mux_displays
    port map(
      data_in => s_7seg,
      segments_out => digits_7seg,
      anodes_out => anodes_7seg,
      reset => reset,
      clk => s_clk_1k);
    
  s_enable_count(0) <= count_enable;

  generador_display : for i in 0 to digits-2
  generate
    display : counter_7seg
      port map(
        increment => s_increment(i),
        decrement => s_decrement(i),
        overflow => s_overflow(i),
        reset => reset,      
        count => s_segments_count(4*(i+1)-1 downto i*4),
        segments => s_7seg(7*(i+1)-1 downto 7*i),    
        clk => clk_15k);
    
    s_enable_count(i+1) <= s_overflow(i);
  end generate;

  generador_decider : for i in 0 to digits-1
  generate
    updown_decider : count_decider
      port map(
        up_down => up_down,
        count_enable => s_enable_count(i),
        increment => s_increment(i),
        decrement => s_decrement(i),
        reset => reset,
        clk => clk_15k);
  end generate;
    
  last_display : counter_7seg
    generic map(
      max => max_ms_digit)
    port map(
      increment => s_increment(digits-1),
      decrement => s_decrement(digits-1),
      overflow => open,
      count => s_segments_count((4*digits)-1 downto 4*(digits-1)),
      reset => reset,      
      segments => s_7seg((7*digits)-1 downto 7*(digits-1)),    
      clk => clk_15k);
  
  bcd_decoder : bcd2bin
    generic map(
      binary_bits => count_bits,
      digits => digits)
    port map (
      decimal_digits => s_segments_count,
      binary => current_count);
    
   -- proc_counter_sum : process (s_segments_count)
   --   variable running_sum : unsigned(count_bits-1 downto 0);
   --   variable one : unsigned(count_bits-1 downto 0);
   -- begin
   --   running_sum := (others => '0');
   --   one := (others => '0');
   --   one(0) := '1';
   --   loop_counter_sum : for i in 0 to digits-1 loop
   --     running_sum := running_sum + resize(10**i * one *
   --                    unsigned(s_segments_count(4*(i+1)-1 downto 4*i)), count_bits);
   --   end loop;
   --   s_current_count <= std_logic_vector(running_sum(count_bits-1 downto 0));
   -- end process;     

   -- current_count <= s_current_count;
    
end logica;