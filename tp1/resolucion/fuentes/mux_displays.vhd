--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº10: Cronómetro Up/Down de 0-59
-- Implementacion: top_cronometro.vhd, clk_divisor.vhd,
--                 bcd7seg.vhd, decimal_counter.vhd
-- Simulacion: tb_top_cronometro.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

entity mux_displays is generic(
  digits : integer := 2);
  port(
    data_in : in std_logic_vector((digits*7)-1 downto 0);
    segments_out : out std_logic_vector(6 downto 0);
    anodes_out : out std_logic_vector(digits-1 downto 0);
    reset : in std_logic;
    clk : in std_logic);
end mux_displays;

architecture logica of mux_displays is

  constant bits_digits : integer := integer(log2(real(digits)));
  signal s_counter : std_logic_vector(bits_digits-1 downto 0);  
  signal s_anodes : std_logic_vector(digits-1 downto 0);  

begin

  proc_counter : process(clk, reset) begin
    if(reset = '1') then
        s_counter <= std_logic_vector(to_unsigned(0, bits_digits));
        s_anodes <= std_logic_vector(to_unsigned(0, digits));
    elsif(rising_edge(clk)) then
      s_counter <= s_counter + 1;
      s_anodes <= (others => '0');
      s_anodes(to_integer(unsigned(s_counter))) <= '1';
      segments_out <= data_in(to_integer(unsigned(s_counter))*7+6 downto to_integer(unsigned(s_counter))*7);
    end if;
  end process;

  anodes_out <= s_anodes;
  
end logica;
