--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity bcd2bin is
  generic(
    binary_bits : integer := 4;
    digits : integer := 1); 
  port(
    decimal_digits : in std_logic_vector(4*digits-1 downto 0);
    binary : out std_logic_vector(binary_bits-1 downto 0));
end bcd2bin;

architecture logica of bcd2bin is

  signal s_binary : std_logic_vector(binary_bits-1 downto 0);

begin
            
    proc_decode_sum : process (decimal_digits)
      variable running_sum : unsigned(binary_bits-1 downto 0);
      variable one : unsigned(binary_bits-1 downto 0);
    begin
      running_sum := (others => '0');
      one := (others => '0');
      one(0) := '1';
      loop_counter_sum : for i in 0 to digits-1 loop
        running_sum := running_sum + resize(10**i * one *
                       unsigned(decimal_digits(4*(i+1)-1 downto 4*i)), binary_bits);
      end loop;
      s_binary <= std_logic_vector(running_sum(binary_bits-1 downto 0));
    end process;     

    binary <= s_binary;
    
end logica;
