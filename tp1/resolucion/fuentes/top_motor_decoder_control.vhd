--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity motor_encoder_control is generic(
  digits : integer := 4;
  count_bits : integer := 11; -- 2048 values for current example
  max_count : integer := 2000;
  max_ms_digit : integer := 1);
  port(
    digits_7seg : out std_logic_vector(6 downto 0);
    anodes_7seg : out std_logic_vector(digits-1 downto 0);
    count_out : out std_logic_vector(count_bits-1 downto 0);
    desired_position : in std_logic_vector(count_bits-1 downto 0);
    enc_a, enc_b : in std_logic;
    pwm_out : out std_logic;
    reset : in std_logic;
    clk_15k : in std_logic);
end motor_encoder_control;

architecture logica of motor_encoder_control is

  component full_counter generic(
    digits : integer;
    count_bits : integer;
    max_ms_digit : integer);
    port(
    digits_7seg : out std_logic_vector(6 downto 0);
    anodes_7seg : out std_logic_vector(digits-1 downto 0);
    current_count : out std_logic_vector(count_bits-1 downto 0);
    count_enable : in std_logic;
    up_down : in std_logic;
    reset : in std_logic;
    clk_15k : in std_logic);
  end component;
  
  component decoder generic(
    steps : integer);
   port(
    enc_a, enc_b : in std_logic;
    count_enable : out std_logic;
    reset : in std_logic;
    up_down : out std_logic);
  end component;

  component pwm
    port(
      duty : in std_logic_vector(6 downto 0);
      pwm_out : out std_logic;
      reset : in std_logic;
      clk : in std_logic);
  end component;        

  component pwm_control
    generic(
      max_count : integer;
      count_bits : integer);
    port(
      current_pos : in std_logic_vector(count_bits-1 downto 0);
      desired_pos : in std_logic_vector(count_bits-1 downto 0);
      duty : out std_logic_vector(6 downto 0));
  end component;
  
  signal s_up_down : std_logic;
  signal s_count_enable : std_logic;
  signal s_current_count : std_logic_vector (count_bits-1 downto 0);
  signal s_pwm_duty : std_logic_vector(6 downto 0);

begin

  pwm_controller : pwm_control
    generic map(
      max_count => max_count,
      count_bits => count_bits)
    port map(
      current_pos => s_current_count,
      desired_pos => desired_position,
      duty => s_pwm_duty);
  
  pwm_driver : pwm
    port map(
      duty => s_pwm_duty,
      pwm_out => pwm_out,
      reset => reset,
      clk => clk_15k);

  counter_4_disp : full_counter generic map(
    digits => digits,
    count_bits => count_bits,
    max_ms_digit => max_ms_digit
    ) port map(
    digits_7seg => digits_7seg,
    anodes_7seg => anodes_7seg,
    count_enable => s_count_enable,
    current_count => s_current_count,
    up_down => s_up_down,
    reset => reset,
    clk_15k => clk_15k);

  count_out <= s_current_count;
    
  decoder_ab : decoder generic map(
    steps => 3)
  port map(
    enc_a => enc_a,
    enc_b => enc_b,
    reset => reset,
    count_enable => s_count_enable,
    up_down => s_up_down);
    
end logica;
