--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº10: Cronómetro Up/Down de 0-59
-- Implementacion: top_cronometro.vhd, clk_divisor.vhd,
--                 bcd7seg.vhd, decimal_counter.vhd
-- Simulacion: tb_top_cronometro.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity count_decider is port(
    up_down : in std_logic;
    count_enable : in std_logic;
    increment : out std_logic;
    decrement : out std_logic;
    reset : in std_logic;    
    clk : in std_logic);
end count_decider;

architecture logica of count_decider is

    signal last_enable : std_logic;
      
begin

    flank_detect : process(clk, reset)
  begin
    if( reset = '1' ) then
      last_enable <= '0';
    elsif(rising_edge(clk)) then
      last_enable <= count_enable;
    end if;
  end process;

  decision : process (last_enable, count_enable, up_down)
  begin
    if(last_enable = '0' and count_enable = '1') then
      increment <= up_down;
      decrement <= not(up_down);
    else
      increment <= '0';
      decrement <= '0';            
    end if;
  end process;

end logica;
