--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
-- use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity pwm_control is
  generic(
    max_count : integer := 2000;
    count_bits : integer := 11);
  port(
    current_pos : in std_logic_vector(count_bits-1 downto 0);
    desired_pos : in std_logic_vector(count_bits-1 downto 0);
    duty : out std_logic_vector(6 downto 0));
end pwm_control;

architecture logic of pwm_control is

  signal s_duty : std_logic_vector(6 downto 0);
  signal s_clockwise : std_logic;

begin

  pos_substract : process (current_pos, desired_pos)
    variable s_sub : unsigned(count_bits-1 downto 0);
    variable s_sub2 : unsigned(count_bits-1 downto 0);
    variable s_inv : std_logic;
    variable s_inv2 : std_logic;
  begin

    if(current_pos > desired_pos) then
      s_sub := unsigned(current_pos) - unsigned(desired_pos);
      s_inv := '1';
    else
      s_sub := unsigned(desired_pos) - unsigned(current_pos);
      s_inv := '0';
    end if;

    --if(s_sub > "1111101000") then
      --s_sub2 := "11111010000" - s_sub;
    if(s_sub > to_unsigned(max_count/2, count_bits)) then
      s_sub2 := to_unsigned(max_count, count_bits) - s_sub;
      s_inv2 := '1';
    else
      s_sub2 := s_sub;
      s_inv2 := '0';
    end if;

    if(s_sub2 > to_unsigned(95,count_bits)) then
      s_duty <= "1011111";
    elsif (s_sub2 < to_unsigned(55,count_bits)) then
      s_duty <= "0110111";
    else
      s_duty <= std_logic_vector(s_sub2(6 downto 0));
    end if;
    s_clockwise <= s_inv xor s_inv2;
  end process;


  assign_duty : process(s_duty, s_clockwise)
  begin
    if(s_clockwise = '1') then
      duty <= s_duty;
    else
      duty <= std_logic_vector("1100100" - unsigned(s_duty));
    end if;
  end process;

end logic;
