--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
-- ACLARACION: La frecuencia del pwm será 100 veces menos que lo
-- que se le provea por el clk
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity pwm is
  port(
    duty : in std_logic_vector(6 downto 0);
    pwm_out : out std_logic;
    reset : in std_logic;
    clk : in std_logic);
end pwm;

architecture logic of pwm is

  signal s_count : std_logic_vector(6 downto 0);
  signal s_pwm : std_logic;

begin

  proc_pwm_count : process(clk, reset)
  begin
    if(reset = '1') then
      s_count <= std_logic_vector(to_unsigned(0, 7));
    elsif(rising_edge(clk)) then
      if(s_count = std_logic_vector(to_unsigned(99, 7))) then
        s_count <= std_logic_vector(to_unsigned(0, 7));
      else
        s_count <= s_count + '1';
      end if;
    else
      s_count <= s_count;
    end if;
  end process;

  proc_pwm_decide : process(s_count, duty)
  begin
    if((s_count + '1') > duty) then
      s_pwm <= '0';
    else
      s_pwm <= '1';
    end if;
  end process;
  
  pwm_out <= s_pwm;
            
end logic;
