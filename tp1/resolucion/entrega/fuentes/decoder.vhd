--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

entity decoder is generic(
        steps : integer := 100);
    port(
        enc_a, enc_b : in std_logic;
        count_enable : out std_logic;
        up_down : out std_logic;
        reset : in std_logic);
end decoder;

architecture logica of decoder is

    constant steps_bits : integer := integer(log2(real(steps)));
    signal s_step_counter : std_logic_vector(steps_bits-1 downto 0);
    
begin

    proc_decode : process(enc_a, reset) begin
        if (reset = '1') then
            s_step_counter <= std_logic_vector(to_unsigned(0, steps_bits));
        elsif (rising_edge(enc_a)) then
            up_down <= enc_b;
            if(s_step_counter = steps) then
                s_step_counter <= s_step_counter + 1;
                count_enable <= '1';            
            else
                s_step_counter <= s_step_counter + 1;
                count_enable <= '0';                            
            end if;
        end if;
    end process;
    
end logica;
