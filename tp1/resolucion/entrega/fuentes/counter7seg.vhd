--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity counter_7seg is generic(
        max : integer := 9); 
   port(
        clk : in std_logic;
        reset : in std_logic;
        overflow : out std_logic;
        increment : in std_logic;
        decrement : in std_logic;
        segments : out std_logic_vector(6 downto 0));
end counter_7seg;

architecture logica of counter_7seg is
  
  component bcd7seg port(
        bcd : in std_logic_vector(3 downto 0);
        segments : out std_logic_vector(6 downto 0));
  end component;
  
  component decimal_counter generic(
    max : integer);
  port(
        clk : in std_logic;
        reset : in std_logic;
        overflow : out std_logic;        
        increment : in std_logic;
        decrement : in std_logic;
        count : out std_logic_vector(3 downto 0));
  end component;  

  signal s_count : std_logic_vector(3 downto 0);
  
begin

    counter : decimal_counter generic map(
        max => max)
    port map(
        clk => clk,
        reset => reset,
        overflow => overflow,
        increment => increment,
        decrement => decrement,
        count => s_count);
        
    display : bcd7seg port map(
        bcd => s_count,
        segments => segments);
            
end logica;
