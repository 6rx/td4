--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity encoder_decode is generic(
  digits : integer := 4;
  max_ms_digit : integer := 1);
  port(
    digits_7seg : out std_logic_vector(6 downto 0);
    anodes_7seg : out std_logic_vector(digits-1 downto 0);
    enc_a, enc_b : in std_logic;
    reset : in std_logic;
    clk_15k : in std_logic);
end encoder_decode;

architecture logica of encoder_decode is

  component full_counter generic(
    digits : integer;
    max_ms_digit : integer);
    port(
    digits_7seg : out std_logic_vector(6 downto 0);
    anodes_7seg : out std_logic_vector(digits-1 downto 0);
    count_enable : in std_logic;
    up_down : in std_logic;
    reset : in std_logic;
    clk_15k : in std_logic);
  end component;
  
  component decoder generic(
    steps : integer);
   port(
    enc_a, enc_b : in std_logic;
    count_enable : out std_logic;
    reset : in std_logic;
    up_down : out std_logic);
  end component;
  
  signal s_up_down : std_logic;
  signal s_count_enable : std_logic;
  
begin
       
  counter_4_disp : full_counter generic map(
    digits => digits,
    max_ms_digit => max_ms_digit
    ) port map(
    digits_7seg => digits_7seg,
    anodes_7seg => anodes_7seg,
    count_enable => s_count_enable,
    up_down => s_up_down,
    reset => reset,
    clk_15k => clk_15k);
    
  decoder_ab : decoder generic map(
    steps => 3)
  port map(
    enc_a => enc_a,
    enc_b => enc_b,
    reset => reset,
    count_enable => s_count_enable,
    up_down => s_up_down);
    
end logica;
