--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity enc_dec_tb is generic(
  digits : integer := 4;
  max_ms_digit : integer := 1);
end enc_dec_tb;

architecture testbench of enc_dec_tb is

  component encoder_decode 
    port(
      digits_7seg : out std_logic_vector(6 downto 0);
      anodes_7seg : out std_logic_vector(digits-1 downto 0);
      enc_b : in std_logic;    
      enc_a : in std_logic;    
      reset : in std_logic;
      clk_15k : in std_logic);
  end component;

  signal seg_tb : std_logic_vector(6 downto 0);
  signal an7_tb : std_logic_vector(digits-1 downto 0);
  signal enc_a : std_logic;
  signal enc_b : std_logic;
  signal reset_tb : std_logic;
  signal clk_15k_tb : std_logic;

begin

  uut_encoder_decode: encoder_decode
     port map(
      digits_7seg => seg_tb,
      anodes_7seg => an7_tb,
      enc_a => enc_a,
      enc_b => enc_b,
      reset => reset_tb,
      clk_15k => clk_15k_tb);

  reset_tb_proc : process
    constant periodo : time := 33.333ns;
  begin
    reset_tb <= '0'; wait for periodo;
    reset_tb <= '1'; wait for 10*periodo;
    reset_tb <= '0'; wait;
  end process;

  enc_proc : process
    constant periodo : time := 33.333ns;
  begin
    wait for periodo/3;  
    for t in 1 to 20 loop
        enc_b <= '1'; wait for periodo*4;
        enc_a <= '1'; wait for periodo*4;
        enc_b <= '0'; wait for periodo*4;
        enc_a <= '0'; wait for periodo*4;
    end loop;   
    wait for periodo/5;
    for t in 1 to 20 loop
        enc_a <= '1'; wait for periodo*4;
        enc_b <= '1'; wait for periodo*4;
        enc_a <= '0'; wait for periodo*4;
        enc_b <= '0'; wait for periodo*4;
    end loop;
    wait for periodo/3;  
    for t in 1 to 10000 loop
        enc_a <= '1'; wait for periodo*4;
        enc_b <= '1'; wait for periodo*4;
        enc_a <= '0'; wait for periodo*4;
        enc_b <= '0'; wait for periodo*4;
    end loop; 
  end process;
  
  clock_tb_proc : process
    constant periodo : time := 33.333ns;
  begin
    clk_15k_tb <= '0'; wait for periodo/2;
    clk_15k_tb <= '1'; wait for periodo/2;
  end process;

end testbench;
