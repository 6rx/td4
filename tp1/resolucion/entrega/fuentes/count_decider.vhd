--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Gonzalez, Gabriel Fermin. Leg. nº:70123
-- Gratton, Antonino. Leg. nº: 70885
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Trabajo práctico numero 1: Decodificador de encoder
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity count_decider is port(
    up_down : in std_logic;
    count_enable : in std_logic;
    increment : out std_logic;
    decrement : out std_logic;
    reset : in std_logic;    
    clk : in std_logic);
end count_decider;

architecture logica of count_decider is

    signal last_enable : std_logic;
      
begin

    decision : process(clk, reset) begin
        if( reset = '1' ) then
            last_enable <= '0';
        elsif (rising_edge(clk) )then
            if(last_enable = '0' and count_enable = '1') then
                increment <= up_down;
                decrement <= not(up_down);
            else
                increment <= '0';
                decrement <= '0';            
            end if;
            last_enable <= count_enable;
        end if;
    end process;

end logica;
