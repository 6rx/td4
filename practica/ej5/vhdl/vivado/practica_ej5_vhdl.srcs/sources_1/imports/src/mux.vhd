library ieee;
use ieee.std_logic_1164.all;


entity mux is port(
  enable : in std_logic;
  inputs : in std_logic_vector(7 downto 0);
  channel : in std_logic_vector(2 downto 0);
  salida : out std_logic);
end mux;

architecture logica of mux is

  signal seleccionado : std_logic;

begin

  case channel is
    when "000" => seleccionado <= inputs[0];
    when "001" => seleccionado <= inputs[1];
    when "010" => seleccionado <= inputs[2];
    when "011" => seleccionado <= inputs[3];
    when "100" => seleccionado <= inputs[4];
    when "101" => seleccionado <= inputs[5];
    when "110" => seleccionado <= inputs[6];
    when "111" => seleccionado <= inputs[7];
  end case;

  if(enable) then
    salida <= seleccionado;
  else
    salida <= 'Z';
  end if;

end logica;