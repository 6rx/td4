// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 07:44:17 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej5/vhdl/vivado/practica_ej5_vhdl.sim/sim_1/impl/timing/xsim/mux_tb_time_impl.v
// Design      : mux
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "da6e2ff3" *) 
(* NotValidForBitStream *)
module mux
   (enable,
    input7,
    input6,
    input5,
    input4,
    input3,
    input2,
    input1,
    input0,
    channel2,
    channel1,
    channel0,
    salida);
  input enable;
  input input7;
  input input6;
  input input5;
  input input4;
  input input3;
  input input2;
  input input1;
  input input0;
  input channel2;
  input channel1;
  input channel0;
  output salida;

  wire channel0;
  wire channel0_IBUF;
  wire channel1;
  wire channel1_IBUF;
  wire channel2;
  wire channel2_IBUF;
  wire enable;
  wire enable_IBUF;
  wire input0;
  wire input0_IBUF;
  wire input1;
  wire input1_IBUF;
  wire input2;
  wire input2_IBUF;
  wire input3;
  wire input3_IBUF;
  wire input4;
  wire input4_IBUF;
  wire input5;
  wire input5_IBUF;
  wire input6;
  wire input6_IBUF;
  wire input7;
  wire input7_IBUF;
  wire salida;
  wire salida_OBUF;
  wire salida_OBUFT_inst_i_3_n_0;
  wire salida_OBUFT_inst_i_4_n_0;
  wire salida_TRI;

initial begin
 $sdf_annotate("mux_tb_time_impl.sdf",,,,"tool_control");
end
  IBUF channel0_IBUF_inst
       (.I(channel0),
        .O(channel0_IBUF));
  IBUF channel1_IBUF_inst
       (.I(channel1),
        .O(channel1_IBUF));
  IBUF channel2_IBUF_inst
       (.I(channel2),
        .O(channel2_IBUF));
  IBUF enable_IBUF_inst
       (.I(enable),
        .O(enable_IBUF));
  IBUF input0_IBUF_inst
       (.I(input0),
        .O(input0_IBUF));
  IBUF input1_IBUF_inst
       (.I(input1),
        .O(input1_IBUF));
  IBUF input2_IBUF_inst
       (.I(input2),
        .O(input2_IBUF));
  IBUF input3_IBUF_inst
       (.I(input3),
        .O(input3_IBUF));
  IBUF input4_IBUF_inst
       (.I(input4),
        .O(input4_IBUF));
  IBUF input5_IBUF_inst
       (.I(input5),
        .O(input5_IBUF));
  IBUF input6_IBUF_inst
       (.I(input6),
        .O(input6_IBUF));
  IBUF input7_IBUF_inst
       (.I(input7),
        .O(input7_IBUF));
  OBUFT salida_OBUFT_inst
       (.I(salida_OBUF),
        .O(salida),
        .T(salida_TRI));
  MUXF7 salida_OBUFT_inst_i_1
       (.I0(salida_OBUFT_inst_i_3_n_0),
        .I1(salida_OBUFT_inst_i_4_n_0),
        .O(salida_OBUF),
        .S(channel2_IBUF));
  LUT1 #(
    .INIT(2'h1)) 
    salida_OBUFT_inst_i_2
       (.I0(enable_IBUF),
        .O(salida_TRI));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    salida_OBUFT_inst_i_3
       (.I0(input3_IBUF),
        .I1(input2_IBUF),
        .I2(channel1_IBUF),
        .I3(input1_IBUF),
        .I4(channel0_IBUF),
        .I5(input0_IBUF),
        .O(salida_OBUFT_inst_i_3_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    salida_OBUFT_inst_i_4
       (.I0(input7_IBUF),
        .I1(input6_IBUF),
        .I2(channel1_IBUF),
        .I3(input5_IBUF),
        .I4(channel0_IBUF),
        .I5(input4_IBUF),
        .O(salida_OBUFT_inst_i_4_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
