--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº02: Sumador de un bit com "case/when"
-- Implementacion: top_sumador.vhd
-- Simulacion: tb_top_sumador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity sumador is port(
  a : in std_logic_vector(1 downto 0);
  enable : in std_logic;
  d : out std_logic_vector(1 downto 0)
  );
end sumador;

architecture logica of sumador is
  
  signal suma : std_logic_vector(1 downto 0);

begin

  suma_proc : process(a, suma)
  begin
    case a is
      when "00" => suma <= "00";
      when "01" => suma <= "01";
      when "10" => suma <= "01";
      when "11" => suma <= "10";
      when others => suma <= "11";
    end case;
    
    case enable is
      when '1' => d <= suma;
      when '0' => d <= "11";
      when others => d <= "11";
    end case;
  end process;
end logica;