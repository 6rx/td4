--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº02: Sumador de un bit com "case/when"
-- Implementacion: top_sumador.vhd
-- Simulacion: tb_top_sumador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity sumador_tb is
end sumador_tb;

architecture testbench of sumador_tb is

  component sumador port(
    a : in std_logic_vector(1 downto 0);
    enable : in std_logic;
    d : out std_logic_vector(1 downto 0));
    end component;

  signal a_tb : std_logic_vector(1 downto 0);
  signal enable_tb : std_logic;
  signal d_tb : std_logic_vector(1 downto 0);

begin

  uut_sumador : sumador port map(
    a => a_tb,
    enable => enable_tb,
    d => d_tb);

  proceso_tb : process
    constant periodo: time := 100ns;
    begin 
      a_tb <= "XX";
      enable_tb <= '0';
      wait for periodo;
      assert ( d_tb(1 downto 0) = "11" ) report "Falla en el enable en 0, inputs X,X" severity error;

      enable_tb <= '1'; -- Ya dejo el enable habilitado para los prox test

      a_tb <= "00"; wait for periodo;
      assert ( d_tb(1 downto 0) = "00" ) report "Falla en el enable en 1, inputs 0,0" severity error;
      a_tb <= "01"; wait for periodo;
      assert ( d_tb(1 downto 0) = "01" ) report "Falla en el enable en 1, inputs 0,1" severity error;
      a_tb <= "10"; wait for periodo;
      assert ( d_tb(1 downto 0) = "01" ) report "Falla en el enable en 1, inputs 1,0" severity error;
      a_tb <= "11"; wait for periodo;
      assert ( d_tb(1 downto 0) = "10" ) report "Falla en el enable en 1, inputs 1,1" severity error;
    end process;

end testbench;
