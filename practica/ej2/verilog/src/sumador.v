`timescale 1ns / 1ps

module sumador(
               input [1:0] a,
               input enable,
               output reg [1:0] d
               );

   reg [1:0]                    suma;

   always@(*) begin
      case(a)
        2'b00: suma = 2'b00;
        2'b01: suma = 2'b01;
        2'b10: suma = 2'b01;
        2'b11: suma = 2'b10;
        default: suma = 2'b11;
      endcase
      
      if(enable) d = suma;
      else d = 2'b11;
   end
   
endmodule