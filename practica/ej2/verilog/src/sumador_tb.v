`timescale 1ns/1ps

module sumador_tb();

   reg [1:0] a_tb;
   reg enable_tb;
   wire [1:0] d_tb;

   sumador uut_sumador(
                       .a(a_tb),
                       .enable(enable_tb),
                       .d(d_tb));

   initial begin enable_tb = 1'b1; #10 enable_tb = 1'b0; #100 enable_tb = 1'b1; end
   initial begin a_tb = 2'b00; end
   initial begin forever #100 a_tb = a_tb+1; end
   
endmodule