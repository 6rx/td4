--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº08: Contador asíncrono
-- Implementacion: top_contador.vhd, ffjk.vhd
-- Simulacion: tb_top_contador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity contador_tb is
end contador_tb;

architecture testbench of contador_tb is

  component contador port(
    salida : out std_logic_vector(2 downto 0);
    reset : in std_logic;
    clk : in std_logic);
  end component;

  signal salida_tb : std_logic_vector(2 downto 0);
  signal reset_tb : std_logic;
  signal clk_tb : std_logic;
  
begin

  uut_contador : contador port map(
    salida => salida_tb,
    reset => reset_tb,
    clk => clk_tb);

  clock_tb : process
    constant periodo : time := 25ns;
  begin
    clk_tb <= '0'; wait for periodo;
    clk_tb <= '1'; wait for periodo;
  end process;

  inputs_tb : process
    constant periodo : time := 50ns;
  begin
    reset_tb <= '0';
    wait for periodo;
    reset_tb <= '1'; -- Ya desactivo el reset para los prox tests
    wait for 5*periodo;
    reset_tb <= '0';
    wait;
  end process;
  
end testbench;