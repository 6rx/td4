// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 10:32:38 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej8/vhdl/vivado/practica_ej8_vhdl.sim/sim_1/impl/timing/xsim/contador_tb_time_impl.v
// Design      : contador
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "152fd523" *) 
(* NotValidForBitStream *)
module contador
   (salida,
    reset,
    clk);
  output [2:0]salida;
  input reset;
  input clk;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire ff_jk_1_n_0;
  wire q;
  wire reset;
  wire reset_IBUF;
  wire [2:0]salida;
  wire [2:0]salida_OBUF;

initial begin
 $sdf_annotate("contador_tb_time_impl.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  ff_jk ff_jk_0
       (.clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q(q),
        .reset_IBUF(reset_IBUF),
        .salida_OBUF(salida_OBUF[0]));
  ff_jk_0 ff_jk_1
       (.q(q),
        .q_reg_0(ff_jk_1_n_0),
        .reset_IBUF(reset_IBUF),
        .salida_OBUF(salida_OBUF[1]));
  ff_jk_1 ff_jk_2
       (.q_reg_0(ff_jk_1_n_0),
        .reset_IBUF(reset_IBUF),
        .salida_OBUF(salida_OBUF[2]));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  OBUF \salida_OBUF[0]_inst 
       (.I(salida_OBUF[0]),
        .O(salida[0]));
  OBUF \salida_OBUF[1]_inst 
       (.I(salida_OBUF[1]),
        .O(salida[1]));
  OBUF \salida_OBUF[2]_inst 
       (.I(salida_OBUF[2]),
        .O(salida[2]));
endmodule

module ff_jk
   (q,
    salida_OBUF,
    clk_IBUF_BUFG,
    reset_IBUF);
  output q;
  output [0:0]salida_OBUF;
  input clk_IBUF_BUFG;
  input reset_IBUF;

  wire clk_IBUF_BUFG;
  wire q;
  wire reset_IBUF;
  wire [0:0]salida_OBUF;

  FDCE #(
    .INIT(1'b0)) 
    q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(salida_OBUF),
        .Q(q));
  LUT1 #(
    .INIT(2'h1)) 
    \salida_OBUF[0]_inst_i_1 
       (.I0(q),
        .O(salida_OBUF));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_0
   (q_reg_0,
    salida_OBUF,
    q,
    reset_IBUF);
  output q_reg_0;
  output [0:0]salida_OBUF;
  input q;
  input reset_IBUF;

  wire q;
  wire q_reg_0;
  wire reset_IBUF;
  wire [0:0]salida_OBUF;

  FDCE #(
    .INIT(1'b0)) 
    q_reg
       (.C(q),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(salida_OBUF),
        .Q(q_reg_0));
  LUT1 #(
    .INIT(2'h1)) 
    \salida_OBUF[1]_inst_i_1 
       (.I0(q_reg_0),
        .O(salida_OBUF));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_1
   (salida_OBUF,
    q_reg_0,
    reset_IBUF);
  output [0:0]salida_OBUF;
  input q_reg_0;
  input reset_IBUF;

  wire q_reg_0;
  wire q_reg_n_0;
  wire reset_IBUF;
  wire [0:0]salida_OBUF;

  FDPE #(
    .INIT(1'b1)) 
    q_reg
       (.C(q_reg_0),
        .CE(1'b1),
        .D(salida_OBUF),
        .PRE(reset_IBUF),
        .Q(q_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \salida_OBUF[2]_inst_i_1 
       (.I0(q_reg_n_0),
        .O(salida_OBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
