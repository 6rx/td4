// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Apr  6 17:13:51 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej8/vhdl/vivado/practica_ej8_vhdl.sim/sim_1/synth/timing/xsim/contador_tb_time_synth.v
// Design      : contador
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* NotValidForBitStream *)
module contador
   (salida,
    reset,
    clk);
  output [2:0]salida;
  input reset;
  input clk;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire ff_jk_1_n_2;
  wire ff_jk_2_n_2;
  wire fin_cuenta;
  wire inicio_cuenta;
  wire q;
  wire q_0;
  wire q_1;
  wire reset;
  wire reset_IBUF;
  wire [2:0]salida;
  wire [2:0]salida_OBUF;

initial begin
 $sdf_annotate("contador_tb_time_synth.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  ff_jk ff_jk_0
       (.clk_IBUF_BUFG(clk_IBUF_BUFG),
        .fin_cuenta(fin_cuenta),
        .inicio_cuenta(inicio_cuenta),
        .q(q),
        .reset_IBUF(reset_IBUF),
        .salida_OBUF(salida_OBUF[0]));
  ff_jk_0 ff_jk_1
       (.fin_cuenta(fin_cuenta),
        .inicio_cuenta(inicio_cuenta),
        .q(q_0),
        .q_0(q),
        .q_1(q_1),
        .q_reg_0(ff_jk_1_n_2),
        .reset_IBUF(reset_IBUF),
        .salida_OBUF(salida_OBUF[1]));
  ff_jk_1 ff_jk_2
       (.fin_cuenta(fin_cuenta),
        .inicio_cuenta(inicio_cuenta),
        .q(q_1),
        .q_0(q),
        .q_1(q_0),
        .q_reg_0(ff_jk_2_n_2),
        .reset_IBUF(reset_IBUF),
        .salida_OBUF(salida_OBUF[2]));
  FDRE #(
    .INIT(1'b0)) 
    fin_cuenta_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(ff_jk_2_n_2),
        .Q(fin_cuenta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    inicio_cuenta_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(ff_jk_1_n_2),
        .Q(inicio_cuenta),
        .R(1'b0));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  OBUF \salida_OBUF[0]_inst 
       (.I(salida_OBUF[0]),
        .O(salida[0]));
  OBUF \salida_OBUF[1]_inst 
       (.I(salida_OBUF[1]),
        .O(salida[1]));
  OBUF \salida_OBUF[2]_inst 
       (.I(salida_OBUF[2]),
        .O(salida[2]));
endmodule

module ff_jk
   (q,
    salida_OBUF,
    clk_IBUF_BUFG,
    fin_cuenta,
    inicio_cuenta,
    reset_IBUF);
  output q;
  output [0:0]salida_OBUF;
  input clk_IBUF_BUFG;
  input fin_cuenta;
  input inicio_cuenta;
  input reset_IBUF;

  wire clk_IBUF_BUFG;
  wire fin_cuenta;
  wire inicio_cuenta;
  wire q;
  wire q_i_1_n_0;
  wire reset_IBUF;
  wire [0:0]salida_OBUF;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT1 #(
    .INIT(2'h1)) 
    q_i_1
       (.I0(q),
        .O(q_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_i_1_n_0),
        .Q(q),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h00A2)) 
    \salida_OBUF[0]_inst_i_1 
       (.I0(q),
        .I1(fin_cuenta),
        .I2(inicio_cuenta),
        .I3(reset_IBUF),
        .O(salida_OBUF));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_0
   (q,
    salida_OBUF,
    q_reg_0,
    q_0,
    q_1,
    reset_IBUF,
    inicio_cuenta,
    fin_cuenta);
  output q;
  output [0:0]salida_OBUF;
  output q_reg_0;
  input q_0;
  input q_1;
  input reset_IBUF;
  input inicio_cuenta;
  input fin_cuenta;

  wire fin_cuenta;
  wire inicio_cuenta;
  wire q;
  wire q_0;
  wire q_1;
  wire q_i_1__0_n_0;
  wire q_reg_0;
  wire reset_IBUF;
  wire [0:0]salida_OBUF;

  LUT6 #(
    .INIT(64'h0008000000080008)) 
    inicio_cuenta_i_1
       (.I0(q),
        .I1(q_0),
        .I2(q_1),
        .I3(reset_IBUF),
        .I4(inicio_cuenta),
        .I5(fin_cuenta),
        .O(q_reg_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_i_1__0
       (.I0(q),
        .O(q_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    q_reg
       (.C(salida_OBUF),
        .CE(1'b1),
        .D(q_i_1__0_n_0),
        .Q(q),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h00A2)) 
    \salida_OBUF[1]_inst_i_1 
       (.I0(q),
        .I1(fin_cuenta),
        .I2(inicio_cuenta),
        .I3(reset_IBUF),
        .O(salida_OBUF));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_1
   (q,
    salida_OBUF,
    q_reg_0,
    q_0,
    reset_IBUF,
    inicio_cuenta,
    fin_cuenta,
    q_1);
  output q;
  output [0:0]salida_OBUF;
  output q_reg_0;
  input q_0;
  input reset_IBUF;
  input inicio_cuenta;
  input fin_cuenta;
  input q_1;

  wire fin_cuenta;
  wire inicio_cuenta;
  wire q;
  wire q_0;
  wire q_1;
  wire q_i_1__1_n_0;
  wire q_reg_0;
  wire reset_IBUF;
  wire [0:0]salida_OBUF;

  LUT6 #(
    .INIT(64'h0000000008000808)) 
    fin_cuenta_i_1
       (.I0(q),
        .I1(q_0),
        .I2(reset_IBUF),
        .I3(inicio_cuenta),
        .I4(fin_cuenta),
        .I5(q_1),
        .O(q_reg_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_i_1__1
       (.I0(q),
        .O(q_i_1__1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    q_reg
       (.C(salida_OBUF),
        .CE(1'b1),
        .D(q_i_1__1_n_0),
        .Q(q),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFFF2)) 
    \salida_OBUF[2]_inst_i_1 
       (.I0(fin_cuenta),
        .I1(inicio_cuenta),
        .I2(reset_IBUF),
        .I3(q),
        .O(salida_OBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
