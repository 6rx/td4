`timescale 1ns / 1ps

module contador_tb();

   wire [2:0] salida_tb;
   reg       reset_tb;
   reg       clk_tb;       

   initial begin
      clk_tb = 1'b0;
      reset_tb = 1'b0;
      #20
        reset_tb = 1'b1;
      #100
        reset_tb = 1'b0;
   end

   initial begin forever #10 clk_tb = ~clk_tb; end

   contador uut_contador(
                         .reset(reset_tb),
                         .salida(salida_tb[2:0]),
                         .clk(clk_tb));
   
endmodule