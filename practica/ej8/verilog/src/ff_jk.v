`timescale 1ns / 1ps

module ff_jk(
             input s,
             input r,
             input j,
             input k,
             output q,
             output q_n,
             input clk);

   reg             q_reg;

   always@(posedge clk or posedge r or posedge s) begin
      if(r) begin
         q_reg <= 1'b0;
      end else if(s) begin
         q_reg <= 1'b1;
      end else begin
         case ({j,k})
           2'b00: begin q_reg <= q_reg; end
           2'b01: begin q_reg <= 0; end
           2'b10: begin q_reg <= 1; end
           2'b11: begin q_reg <= ~q_reg; end
           default: begin q_reg <= q_reg; end
         endcase
      end
   end

   assign q = q_reg;
   assign q_n = ~q_reg;

endmodule