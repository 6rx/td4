`timescale 1ns / 1ps

module contador(
                output [2:0] salida,
                input        reset,
                input        clk);

   wire                      reset_ff;
   wire [2:0]                salidas_ff;                     
   reg                       fin_cuenta;
   reg                       inicio_cuenta;
   
   always@(posedge clk) begin
      fin_cuenta <= ~salidas_ff[2:0] == 3'd7;
   end
   always@(negedge clk) begin
      inicio_cuenta <= ~salidas_ff[2:0] == 3'd3;
   end
   assign reset_ff = reset | (fin_cuenta & ~inicio_cuenta);

   ff_jk ff_jk_0(
                    .s(1'b0),
                    .r(reset_ff),
                    .j(1'b1),
                    .k(1'b1),
                    .q(salidas_ff[0]),
                    .q_n(),
                    .clk(clk));

   ff_jk ff_jk_1(
                    .s(1'b0),
                    .r(reset_ff),
                    .j(1'b1),
                    .k(1'b1),
                    .q(salidas_ff[1]),
                    .q_n(),
                    .clk(salidas_ff[0]));

   ff_jk ff_jk_2(
                    .s(reset_ff),
                    .r(1'b0),
                    .j(1'b1),
                    .k(1'b1),
                    .q(salidas_ff[2]),
                    .q_n(),
                    .clk(salidas_ff[1]));

   assign salida[2:0] = ~salidas_ff[2:0];

endmodule