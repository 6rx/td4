--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº07: Contador par-impar
-- Implementacion: top_contador.vhd, ffjk.vhd
-- Simulacion: tb_top_contador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity contador is port(
  salida : out std_logic_vector(2 downto 0);
  rst : in std_logic;
  x : in std_logic;
  clk : in std_logic);
end contador;

architecture logica of contador is

  component ff_jk port(
    j, k, s, r : in std_logic;
    entrada : in std_logic;
    salida : out std_logic;
    clk : in std_logic);
  end component;

  signal salidas_signal : std_logic_vector(2 downto 0);
  signal j_signals : std_logic_vector(2 downto 0);
  signal k_signals : std_logic_vector(2 downto 0);

begin

  ff_jk_0 : ff_jk port map(
    s => '0',
    r => rst,
    j => j_signals(0),
    k => k_signals(0),
    entrada => '1',
    salida => salidas_signal(0),
    clk => clk);

  ff_jk_1 : ff_jk port map(
    s => '0',
    r => rst,
    j => j_signals(1),
    k => k_signals(1),
    entrada => '1',
    salida => salidas_signal(1),
    clk => clk);

  ff_jk_2 : ff_jk port map(
    s => '0',
    r => rst,
    j => j_signals(2),
    k => k_signals(2),
    entrada => '1',
    salida => salidas_signal(2),
    clk => clk);

  contador_proc : process(clk) begin
    if(rising_edge(clk)) then
       j_signals <= salidas_signal(1) & '1' & x;
       k_signals <= salidas_signal(1) & '1' & not(x);
    end if;
  end process;

  salida <= not(salidas_signal);

end logica;
