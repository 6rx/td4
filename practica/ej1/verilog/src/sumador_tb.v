`timescale 1ns / 1ps

module sumador_tb();

   reg a_tb;
   reg b_tb;
   reg enable_tb;
   wire [1:0] resultado_tb;

   initial begin
      a_tb = 1'b0;
      b_tb = 1'b0;
      enable_tb = 1'b0;
   end
   
   initial begin forever #100 a_tb = ~a_tb; end
   initial begin forever #200 b_tb = ~b_tb; end
   initial begin forever #400 enable_tb = ~enable_tb; end
   
   sumador uut_sumador(
                       .resultado       (resultado_tb[1:0]),
                       .a               (a_tb),
                       .b               (b_tb),
                       .enable          (enable_tb));

endmodule
