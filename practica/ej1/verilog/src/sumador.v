`timescale 1ns / 1ps

module sumador(
    input a,
    input b,
    input enable,
    output reg [1:0] resultado
    );
    
    always@(*) begin
        if(enable == 0) begin
            resultado = 2'b11;
        end else if (a == 0 & b == 0) begin
            resultado = 2'b00;
        end else if (a == 0 & b == 1) begin
            resultado = 2'b01;
        end else if (a == 1 & b == 0) begin
            resultado = 2'b01;
        end else if (a == 1 & b == 1) begin
            resultado = 2'b10;
        end
    end
endmodule
