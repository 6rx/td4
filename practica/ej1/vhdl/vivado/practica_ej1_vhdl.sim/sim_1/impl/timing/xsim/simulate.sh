#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.2 (64-bit)
#
# Filename    : simulate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for simulating the design by launching the simulator
#
# Generated by Vivado on Mon Apr 20 10:00:32 -03 2020
# SW Build 2708876 on Wed Nov  6 21:39:14 MST 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: simulate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xsim sumador_test_time_impl -key {Post-Implementation:sim_1:Timing:sumador_test} -tclbatch sumador_test.tcl -log simulate.log"
xsim sumador_test_time_impl -key {Post-Implementation:sim_1:Timing:sumador_test} -tclbatch sumador_test.tcl -log simulate.log

