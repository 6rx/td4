--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº03: Decodificador BCD a Decimal
-- Implementacion: top_bcd2decimal.vhd
-- Simulacion: tb_top_bcd2decimal.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bcd2decimal_tb is
end bcd2decimal_tb;

architecture testbench of bcd2decimal_tb is

  component bcd2decimal port(
    input : in std_logic_vector(3 downto 0);
    salida : out std_logic_vector(9 downto 0);
    reset : in std_logic;
    clk : in std_logic);
  end component;

  signal input_tb : std_logic_vector(3 downto 0);
  signal salida_tb : std_logic_vector(9 downto 0);
  signal reset_tb : std_logic;
  signal clk_tb : std_logic;

begin

  uut_bcd2decimal : bcd2decimal port map(
    input => input_tb,
    salida => salida_tb,
    reset => reset_tb,
    clk => clk_tb);

  clock_tb : process
    constant periodo : time := 25ns;
  begin
    clk_tb <= '0'; wait for periodo;
    clk_tb <= '1'; wait for periodo;
  end process;

  inputs_tb : process
    constant periodo : time := 50ns;
  begin
    input_tb <= "XXXX";
    reset_tb <= '1';
    wait for periodo;
    assert ( salida_tb(9 downto 0) = "0000000000" ) report "Falla en el reset, input todo en X" severity error;

    reset_tb <= '0'; -- Ya desactivo el reset para los prox tests
    input_tb <= "0000";
    wait for periodo;
    
    loop
      input_tb <= input_tb + '1';
      wait for periodo;
    end loop;
  end process;
  
end testbench;