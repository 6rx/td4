--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº03: Decodificador BCD a Decimal
-- Implementacion: top_bcd2decimal.vhd
-- Simulacion: tb_top_bcd2decimal.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity bcd2decimal is port(
  input : in std_logic_vector(3 downto 0);
  salida : out std_logic_vector(9 downto 0);
  reset : in std_logic;
  clk : in std_logic);
end bcd2decimal;
                    
architecture logica of bcd2decimal is

    signal salida_signal : std_logic_vector(9 downto 0);

begin

  decodificacion : process (input)
  begin
    case input is
      when "0000" => salida_signal <= "0000000001";
      when "0001" => salida_signal <= "0000000010";
      when "0010" => salida_signal <= "0000000100";
      when "0011" => salida_signal <= "0000001000";
      when "0100" => salida_signal <= "0000010000";
      when "0101" => salida_signal <= "0000100000";
      when "0110" => salida_signal <= "0001000000";
      when "0111" => salida_signal <= "0010000000";
      when "1000" => salida_signal <= "0100000000";
      when "1001" => salida_signal <= "1000000000";
      when others => salida_signal <= "0000000000";
    end case;
  end process;
  
  asignacion : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        salida <= "0000000000";
      else
        salida <= salida_signal;
      end if;
    end if;
  end process;
  
end logica;