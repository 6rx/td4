// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 07:27:18 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej3/vhdl/vivado/practica_ej3_vhdl.sim/sim_1/impl/timing/xsim/bcd2decimal_tb_time_impl.v
// Design      : bcd2decimal
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "2a85f3e" *) 
(* NotValidForBitStream *)
module bcd2decimal
   (\input ,
    salida,
    reset,
    clk);
  input [3:0]\input ;
  output [9:0]salida;
  input reset;
  input clk;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire [3:0]\input ;
  wire reset;
  wire reset_IBUF;
  wire [9:0]salida;
  wire \salida[0]_i_1_n_0 ;
  wire \salida[1]_i_1_n_0 ;
  wire \salida[2]_i_1_n_0 ;
  wire \salida[3]_i_1_n_0 ;
  wire \salida[4]_i_1_n_0 ;
  wire \salida[5]_i_1_n_0 ;
  wire \salida[6]_i_1_n_0 ;
  wire \salida[7]_i_1_n_0 ;
  wire [9:0]salida_OBUF;
  wire \salida_reg[9]_i_2_n_0 ;
  wire \salida_reg[9]_i_3_n_0 ;
  wire \salida_reg[9]_i_4_n_0 ;
  wire \salida_reg[9]_i_5_n_0 ;
  wire [9:8]salida_signal;

initial begin
 $sdf_annotate("bcd2decimal_tb_time_impl.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \salida[0]_i_1 
       (.I0(\salida_reg[9]_i_4_n_0 ),
        .I1(\salida_reg[9]_i_3_n_0 ),
        .I2(\salida_reg[9]_i_5_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \salida[1]_i_1 
       (.I0(\salida_reg[9]_i_4_n_0 ),
        .I1(\salida_reg[9]_i_5_n_0 ),
        .I2(\salida_reg[9]_i_3_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \salida[2]_i_1 
       (.I0(\salida_reg[9]_i_4_n_0 ),
        .I1(\salida_reg[9]_i_3_n_0 ),
        .I2(\salida_reg[9]_i_5_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \salida[3]_i_1 
       (.I0(\salida_reg[9]_i_4_n_0 ),
        .I1(\salida_reg[9]_i_3_n_0 ),
        .I2(\salida_reg[9]_i_5_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \salida[4]_i_1 
       (.I0(\salida_reg[9]_i_3_n_0 ),
        .I1(\salida_reg[9]_i_5_n_0 ),
        .I2(\salida_reg[9]_i_4_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \salida[5]_i_1 
       (.I0(\salida_reg[9]_i_5_n_0 ),
        .I1(\salida_reg[9]_i_3_n_0 ),
        .I2(\salida_reg[9]_i_4_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \salida[6]_i_1 
       (.I0(\salida_reg[9]_i_3_n_0 ),
        .I1(\salida_reg[9]_i_5_n_0 ),
        .I2(\salida_reg[9]_i_4_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \salida[7]_i_1 
       (.I0(\salida_reg[9]_i_3_n_0 ),
        .I1(\salida_reg[9]_i_5_n_0 ),
        .I2(\salida_reg[9]_i_4_n_0 ),
        .I3(\salida_reg[9]_i_2_n_0 ),
        .O(\salida[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \salida[8]_i_1 
       (.I0(\salida_reg[9]_i_2_n_0 ),
        .I1(\salida_reg[9]_i_4_n_0 ),
        .I2(\salida_reg[9]_i_3_n_0 ),
        .I3(\salida_reg[9]_i_5_n_0 ),
        .O(salida_signal[8]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \salida[9]_i_1 
       (.I0(\salida_reg[9]_i_2_n_0 ),
        .I1(\salida_reg[9]_i_3_n_0 ),
        .I2(\salida_reg[9]_i_4_n_0 ),
        .I3(\salida_reg[9]_i_5_n_0 ),
        .O(salida_signal[9]));
  OBUF \salida_OBUF[0]_inst 
       (.I(salida_OBUF[0]),
        .O(salida[0]));
  OBUF \salida_OBUF[1]_inst 
       (.I(salida_OBUF[1]),
        .O(salida[1]));
  OBUF \salida_OBUF[2]_inst 
       (.I(salida_OBUF[2]),
        .O(salida[2]));
  OBUF \salida_OBUF[3]_inst 
       (.I(salida_OBUF[3]),
        .O(salida[3]));
  OBUF \salida_OBUF[4]_inst 
       (.I(salida_OBUF[4]),
        .O(salida[4]));
  OBUF \salida_OBUF[5]_inst 
       (.I(salida_OBUF[5]),
        .O(salida[5]));
  OBUF \salida_OBUF[6]_inst 
       (.I(salida_OBUF[6]),
        .O(salida[6]));
  OBUF \salida_OBUF[7]_inst 
       (.I(salida_OBUF[7]),
        .O(salida[7]));
  OBUF \salida_OBUF[8]_inst 
       (.I(salida_OBUF[8]),
        .O(salida[8]));
  OBUF \salida_OBUF[9]_inst 
       (.I(salida_OBUF[9]),
        .O(salida[9]));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[0]_i_1_n_0 ),
        .Q(salida_OBUF[0]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[1]_i_1_n_0 ),
        .Q(salida_OBUF[1]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[2]_i_1_n_0 ),
        .Q(salida_OBUF[2]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[3]_i_1_n_0 ),
        .Q(salida_OBUF[3]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[4]_i_1_n_0 ),
        .Q(salida_OBUF[4]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[5]_i_1_n_0 ),
        .Q(salida_OBUF[5]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[6]_i_1_n_0 ),
        .Q(salida_OBUF[6]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\salida[7]_i_1_n_0 ),
        .Q(salida_OBUF[7]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(salida_signal[8]),
        .Q(salida_OBUF[8]),
        .R(reset_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \salida_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(salida_signal[9]),
        .Q(salida_OBUF[9]),
        .R(reset_IBUF));
  IBUF \salida_reg[9]_i_2 
       (.I(\input [3]),
        .O(\salida_reg[9]_i_2_n_0 ));
  IBUF \salida_reg[9]_i_3 
       (.I(\input [1]),
        .O(\salida_reg[9]_i_3_n_0 ));
  IBUF \salida_reg[9]_i_4 
       (.I(\input [2]),
        .O(\salida_reg[9]_i_4_n_0 ));
  IBUF \salida_reg[9]_i_5 
       (.I(\input [0]),
        .O(\salida_reg[9]_i_5_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
