// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 10:39:48 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej6/vhdl/vivado/practica_ej6_vhdl.sim/sim_1/impl/timing/xsim/ff_jk_tb_time_impl.v
// Design      : ff_jk
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "53aa4fbb" *) 
(* NotValidForBitStream *)
module ff_jk
   (entrada,
    salida,
    j,
    k,
    s,
    r,
    clk);
  input entrada;
  output salida;
  input j;
  input k;
  input s;
  input r;
  input clk;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire entrada;
  wire entrada_IBUF;
  wire j;
  wire j_IBUF;
  wire k;
  wire k_IBUF;
  wire q_C_i_1_n_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_i_1_n_0;
  wire q_reg_LDC_i_2_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_n_0;
  wire r;
  wire r_IBUF;
  wire s;
  wire s_IBUF;
  wire salida;
  wire salida_OBUF;

initial begin
 $sdf_annotate("ff_jk_tb_time_impl.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  IBUF entrada_IBUF_inst
       (.I(entrada),
        .O(entrada_IBUF));
  IBUF j_IBUF_inst
       (.I(j),
        .O(j_IBUF));
  IBUF k_IBUF_inst
       (.I(k),
        .O(k_IBUF));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h3A333AAA)) 
    q_C_i_1
       (.I0(j_IBUF),
        .I1(k_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_C_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_LDC_i_2_n_0),
        .D(q_C_i_1_n_0),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_LDC_i_2_n_0),
        .D(1'b1),
        .G(q_reg_LDC_i_1_n_0),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    q_reg_LDC_i_1
       (.I0(r_IBUF),
        .I1(entrada_IBUF),
        .I2(s_IBUF),
        .O(q_reg_LDC_i_1_n_0));
  LUT3 #(
    .INIT(8'hAE)) 
    q_reg_LDC_i_2
       (.I0(r_IBUF),
        .I1(s_IBUF),
        .I2(entrada_IBUF),
        .O(q_reg_LDC_i_2_n_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_C_i_1_n_0),
        .PRE(q_reg_LDC_i_1_n_0),
        .Q(q_reg_P_n_0));
  IBUF r_IBUF_inst
       (.I(r),
        .O(r_IBUF));
  IBUF s_IBUF_inst
       (.I(s),
        .O(s_IBUF));
  OBUF salida_OBUF_inst
       (.I(salida_OBUF),
        .O(salida));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    salida_OBUF_inst_i_1
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(salida_OBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
