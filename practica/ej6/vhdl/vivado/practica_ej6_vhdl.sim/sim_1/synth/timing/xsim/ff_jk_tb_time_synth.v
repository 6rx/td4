// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Apr  6 17:11:54 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej6/vhdl/vivado/practica_ej6_vhdl.sim/sim_1/synth/timing/xsim/ff_jk_tb_time_synth.v
// Design      : ff_jk
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* NotValidForBitStream *)
module ff_jk
   (entrada,
    salida,
    j,
    k,
    s,
    r,
    clk);
  input entrada;
  output salida;
  input j;
  input k;
  input s;
  input r;
  input clk;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire entrada;
  wire entrada_IBUF;
  wire j;
  wire j_IBUF;
  wire k;
  wire k_IBUF;
  wire q;
  wire q_i_1_n_0;
  wire r;
  wire r_IBUF;
  wire s;
  wire s_IBUF;
  wire salida;
  wire salida_OBUF;

initial begin
 $sdf_annotate("ff_jk_tb_time_synth.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  IBUF entrada_IBUF_inst
       (.I(entrada),
        .O(entrada_IBUF));
  IBUF j_IBUF_inst
       (.I(j),
        .O(j_IBUF));
  IBUF k_IBUF_inst
       (.I(k),
        .O(k_IBUF));
  LUT3 #(
    .INIT(8'h3A)) 
    q_i_1
       (.I0(j_IBUF),
        .I1(k_IBUF),
        .I2(q),
        .O(q_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_i_1_n_0),
        .Q(q),
        .R(1'b0));
  IBUF r_IBUF_inst
       (.I(r),
        .O(r_IBUF));
  IBUF s_IBUF_inst
       (.I(s),
        .O(s_IBUF));
  OBUF salida_OBUF_inst
       (.I(salida_OBUF),
        .O(salida));
  LUT4 #(
    .INIT(16'hE540)) 
    salida_OBUF_inst_i_1
       (.I0(r_IBUF),
        .I1(entrada_IBUF),
        .I2(s_IBUF),
        .I3(q),
        .O(salida_OBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
