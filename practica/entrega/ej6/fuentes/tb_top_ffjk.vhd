--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº06: Flip-Flop JK
-- Implementacion: top_ffjk.vhd
-- Simulacion: tb_top_ffjk.vhd
-- ACLARACION: El enunciado es medio ambiguo en cuanto a que quiere.
--	Pareciera que hay que hacer un JK, pero las entradas que
--	requiere se pueden interpretar como de JK, SR o tipo D.
--	Lo que se implemento es una mezcla de estos 3.
--	Así, este flip flop se puede usar tanto como JK, SR o D.
--------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ff_jk_tb is
end ff_jk_tb;

architecture testbench of ff_jk_tb is

  component ff_jk port(
    entrada : in std_logic;
    salida : out std_logic;
    j, k, s, r : in std_logic;
    clk : in std_logic);
  end component;

  signal entrada_tb, salida_tb, clk_tb : std_logic;
  signal s_tb, r_tb : std_logic;
  signal jk_tb : std_logic_vector(1 downto 0);
  
begin

  uut_ff_jk : ff_jk port map(
    entrada => entrada_tb,
    salida => salida_tb,
    j => jk_tb(1),
    k => jk_tb(0),
    r => r_tb,
    s => s_tb,
    clk => clk_tb);

  clock_tb_proc : process
    constant periodo : time := 25ns;
  begin
    clk_tb <= '0'; wait for periodo/4;
    clk_tb <= '1'; wait for periodo/4;
  end process;

  entrada_tb_proc : process
    constant periodo : time := 100ns;
  begin
    entrada_tb <= '0'; wait for periodo;
    entrada_tb <= '1'; wait for periodo;
  end process;

  sr_tb_proc : process
    constant periodo : time := 200ns;
  begin
    s_tb <= '0';
    r_tb <= '1';
    wait for periodo;
    s_tb <= '1';
    r_tb <= '0';
    wait for periodo;
    s_tb <= '0';
    r_tb <= '1';
    wait for periodo;
    s_tb <= '0';
    r_tb <= '0';
    wait;
  end process;

  jk_tb_proc : process
    constant periodo : time := 100ns;
  begin
    jk_tb <= "00";
    wait for periodo;
    loop
      jk_tb <= jk_tb + '1';
      wait for periodo;
    end loop;
  end process;

end testbench;
