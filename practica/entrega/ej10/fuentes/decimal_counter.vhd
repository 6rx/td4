--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº10: Cronómetro Up/Down de 0-59
-- Implementacion: top_cronometro.vhd, clk_divisor.vhd,
--                 bcd7seg.vhd, decimal_counter.vhd
-- Simulacion: tb_top_cronometro.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

entity decimal_counter is generic(
        max : integer := 9);
    port(
        clk : in std_logic;
        reset : in std_logic;
        overflow : out std_logic;
        increment : in std_logic;
        decrement : in std_logic;
        count : out std_logic_vector(3 downto 0));
end decimal_counter;

architecture logica of decimal_counter is
  
  signal s_count : std_logic_vector(3 downto 0);
  
begin
    
    counter : process (clk, reset) begin
        if(reset = '1') then
            s_count <= std_logic_vector(to_unsigned(0, 4));
        elsif (rising_edge(clk)) then
            if (increment = '1') then
                if (s_count >= max) then
                    s_count <= std_logic_vector(to_unsigned(0, 4));
                    overflow <= '1';
                else
                    s_count <= s_count + 1;
                    overflow <= '0';                    
                end if;
            elsif (decrement = '1') then
                if (s_count <= 0) then
                    s_count <= std_logic_vector(to_unsigned(max, 4));
                    overflow <= '1';                
                else                
                    s_count <= s_count - 1;
                    overflow <= '0';
                end if;
            else            
                s_count <= s_count;            
            end if;
        end if;
    end process;

    count <= s_count;
    
end logica;
