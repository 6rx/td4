--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº10: Cronómetro Up/Down de 0-59
-- Implementacion: top_cronometro.vhd, clk_divisor.vhd,
--                 bcd7seg.vhd, decimal_counter.vhd
-- Simulacion: tb_top_cronometro.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

entity clk_divisor is 	generic(
		div : integer);
	port(
		reset : in std_logic;
		input_clk : in std_logic;
		output_clk : out std_logic);
end clk_divisor;

architecture logica of clk_divisor is

  constant bits_div : integer := integer(log2(real(div)));
  signal s_counter : std_logic_vector(bits_div-1 downto 0);  
  signal s_out_clk : std_logic;  

begin

  proc_counter : process(input_clk, reset) begin
    if(reset = '1') then
        s_counter <= std_logic_vector(to_unsigned(0, bits_div));
        s_out_clk <= '0';
    elsif(rising_edge(input_clk)) then
        if(s_counter >= div/2-1) then
            s_counter <= std_logic_vector(to_unsigned(0, bits_div));
            s_out_clk <= not(s_out_clk);
        else
            s_counter <= s_counter + 1;
        end if;
    end if;
  end process;
  
  output_clk <= s_out_clk;
  
end logica;
