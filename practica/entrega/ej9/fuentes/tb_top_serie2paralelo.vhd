--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº09: Conversor serie a paralelo
-- Implementacion: top_serie2paralelo.vhd, ffjk.vhd
-- Simulacion: tb_top_serie2paralelo.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity serie2paralelo_tb is generic(
  ancho : integer := 16);
end serie2paralelo_tb;

architecture testbench of serie2paralelo_tb is

  component serie2paralelo
    port(
      entrada : in std_logic;
      reset, carga : in std_logic;
      q : out std_logic_vector(ancho-1 downto 0);
      clk : in std_logic);
    end component;

  signal entrada_tb, reset_tb, carga_tb, clk_tb : std_logic;
  signal q_tb : std_logic_vector(ancho-1 downto 0);

begin

  uut_serie2paralelo : serie2paralelo
    port map(
      entrada => entrada_tb,
      reset => reset_tb,
      carga => carga_tb,
      q => q_tb,
      clk => clk_tb);

  clock_tb : process
    constant periodo : time := 25ns;
  begin
    clk_tb <= '0'; wait for periodo;
    clk_tb <= '1'; wait for periodo;
  end process;

  inputs_tb : process
    constant periodo : time := 50ns;
  begin
    carga_tb <= 'X'; entrada_tb <= 'X'; reset_tb <= '0'; wait for periodo;
    carga_tb <= 'X'; entrada_tb <= 'X'; reset_tb <= '1'; wait for periodo;
    carga_tb <= '0'; entrada_tb <= '0'; reset_tb <= '0'; wait for periodo;
    loop
      carga_tb <= '1'; wait for periodo;
      entrada_tb <= '1'; wait for periodo;
      entrada_tb <= '0'; wait for periodo;
      entrada_tb <= '1'; wait for periodo;
      entrada_tb <= '0'; wait for periodo;
      entrada_tb <= '1'; wait for periodo;
      entrada_tb <= '0'; wait for periodo;
      entrada_tb <= '0'; wait for periodo;
      entrada_tb <= '1'; wait for periodo;
    end loop;
  end process;
  
end testbench;