--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº06: Flip-Flop JK
-- Implementacion: top_ffjk.vhd
-- Simulacion: tb_top_ffjk.vhd
-- ACLARACION: El enunciado es medio ambiguo en cuanto a que quiere.
--	Pareciera que hay que hacer un JK, pero las entradas que
--	requiere se pueden interpretar como de JK, SR o tipo D.
--	Lo que se implemento es una mezcla de estos 3.
--	Así, este flip flop se puede usar tanto como JK, SR o D.
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ff_jk is port(
  entrada : in std_logic;
  salida : out std_logic;
  j, k, s, r : in std_logic;
  clk : in std_logic);
end ff_jk;

architecture logica of ff_jk is

  signal q : std_logic;
  signal jk : std_logic_vector(1 downto 0);

begin

  jk <= j & k;

  flip_flop : process (clk, r, s) begin
    if(r='1') then
        q <= '0';
    elsif(s='1') then
        q <= entrada;
    elsif(rising_edge(clk)) then
        case jk is
          when "00" => q <= q;
          when "01" => q <= '0';
          when "10" => q <= '1';
          when "11" => q <= not q;
          when others => q <= q;
        end case;
    end if;
  end process;
  
  salida <= q;

end logica;
