--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº07: Contador par-impar
-- Implementacion: top_contador.vhd, ffjk.vhd
-- Simulacion: tb_top_contador.vhd
-- ACLARACION: Se vió que habíá una forma mucho mas simple de hacer
--  la cuenta par/impar, por lo que se implementó así, ocupa mucho
--  menos recursos y es más simple de ver.
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity contador_tb is
end contador_tb;

architecture testbench of contador_tb is

  component contador port(
    salida : out std_logic_vector(2 downto 0);
    rst : in std_logic;
    x : in std_logic;
    clk : in std_logic);
  end component;

  signal x_tb, rst_tb, clk_tb : std_logic;
  signal salida_tb : std_logic_vector(2 downto 0);
  
begin

  uut_contador : contador port map(
    salida => salida_tb,
    rst => rst_tb,
    x => x_tb,
    clk => clk_tb);

  clock_tb : process
    constant periodo : time := 50ns;
  begin
    clk_tb <= '0'; wait for periodo/2;
    clk_tb <= '1'; wait for periodo/2;
  end process;

  inputs_tb : process
    constant periodo : time := 50ns;
  begin
    x_tb <= 'X';
    rst_tb <= '0'; wait for periodo;
    rst_tb <= '1'; wait for periodo;

    x_tb <= '0';
    rst_tb <= '0';
    wait for periodo/2;

    loop
      x_tb <= not x_tb;
      wait for periodo*7;
    end loop;
  end process;

end testbench;
