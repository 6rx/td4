--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº05: Multiplexor 8 a 1
-- Implementacion: top_mux.vhd
-- Simulacion: tb_top_mux.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity mux_tb is
end mux_tb;

architecture testbench of mux_tb is
  component mux port(
    enable : in std_logic;
    input7, input6, input5, input4, input3, input2, input1, input0 : in std_logic;
    channel2, channel1, channel0 : in std_logic;
    salida : out std_logic);
  end component;

  signal enable_tb : std_logic;
  signal channel_tb : std_logic_vector(2 downto 0);
  signal input_tb : std_logic_vector(7 downto 0);
  signal salida_tb : std_logic;

begin

  uut_mux : mux port map(
    enable => enable_tb,
    salida => salida_tb,
    channel2 => channel_tb(2), channel1 => channel_tb(1), channel0 => channel_tb(0),
    input7 => input_tb(7), input6 => input_tb(6), input5 => input_tb(5),
    input4 => input_tb(4), input3 => input_tb(3), input2 => input_tb(2),
    input1 => input_tb(1), input0 => input_tb(0));

  channel_tb_proc : process
    constant periodo : time := 400ns;
  begin
    channel_tb <= "000"; wait for periodo;
    loop
      channel_tb <= channel_tb + '1';
      wait for periodo;
    end loop;
  end process;
  
  inputs_tb_proc : process
    constant periodo : time := 50ns;
  begin
    input_tb <= "XXXXXXXX";
    enable_tb <= '1';
    wait for periodo;
    enable_tb <= '0';
    wait for periodo;
    assert ( salida_tb = 'Z' ) report "Falla en enable deshabilitado, input todo en X" severity error;

    enable_tb <= '1'; -- ya lo dejo habilitado
    input_tb <= "00000000";
    wait for periodo;

    loop
      input_tb <= input_tb + '1';
      wait for periodo;
    end loop;
  end process;
end testbench;
