--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº05: Multiplexor 8 a 1
-- Implementacion: top_mux.vhd
-- Simulacion: tb_top_mux.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


entity mux is port(
  enable : in std_logic;
  input7, input6, input5, input4, input3, input2, input1, input0 : in std_logic;
  channel2, channel1, channel0 : in std_logic;
  salida : out std_logic);
end mux;

architecture logica of mux is

  signal selector : std_logic_vector(2 downto 0);
  signal entrada  : std_logic_vector(7 downto 0);
  signal seleccionado : std_logic;

begin

  entrada <= input7 & input6 & input5 & input4 & input3 & input2 & input1 & input0;
  selector <= channel2 & channel1 & channel0;

  codificacion : process (selector, entrada, enable)
  begin
    case selector is
      when "000" => seleccionado <= entrada(0);
      when "001" => seleccionado <= entrada(1);
      when "010" => seleccionado <= entrada(2);
      when "011" => seleccionado <= entrada(3);
      when "100" => seleccionado <= entrada(4);
      when "101" => seleccionado <= entrada(5);
      when "110" => seleccionado <= entrada(6);
      when "111" => seleccionado <= entrada(7);
      when others => seleccionado <= 'Z';
    end case;

    if(enable = '1') then
      salida <= seleccionado;
    else
      salida <= 'Z';
    end if;
  end process;

end logica;
