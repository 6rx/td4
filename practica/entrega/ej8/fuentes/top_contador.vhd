--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº08: Contador asíncrono
-- Implementacion: top_contador.vhd, ffjk.vhd
-- Simulacion: tb_top_contador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity contador is port(
  salida : out std_logic_vector(2 downto 0);
  reset : in std_logic;
  clk : in std_logic);

end contador;

architecture logica of contador is

  component ff_jk port(
    entrada : in std_logic;
    salida : out std_logic;
    j, k, s, r : in std_logic;
    clk : in std_logic);
  end component;

  signal reset_ff : std_logic;
  signal salidas_ff : std_logic_vector(2 downto 0);
  signal clk_ff : std_logic_vector(1 downto 0);

begin
    
  ff_jk_0 : ff_jk port map(
    salida => salidas_ff(0),
    j => '1', k => '1',
    s => reset, r => '0',
    entrada => '0',
    clk => clk);
  
  ff_jk_1 : ff_jk port map(
    salida => salidas_ff(1),
    j => '1', k => '1',
    s => reset, r => '0',
    entrada => '0',
    clk => salidas_ff(0));

  ff_jk_2 : ff_jk port map(
    salida => salidas_ff(2),
    j => '1', k => '1',
    s => reset, r => '0',
    entrada => '1',
    clk => salidas_ff(1));

    salida <= not(salidas_ff);
end logica;