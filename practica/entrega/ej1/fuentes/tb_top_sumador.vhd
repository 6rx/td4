--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº01: Sumador de un bit com "when/else"
-- Implementacion: top_sumador.vhd
-- Simulacion: top_tb_sumador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity sumador_test is
end sumador_test;

architecture simulacion of sumador_test is
  component sumador
    port (
      a : in std_logic;
      b : in std_logic;
      enable : in std_logic;
      resultado : out std_logic_vector(1 downto 0)
      );
  end component;

  signal test_a : std_logic;
  signal test_b : std_logic;
  signal test_enable : std_logic;
  signal test_resultado : std_logic_vector(1 downto 0);

begin
  uut_sumador: sumador
    port map(
        a => test_a,
        b => test_b,
        enable => test_enable,
        resultado => test_resultado);
        
    test_proceso : process
        constant periodo: time := 100ns;
        begin
            test_enable <= '0';
            test_a <= 'X';
            test_b <= 'X';
            wait for periodo;
            assert ( test_resultado(1 downto 0) = "11" ) report "Falla en el enable en 0, inputs X,X" severity error;

            test_enable <= '1'; -- Ya dejo habilitado el enable
                        
            test_a <= '0';
            test_b <= '0';
            wait for periodo;
            assert ( test_resultado(1 downto 0) = "00" ) report "Falla la suma de 0 y 0" severity error;
            
            test_a <= '0';
            test_b <= '1';
            wait for periodo;
            assert ( test_resultado(1 downto 0) = "01" ) report "Falla la suma de 0 y 1" severity error;
            
            test_a <= '1';
            test_b <= '0';
            wait for periodo;
            assert ( test_resultado(1 downto 0) = "01" ) report "Falla la suma de 1 y 0" severity error;
            
            test_a <= '1';
            test_b <= '1';
            wait for periodo;
            assert ( test_resultado(1 downto 0) = "10" ) report "Falla la suma de 1 y 1" severity error;
     end process;
end simulacion;
