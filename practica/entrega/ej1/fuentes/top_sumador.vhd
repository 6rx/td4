--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº01: Sumador de un bit com "when/else"
-- Implementacion: top_sumador.vhd
-- Simulacion: top_tb_sumador.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity sumador is
	port (
		a : in std_logic;
		b : in std_logic;
		enable : in std_logic;
		resultado : out std_logic_vector(1 downto 0)
	     );
end sumador;

architecture logica of sumador is
begin
    resultado <= "11" when (enable = '0') else
                 "00" when (a='0' and b='0') else
                 "10" when (a='1' and b='1') else
                 "01" when (a='1' or b='1');
end logica;
