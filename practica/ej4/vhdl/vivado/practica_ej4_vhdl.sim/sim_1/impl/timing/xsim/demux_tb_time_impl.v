// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 07:36:26 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej4/vhdl/vivado/practica_ej4_vhdl.sim/sim_1/impl/timing/xsim/demux_tb_time_impl.v
// Design      : demux
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "49a97cdd" *) 
(* NotValidForBitStream *)
module demux
   (reset,
    ce,
    \input ,
    \output ,
    clk);
  input reset;
  input ce;
  input [3:0]\input ;
  output [1:0]\output ;
  input clk;

  wire ce;
  wire ce_IBUF;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire [1:0]decodificacion;
  wire \decodificacion_tristate_oe[0]_i_1_n_0 ;
  wire \decodificacion_tristate_oe[1]_i_1_n_0 ;
  wire \decodificacion_tristate_oe_reg[0]_i_2_n_0 ;
  wire \decodificacion_tristate_oe_reg[1]_i_2_n_0 ;
  wire \decodificacion_tristate_oe_reg[1]_i_3_n_0 ;
  wire \decodificacion_tristate_oe_reg_n_0_[0] ;
  wire \decodificacion_tristate_oe_reg_n_0_[1] ;
  wire [3:0]\input ;
  wire [1:0]\output ;
  wire \output[1]_INST_0_i_2_n_0 ;
  wire \output[1]_INST_0_i_4_n_0 ;
  wire \output[1]_INST_0_i_5_n_0 ;
  wire \output[1]_INST_0_i_6_n_0 ;
  wire [1:0]output_OBUF;
  wire reset;
  wire reset_IBUF;

initial begin
 $sdf_annotate("demux_tb_time_impl.sdf",,,,"tool_control");
end
  IBUF ce_IBUF_inst
       (.I(ce),
        .O(ce_IBUF));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \decodificacion_tristate_oe[0]_i_1 
       (.I0(\decodificacion_tristate_oe_reg[1]_i_2_n_0 ),
        .I1(\decodificacion_tristate_oe_reg[0]_i_2_n_0 ),
        .O(\decodificacion_tristate_oe[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \decodificacion_tristate_oe[1]_i_1 
       (.I0(\decodificacion_tristate_oe_reg[1]_i_2_n_0 ),
        .I1(\decodificacion_tristate_oe_reg[1]_i_3_n_0 ),
        .O(\decodificacion_tristate_oe[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \decodificacion_tristate_oe_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\decodificacion_tristate_oe[0]_i_1_n_0 ),
        .Q(\decodificacion_tristate_oe_reg_n_0_[0] ),
        .R(1'b0));
  IBUF \decodificacion_tristate_oe_reg[0]_i_2 
       (.I(\input [1]),
        .O(\decodificacion_tristate_oe_reg[0]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \decodificacion_tristate_oe_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\decodificacion_tristate_oe[1]_i_1_n_0 ),
        .Q(\decodificacion_tristate_oe_reg_n_0_[1] ),
        .R(1'b0));
  IBUF \decodificacion_tristate_oe_reg[1]_i_2 
       (.I(\input [3]),
        .O(\decodificacion_tristate_oe_reg[1]_i_2_n_0 ));
  IBUF \decodificacion_tristate_oe_reg[1]_i_3 
       (.I(\input [2]),
        .O(\decodificacion_tristate_oe_reg[1]_i_3_n_0 ));
  OBUFT \output[0]_INST_0 
       (.I(output_OBUF[0]),
        .O(\output [0]),
        .T(\output[1]_INST_0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \output[0]_INST_0_i_1 
       (.I0(decodificacion[0]),
        .I1(reset_IBUF),
        .O(output_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \output[0]_INST_0_i_2 
       (.I0(\decodificacion_tristate_oe_reg_n_0_[0] ),
        .I1(\output[1]_INST_0_i_4_n_0 ),
        .O(decodificacion[0]));
  OBUFT \output[1]_INST_0 
       (.I(output_OBUF[1]),
        .O(\output [1]),
        .T(\output[1]_INST_0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \output[1]_INST_0_i_1 
       (.I0(decodificacion[1]),
        .I1(reset_IBUF),
        .O(output_OBUF[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \output[1]_INST_0_i_2 
       (.I0(ce_IBUF),
        .O(\output[1]_INST_0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \output[1]_INST_0_i_3 
       (.I0(\decodificacion_tristate_oe_reg_n_0_[1] ),
        .I1(\output[1]_INST_0_i_4_n_0 ),
        .O(decodificacion[1]));
  FDRE #(
    .INIT(1'b1)) 
    \output[1]_INST_0_i_4 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\output[1]_INST_0_i_5_n_0 ),
        .Q(\output[1]_INST_0_i_4_n_0 ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0116)) 
    \output[1]_INST_0_i_5 
       (.I0(\output[1]_INST_0_i_6_n_0 ),
        .I1(\decodificacion_tristate_oe_reg[0]_i_2_n_0 ),
        .I2(\decodificacion_tristate_oe_reg[1]_i_3_n_0 ),
        .I3(\decodificacion_tristate_oe_reg[1]_i_2_n_0 ),
        .O(\output[1]_INST_0_i_5_n_0 ));
  IBUF \output[1]_INST_0_i_6 
       (.I(\input [0]),
        .O(\output[1]_INST_0_i_6_n_0 ));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
