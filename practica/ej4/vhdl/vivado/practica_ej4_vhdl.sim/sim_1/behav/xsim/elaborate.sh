#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.2 (64-bit)
#
# Filename    : elaborate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for elaborating the compiled design
#
# Generated by Vivado on Mon Mar 30 14:33:38 -03 2020
# SW Build 2708876 on Wed Nov  6 21:39:14 MST 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: elaborate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xelab -wto 606a61b20a29485a9c0dbb6e077565cb --incr --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot demux_tb_behav xil_defaultlib.demux_tb -log elaborate.log"
xelab -wto 606a61b20a29485a9c0dbb6e077565cb --incr --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot demux_tb_behav xil_defaultlib.demux_tb -log elaborate.log

