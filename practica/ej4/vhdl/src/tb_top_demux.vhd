--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº04: Decodificador 4 a 2
-- Implementacion: top_demux.vhd
-- Simulacion: tb_top_demux.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity demux_tb is
end demux_tb;

architecture testbench of demux_tb is

  component demux port(
    reset : in std_logic;
    ce : in std_logic;
    input : in std_logic_vector(3 downto 0);
    output : out std_logic_vector (1 downto 0);
    clk : in std_logic);
  end component;

  signal reset_tb, ce_tb, clk_tb : std_logic;
  signal input_tb : std_logic_vector(3 downto 0);
  signal output_tb : std_logic_vector(1 downto 0);
  
begin

  uut_demux : demux port map(
    reset => reset_tb,
    ce => ce_tb,
    input => input_tb,
    output => output_tb, 
    clk => clk_tb);

  clock_tb : process
    constant periodo : time := 25ns;
  begin
    clk_tb <= '0'; wait for periodo/4;
    clk_tb <= '1'; wait for periodo/4;
  end process;

  inputs_tb : process
    constant periodo : time := 100ns;
  begin
    input_tb <= "XXXX";
    reset_tb <= 'X';
    ce_tb <= '0';
    wait for periodo;
    assert ( output_tb(1 downto 0) = "ZZ" ) report "Falla en el enable deshabilitado, el resto todo en X" severity error;

    input_tb <= "XXXX";
    reset_tb <= '1';
    ce_tb <= '1'; -- ya lo dejo habilitado
    wait for periodo;
    assert ( output_tb(1 downto 0) = "00" ) report "Falla en el reset, con enable habilitado, input todo en X" severity error;

    reset_tb <= '0'; -- Ya desactivo el reset para los prox tests
    input_tb <= "0000";
    wait for periodo;

    loop
      input_tb <= input_tb + '1';
      wait for periodo;
    end loop;
  end process;

end testbench;