--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº04: Decodificador 4 a 2
-- Implementacion: top_demux.vhd
-- Simulacion: tb_top_demux.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity demux is port(
  reset : in std_logic;
  ce : in std_logic;
  input : in std_logic_vector(3 downto 0);
  output : out std_logic_vector (1 downto 0);
  clk : in std_logic);
end demux;

architecture logica of demux is

  signal decodificacion : std_logic_vector(1 downto 0);
  
begin

  deocder_proc : process (clk)
  begin
    if(rising_edge(clk)) then
      case input is
        when "0001" => decodificacion <= "00";
        when "0010" => decodificacion <= "01";
        when "0100" => decodificacion <= "10";
        when "1000" => decodificacion <= "11";
        when others => decodificacion <= "ZZ";
      end case;
    end if;
  end process;

  asignacion : process(ce, reset, decodificacion)
  begin
    if (ce='0') then
      output <= "ZZ";
    elsif (reset='1') then
      output <= "00";
    else
      output <= decodificacion;
    end if;
  end process;

end logica;