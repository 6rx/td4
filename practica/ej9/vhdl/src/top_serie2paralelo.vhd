--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº09: Conversor serie a paralelo
-- Implementacion: top_serie2paralelo.vhd, ffjk.vhd
-- Simulacion: tb_top_serie2paralelo.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity serie2paralelo is
  generic(
    L : integer := 16);
  port(
  entrada : in std_logic;
  reset, carga : in std_logic;
  -- d : in std_logic (L-1 downto 0);
  q : out std_logic_vector(L-1 downto 0);
  clk : in std_logic);
end serie2paralelo;

architecture logica of serie2paralelo is

  component ff_jk is port(
    entrada : in std_logic;
    salida : out std_logic;
    j, k, s, r : in std_logic;
    clk : in std_logic);
  end component;

  signal salidas_signal : std_logic_vector(L-1 downto 0);
  
begin

    ff_jk_0 : ff_jk port map(
      j => '0',
      k => '0',
      s => carga,
      r => reset,
      entrada => entrada,
      salida => salidas_signal(0),
      clk => clk);

  generador : for i in 1 to L-1 generate
    ff_jk_X : ff_jk port map(
      j => '0',
      k => '0',
      s => carga,
      r => reset,
      entrada => salidas_signal(i-1),
      salida => salidas_signal(i),
      clk => clk);
  end generate generador;

  q <= salidas_signal;

end logica;