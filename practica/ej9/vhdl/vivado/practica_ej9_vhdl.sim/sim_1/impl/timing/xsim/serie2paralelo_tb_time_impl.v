// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 11:08:25 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej9/vhdl/vivado/practica_ej9_vhdl.sim/sim_1/impl/timing/xsim/serie2paralelo_tb_time_impl.v
// Design      : serie2paralelo
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module ff_jk
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF,
    entrada_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;
  input entrada_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire entrada_IBUF;
  wire [0:0]q_OBUF;
  wire q_reg_C_n_0;
  wire q_reg_LDC_i_1__14_n_0;
  wire q_reg_LDC_i_2__14_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[0]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_LDC_i_2__14_n_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_LDC_i_2__14_n_0),
        .D(1'b1),
        .G(q_reg_LDC_i_1__14_n_0),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  LUT3 #(
    .INIT(8'h40)) 
    q_reg_LDC_i_1__14
       (.I0(reset_IBUF),
        .I1(entrada_IBUF),
        .I2(carga_IBUF),
        .O(q_reg_LDC_i_1__14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  LUT3 #(
    .INIT(8'hAE)) 
    q_reg_LDC_i_2__14
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(entrada_IBUF),
        .O(q_reg_LDC_i_2__14_n_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_LDC_i_1__14_n_0),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_0
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[10]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__9
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__9
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_1
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[11]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__10
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__10
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_10
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[5]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__4
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__4
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_11
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[6]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__5
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__5
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_12
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[7]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__6
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__6
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_13
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[8]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__7
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__7
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_14
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[9]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__8
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__8
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_2
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[12]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__11
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__11
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_3
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[13]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__12
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__12
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_4
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[14]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__13
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__13
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_5
   (q_OBUF,
    \q[15] ,
    \q[15]_0 );
  output [0:0]q_OBUF;
  input \q[15] ;
  input \q[15]_0 ;

  wire \q[15] ;
  wire \q[15]_0 ;
  wire [0:0]q_OBUF;

  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(\q[15]_0 ),
        .D(1'b1),
        .G(\q[15] ),
        .GE(1'b1),
        .Q(q_OBUF));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_6
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[1]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__0
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__0
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_7
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[2]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__1
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__1
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_8
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[3]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__2
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__2
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ORIG_REF_NAME = "ff_jk" *) 
module ff_jk_9
   (q_OBUF,
    q_reg_P_0,
    q_reg_P_1,
    q_reg_P_2,
    q_reg_C_0,
    clk_IBUF_BUFG,
    reset_IBUF,
    carga_IBUF);
  output [0:0]q_OBUF;
  output q_reg_P_0;
  output q_reg_P_1;
  input q_reg_P_2;
  input q_reg_C_0;
  input clk_IBUF_BUFG;
  input reset_IBUF;
  input carga_IBUF;

  wire carga_IBUF;
  wire clk_IBUF_BUFG;
  wire [0:0]q_OBUF;
  wire q_reg_C_0;
  wire q_reg_C_n_0;
  wire q_reg_LDC_n_0;
  wire q_reg_P_0;
  wire q_reg_P_1;
  wire q_reg_P_2;
  wire q_reg_P_n_0;
  wire reset_IBUF;

  LUT3 #(
    .INIT(8'hB8)) 
    \q_OBUF[4]_inst_i_1 
       (.I0(q_reg_P_n_0),
        .I1(q_reg_LDC_n_0),
        .I2(q_reg_C_n_0),
        .O(q_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    q_reg_C
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(q_reg_C_0),
        .D(q_OBUF),
        .Q(q_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    q_reg_LDC
       (.CLR(q_reg_C_0),
        .D(1'b1),
        .G(q_reg_P_2),
        .GE(1'b1),
        .Q(q_reg_LDC_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h45400000)) 
    q_reg_LDC_i_1__3
       (.I0(reset_IBUF),
        .I1(q_reg_P_n_0),
        .I2(q_reg_LDC_n_0),
        .I3(q_reg_C_n_0),
        .I4(carga_IBUF),
        .O(q_reg_P_1));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hAEAAAEEE)) 
    q_reg_LDC_i_2__3
       (.I0(reset_IBUF),
        .I1(carga_IBUF),
        .I2(q_reg_P_n_0),
        .I3(q_reg_LDC_n_0),
        .I4(q_reg_C_n_0),
        .O(q_reg_P_0));
  FDPE #(
    .INIT(1'b1)) 
    q_reg_P
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(q_OBUF),
        .PRE(q_reg_P_2),
        .Q(q_reg_P_n_0));
endmodule

(* ECO_CHECKSUM = "a2393ff2" *) (* L = "16" *) 
(* NotValidForBitStream *)
module serie2paralelo
   (entrada,
    reset,
    carga,
    q,
    clk);
  input entrada;
  input reset;
  input carga;
  output [15:0]q;
  input clk;

  wire carga;
  wire carga_IBUF;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire entrada;
  wire entrada_IBUF;
  wire ff_jk_0_n_1;
  wire ff_jk_0_n_2;
  wire \generador[10].ff_jk_X_n_1 ;
  wire \generador[10].ff_jk_X_n_2 ;
  wire \generador[11].ff_jk_X_n_1 ;
  wire \generador[11].ff_jk_X_n_2 ;
  wire \generador[12].ff_jk_X_n_1 ;
  wire \generador[12].ff_jk_X_n_2 ;
  wire \generador[13].ff_jk_X_n_1 ;
  wire \generador[13].ff_jk_X_n_2 ;
  wire \generador[14].ff_jk_X_n_1 ;
  wire \generador[14].ff_jk_X_n_2 ;
  wire \generador[1].ff_jk_X_n_1 ;
  wire \generador[1].ff_jk_X_n_2 ;
  wire \generador[2].ff_jk_X_n_1 ;
  wire \generador[2].ff_jk_X_n_2 ;
  wire \generador[3].ff_jk_X_n_1 ;
  wire \generador[3].ff_jk_X_n_2 ;
  wire \generador[4].ff_jk_X_n_1 ;
  wire \generador[4].ff_jk_X_n_2 ;
  wire \generador[5].ff_jk_X_n_1 ;
  wire \generador[5].ff_jk_X_n_2 ;
  wire \generador[6].ff_jk_X_n_1 ;
  wire \generador[6].ff_jk_X_n_2 ;
  wire \generador[7].ff_jk_X_n_1 ;
  wire \generador[7].ff_jk_X_n_2 ;
  wire \generador[8].ff_jk_X_n_1 ;
  wire \generador[8].ff_jk_X_n_2 ;
  wire \generador[9].ff_jk_X_n_1 ;
  wire \generador[9].ff_jk_X_n_2 ;
  wire [15:0]q;
  wire [15:0]q_OBUF;
  wire reset;
  wire reset_IBUF;

initial begin
 $sdf_annotate("serie2paralelo_tb_time_impl.sdf",,,,"tool_control");
end
  IBUF carga_IBUF_inst
       (.I(carga),
        .O(carga_IBUF));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  IBUF entrada_IBUF_inst
       (.I(entrada),
        .O(entrada_IBUF));
  ff_jk ff_jk_0
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .entrada_IBUF(entrada_IBUF),
        .q_OBUF(q_OBUF[0]),
        .q_reg_P_0(ff_jk_0_n_1),
        .q_reg_P_1(ff_jk_0_n_2),
        .reset_IBUF(reset_IBUF));
  ff_jk_0 \generador[10].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[10]),
        .q_reg_C_0(\generador[9].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[10].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[10].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[9].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_1 \generador[11].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[11]),
        .q_reg_C_0(\generador[10].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[11].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[11].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[10].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_2 \generador[12].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[12]),
        .q_reg_C_0(\generador[11].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[12].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[12].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[11].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_3 \generador[13].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[13]),
        .q_reg_C_0(\generador[12].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[13].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[13].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[12].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_4 \generador[14].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[14]),
        .q_reg_C_0(\generador[13].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[14].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[14].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[13].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_5 \generador[15].ff_jk_X 
       (.\q[15] (\generador[14].ff_jk_X_n_2 ),
        .\q[15]_0 (\generador[14].ff_jk_X_n_1 ),
        .q_OBUF(q_OBUF[15]));
  ff_jk_6 \generador[1].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[1]),
        .q_reg_C_0(ff_jk_0_n_1),
        .q_reg_P_0(\generador[1].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[1].ff_jk_X_n_2 ),
        .q_reg_P_2(ff_jk_0_n_2),
        .reset_IBUF(reset_IBUF));
  ff_jk_7 \generador[2].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[2]),
        .q_reg_C_0(\generador[1].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[2].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[2].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[1].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_8 \generador[3].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[3]),
        .q_reg_C_0(\generador[2].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[3].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[3].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[2].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_9 \generador[4].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[4]),
        .q_reg_C_0(\generador[3].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[4].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[4].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[3].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_10 \generador[5].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[5]),
        .q_reg_C_0(\generador[4].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[5].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[5].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[4].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_11 \generador[6].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[6]),
        .q_reg_C_0(\generador[5].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[6].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[6].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[5].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_12 \generador[7].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[7]),
        .q_reg_C_0(\generador[6].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[7].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[7].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[6].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_13 \generador[8].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[8]),
        .q_reg_C_0(\generador[7].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[8].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[8].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[7].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  ff_jk_14 \generador[9].ff_jk_X 
       (.carga_IBUF(carga_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .q_OBUF(q_OBUF[9]),
        .q_reg_C_0(\generador[8].ff_jk_X_n_1 ),
        .q_reg_P_0(\generador[9].ff_jk_X_n_1 ),
        .q_reg_P_1(\generador[9].ff_jk_X_n_2 ),
        .q_reg_P_2(\generador[8].ff_jk_X_n_2 ),
        .reset_IBUF(reset_IBUF));
  OBUF \q_OBUF[0]_inst 
       (.I(q_OBUF[0]),
        .O(q[0]));
  OBUF \q_OBUF[10]_inst 
       (.I(q_OBUF[10]),
        .O(q[10]));
  OBUF \q_OBUF[11]_inst 
       (.I(q_OBUF[11]),
        .O(q[11]));
  OBUF \q_OBUF[12]_inst 
       (.I(q_OBUF[12]),
        .O(q[12]));
  OBUF \q_OBUF[13]_inst 
       (.I(q_OBUF[13]),
        .O(q[13]));
  OBUF \q_OBUF[14]_inst 
       (.I(q_OBUF[14]),
        .O(q[14]));
  OBUF \q_OBUF[15]_inst 
       (.I(q_OBUF[15]),
        .O(q[15]));
  OBUF \q_OBUF[1]_inst 
       (.I(q_OBUF[1]),
        .O(q[1]));
  OBUF \q_OBUF[2]_inst 
       (.I(q_OBUF[2]),
        .O(q[2]));
  OBUF \q_OBUF[3]_inst 
       (.I(q_OBUF[3]),
        .O(q[3]));
  OBUF \q_OBUF[4]_inst 
       (.I(q_OBUF[4]),
        .O(q[4]));
  OBUF \q_OBUF[5]_inst 
       (.I(q_OBUF[5]),
        .O(q[5]));
  OBUF \q_OBUF[6]_inst 
       (.I(q_OBUF[6]),
        .O(q[6]));
  OBUF \q_OBUF[7]_inst 
       (.I(q_OBUF[7]),
        .O(q[7]));
  OBUF \q_OBUF[8]_inst 
       (.I(q_OBUF[8]),
        .O(q[8]));
  OBUF \q_OBUF[9]_inst 
       (.I(q_OBUF[9]),
        .O(q[9]));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
