// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Apr 21 14:34:57 2020
// Host        : snsv running 64-bit unknown
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/nacho/repos/sexto/td4/practica/ej10/vhdl/vivado/practica_ej10_vhdl.sim/sim_1/impl/timing/xsim/cronometro_tb_time_impl.v
// Design      : cronometro
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module bcd7seg
   (salida_7seg_1_OBUF,
    Q);
  output [6:0]salida_7seg_1_OBUF;
  input [3:0]Q;

  wire [3:0]Q;
  wire [6:0]salida_7seg_1_OBUF;

  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \salida_7seg_1_OBUF[0]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(salida_7seg_1_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h5390)) 
    \salida_7seg_1_OBUF[1]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(salida_7seg_1_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h5710)) 
    \salida_7seg_1_OBUF[2]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[0]),
        .O(salida_7seg_1_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hC014)) 
    \salida_7seg_1_OBUF[3]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(salida_7seg_1_OBUF[3]));
  LUT4 #(
    .INIT(16'hA210)) 
    \salida_7seg_1_OBUF[4]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(salida_7seg_1_OBUF[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hAC48)) 
    \salida_7seg_1_OBUF[5]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(salida_7seg_1_OBUF[5]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h2094)) 
    \salida_7seg_1_OBUF[6]_inst_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(salida_7seg_1_OBUF[6]));
endmodule

module clk_divisor
   (clk_1k_OBUF,
    clk_15k_IBUF_BUFG,
    reset_IBUF);
  output clk_1k_OBUF;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;

  wire clk_15k_IBUF_BUFG;
  wire clk_1k_OBUF;
  wire reset_IBUF;
  wire \s_counter[0]_i_1__0_n_0 ;
  wire \s_counter[1]_i_1__0_n_0 ;
  wire \s_counter[2]_i_1__0_n_0 ;
  wire [2:1]s_counter_reg;
  wire \s_counter_reg_n_0_[0] ;
  wire s_out_clk_i_1__0_n_0;

  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h15)) 
    \s_counter[0]_i_1__0 
       (.I0(\s_counter_reg_n_0_[0] ),
        .I1(s_counter_reg[2]),
        .I2(s_counter_reg[1]),
        .O(\s_counter[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h1A)) 
    \s_counter[1]_i_1__0 
       (.I0(\s_counter_reg_n_0_[0] ),
        .I1(s_counter_reg[2]),
        .I2(s_counter_reg[1]),
        .O(\s_counter[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h2C)) 
    \s_counter[2]_i_1__0 
       (.I0(\s_counter_reg_n_0_[0] ),
        .I1(s_counter_reg[2]),
        .I2(s_counter_reg[1]),
        .O(\s_counter[2]_i_1__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[0]_i_1__0_n_0 ),
        .Q(\s_counter_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[1]_i_1__0_n_0 ),
        .Q(s_counter_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[2]_i_1__0_n_0 ),
        .Q(s_counter_reg[2]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    s_out_clk_i_1__0
       (.I0(s_counter_reg[2]),
        .I1(s_counter_reg[1]),
        .I2(clk_1k_OBUF),
        .O(s_out_clk_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    s_out_clk_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_out_clk_i_1__0_n_0),
        .Q(clk_1k_OBUF));
endmodule

(* ORIG_REF_NAME = "clk_divisor" *) 
module clk_divisor__parameterized1
   (s_clk_1hz,
    clk_1k_OBUF,
    reset_IBUF);
  output s_clk_1hz;
  input clk_1k_OBUF;
  input reset_IBUF;

  wire clk_1k_OBUF;
  wire [5:1]plusOp__0;
  wire reset_IBUF;
  wire s_clk_1hz;
  wire \s_counter[0]_i_1_n_0 ;
  wire \s_counter[1]_i_1_n_0 ;
  wire \s_counter[2]_i_1_n_0 ;
  wire \s_counter[3]_i_1_n_0 ;
  wire \s_counter[4]_i_1_n_0 ;
  wire \s_counter[5]_i_1_n_0 ;
  wire \s_counter[6]_i_1_n_0 ;
  wire \s_counter[7]_i_1_n_0 ;
  wire \s_counter[8]_i_1_n_0 ;
  wire \s_counter[9]_i_1_n_0 ;
  wire \s_counter[9]_i_2_n_0 ;
  wire \s_counter[9]_i_3_n_0 ;
  wire [9:0]s_counter_reg;
  wire s_out_clk_i_1_n_0;

  LUT6 #(
    .INIT(64'h0111111111111111)) 
    \s_counter[0]_i_1 
       (.I0(s_counter_reg[0]),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0222222222222222)) 
    \s_counter[1]_i_1 
       (.I0(plusOp__0[1]),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \s_counter[1]_i_2 
       (.I0(s_counter_reg[0]),
        .I1(s_counter_reg[1]),
        .O(plusOp__0[1]));
  LUT6 #(
    .INIT(64'h0222222222222222)) 
    \s_counter[2]_i_1 
       (.I0(plusOp__0[2]),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \s_counter[2]_i_2 
       (.I0(s_counter_reg[0]),
        .I1(s_counter_reg[1]),
        .I2(s_counter_reg[2]),
        .O(plusOp__0[2]));
  LUT6 #(
    .INIT(64'h0222222222222222)) 
    \s_counter[3]_i_1 
       (.I0(plusOp__0[3]),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \s_counter[3]_i_2 
       (.I0(s_counter_reg[1]),
        .I1(s_counter_reg[0]),
        .I2(s_counter_reg[2]),
        .I3(s_counter_reg[3]),
        .O(plusOp__0[3]));
  LUT6 #(
    .INIT(64'h0222222222222222)) 
    \s_counter[4]_i_1 
       (.I0(plusOp__0[4]),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \s_counter[4]_i_2 
       (.I0(s_counter_reg[2]),
        .I1(s_counter_reg[0]),
        .I2(s_counter_reg[1]),
        .I3(s_counter_reg[3]),
        .I4(s_counter_reg[4]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h0222222222222222)) 
    \s_counter[5]_i_1 
       (.I0(plusOp__0[5]),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \s_counter[5]_i_2 
       (.I0(s_counter_reg[3]),
        .I1(s_counter_reg[1]),
        .I2(s_counter_reg[0]),
        .I3(s_counter_reg[2]),
        .I4(s_counter_reg[4]),
        .I5(s_counter_reg[5]),
        .O(plusOp__0[5]));
  LUT6 #(
    .INIT(64'h0122112211221122)) 
    \s_counter[6]_i_1 
       (.I0(\s_counter[9]_i_2_n_0 ),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0133113322002200)) 
    \s_counter[7]_i_1 
       (.I0(\s_counter[9]_i_2_n_0 ),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0230123030303030)) 
    \s_counter[8]_i_1 
       (.I0(\s_counter[9]_i_2_n_0 ),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    \s_counter[9]_i_1 
       (.I0(\s_counter[9]_i_2_n_0 ),
        .I1(s_counter_reg[9]),
        .I2(s_counter_reg[8]),
        .I3(s_counter_reg[6]),
        .I4(\s_counter[9]_i_3_n_0 ),
        .I5(s_counter_reg[7]),
        .O(\s_counter[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \s_counter[9]_i_2 
       (.I0(s_counter_reg[5]),
        .I1(s_counter_reg[3]),
        .I2(s_counter_reg[1]),
        .I3(s_counter_reg[0]),
        .I4(s_counter_reg[2]),
        .I5(s_counter_reg[4]),
        .O(\s_counter[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAA8000000000)) 
    \s_counter[9]_i_3 
       (.I0(s_counter_reg[4]),
        .I1(s_counter_reg[0]),
        .I2(s_counter_reg[1]),
        .I3(s_counter_reg[2]),
        .I4(s_counter_reg[3]),
        .I5(s_counter_reg[5]),
        .O(\s_counter[9]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[0] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[0]_i_1_n_0 ),
        .Q(s_counter_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[1] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[1]_i_1_n_0 ),
        .Q(s_counter_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[2] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[2]_i_1_n_0 ),
        .Q(s_counter_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[3] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[3]_i_1_n_0 ),
        .Q(s_counter_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[4] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[4]_i_1_n_0 ),
        .Q(s_counter_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[5] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[5]_i_1_n_0 ),
        .Q(s_counter_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[6] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[6]_i_1_n_0 ),
        .Q(s_counter_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[7] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[7]_i_1_n_0 ),
        .Q(s_counter_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[8] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[8]_i_1_n_0 ),
        .Q(s_counter_reg[8]));
  FDCE #(
    .INIT(1'b0)) 
    \s_counter_reg[9] 
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\s_counter[9]_i_1_n_0 ),
        .Q(s_counter_reg[9]));
  LUT6 #(
    .INIT(64'h15555555EAAAAAAA)) 
    s_out_clk_i_1
       (.I0(s_counter_reg[9]),
        .I1(s_counter_reg[8]),
        .I2(s_counter_reg[6]),
        .I3(\s_counter[9]_i_3_n_0 ),
        .I4(s_counter_reg[7]),
        .I5(s_clk_1hz),
        .O(s_out_clk_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    s_out_clk_reg
       (.C(clk_1k_OBUF),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_out_clk_i_1_n_0),
        .Q(s_clk_1hz));
endmodule

module count_decider
   (s_increment_0,
    s_decrement_0,
    E,
    s_clk_1hz,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    up_down_IBUF);
  output s_increment_0;
  output s_decrement_0;
  output [0:0]E;
  input s_clk_1hz;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input up_down_IBUF;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire decrement_i_1_n_0;
  wire increment_i_1_n_0;
  wire last_enable;
  wire reset_IBUF;
  wire s_clk_1hz;
  wire s_decrement_0;
  wire s_increment_0;
  wire up_down_IBUF;

  LUT5 #(
    .INIT(32'hCCCC0050)) 
    decrement_i_1
       (.I0(up_down_IBUF),
        .I1(s_decrement_0),
        .I2(s_clk_1hz),
        .I3(last_enable),
        .I4(reset_IBUF),
        .O(decrement_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decrement_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(decrement_i_1_n_0),
        .Q(s_decrement_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF400040)) 
    increment_i_1
       (.I0(last_enable),
        .I1(s_clk_1hz),
        .I2(up_down_IBUF),
        .I3(reset_IBUF),
        .I4(s_increment_0),
        .O(increment_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    increment_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(increment_i_1_n_0),
        .Q(s_increment_0),
        .R(1'b0));
  FDCE #(
    .INIT(1'b0)) 
    last_enable_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(s_clk_1hz),
        .Q(last_enable));
  LUT2 #(
    .INIT(4'hE)) 
    \s_count[3]_i_1 
       (.I0(s_increment_0),
        .I1(s_decrement_0),
        .O(E));
endmodule

(* ORIG_REF_NAME = "count_decider" *) 
module count_decider_0
   (s_increment_1,
    E,
    overflow,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    up_down_IBUF);
  output s_increment_1;
  output [0:0]E;
  input overflow;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input up_down_IBUF;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire decrement_i_1__0_n_0;
  wire increment_i_1__0_n_0;
  wire last_enable;
  wire overflow;
  wire reset_IBUF;
  wire s_decrement_1;
  wire s_increment_1;
  wire up_down_IBUF;

  LUT5 #(
    .INIT(32'hCCCC0050)) 
    decrement_i_1__0
       (.I0(up_down_IBUF),
        .I1(s_decrement_1),
        .I2(overflow),
        .I3(last_enable),
        .I4(reset_IBUF),
        .O(decrement_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decrement_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(decrement_i_1__0_n_0),
        .Q(s_decrement_1),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF400040)) 
    increment_i_1__0
       (.I0(last_enable),
        .I1(overflow),
        .I2(up_down_IBUF),
        .I3(reset_IBUF),
        .I4(s_increment_1),
        .O(increment_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    increment_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(increment_i_1__0_n_0),
        .Q(s_increment_1),
        .R(1'b0));
  FDCE #(
    .INIT(1'b0)) 
    last_enable_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(overflow),
        .Q(last_enable));
  LUT2 #(
    .INIT(4'hE)) 
    \s_count[3]_i_1__0 
       (.I0(s_increment_1),
        .I1(s_decrement_1),
        .O(E));
endmodule

module counter_7seg
   (overflow,
    salida_7seg_0_OBUF,
    clk_15k_IBUF_BUFG,
    s_increment_0,
    s_decrement_0,
    reset_IBUF,
    E);
  output overflow;
  output [6:0]salida_7seg_0_OBUF;
  input clk_15k_IBUF_BUFG;
  input s_increment_0;
  input s_decrement_0;
  input reset_IBUF;
  input [0:0]E;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire overflow;
  wire reset_IBUF;
  wire s_decrement_0;
  wire s_increment_0;
  wire [6:0]salida_7seg_0_OBUF;

  decimal_counter counter
       (.E(E),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .overflow(overflow),
        .reset_IBUF(reset_IBUF),
        .s_decrement_0(s_decrement_0),
        .s_increment_0(s_increment_0),
        .salida_7seg_0_OBUF(salida_7seg_0_OBUF));
endmodule

(* ORIG_REF_NAME = "counter_7seg" *) 
module counter_7seg__parameterized1
   (salida_7seg_1_OBUF,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    s_increment_1);
  output [6:0]salida_7seg_1_OBUF;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input s_increment_1;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire counter_n_0;
  wire counter_n_1;
  wire counter_n_2;
  wire counter_n_3;
  wire reset_IBUF;
  wire s_increment_1;
  wire [6:0]salida_7seg_1_OBUF;

  decimal_counter__parameterized1 counter
       (.E(E),
        .Q({counter_n_0,counter_n_1,counter_n_2,counter_n_3}),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .reset_IBUF(reset_IBUF),
        .s_increment_1(s_increment_1));
  bcd7seg display
       (.Q({counter_n_0,counter_n_1,counter_n_2,counter_n_3}),
        .salida_7seg_1_OBUF(salida_7seg_1_OBUF));
endmodule

(* ECO_CHECKSUM = "dcea3eed" *) 
(* NotValidForBitStream *)
module cronometro
   (salida_7seg_0,
    salida_7seg_1,
    clk_1k,
    up_down,
    reset,
    clk_15k);
  output [6:0]salida_7seg_0;
  output [6:0]salida_7seg_1;
  output clk_1k;
  input up_down;
  input reset;
  input clk_15k;

  wire clk_15k;
  wire clk_15k_IBUF;
  wire clk_15k_IBUF_BUFG;
  wire clk_1k;
  wire clk_1k_OBUF;
  wire overflow;
  wire reset;
  wire reset_IBUF;
  wire s_clk_1hz;
  wire s_decrement_0;
  wire s_increment_0;
  wire s_increment_1;
  wire [6:0]salida_7seg_0;
  wire [6:0]salida_7seg_0_OBUF;
  wire [6:0]salida_7seg_1;
  wire [6:0]salida_7seg_1_OBUF;
  wire up_down;
  wire up_down_IBUF;
  wire updown_decider_0_n_2;
  wire updown_decider_1_n_1;

initial begin
 $sdf_annotate("cronometro_tb_time_impl.sdf",,,,"tool_control");
end
  BUFG clk_15k_IBUF_BUFG_inst
       (.I(clk_15k_IBUF),
        .O(clk_15k_IBUF_BUFG));
  IBUF clk_15k_IBUF_inst
       (.I(clk_15k),
        .O(clk_15k_IBUF));
  OBUF clk_1k_OBUF_inst
       (.I(clk_1k_OBUF),
        .O(clk_1k));
  counter_7seg display_0
       (.E(updown_decider_0_n_2),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .overflow(overflow),
        .reset_IBUF(reset_IBUF),
        .s_decrement_0(s_decrement_0),
        .s_increment_0(s_increment_0),
        .salida_7seg_0_OBUF(salida_7seg_0_OBUF));
  counter_7seg__parameterized1 display_1
       (.E(updown_decider_1_n_1),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .reset_IBUF(reset_IBUF),
        .s_increment_1(s_increment_1),
        .salida_7seg_1_OBUF(salida_7seg_1_OBUF));
  clk_divisor__parameterized1 divisor_1000_1
       (.clk_1k_OBUF(clk_1k_OBUF),
        .reset_IBUF(reset_IBUF),
        .s_clk_1hz(s_clk_1hz));
  clk_divisor divisor_15_1
       (.clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .clk_1k_OBUF(clk_1k_OBUF),
        .reset_IBUF(reset_IBUF));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  OBUF \salida_7seg_0_OBUF[0]_inst 
       (.I(salida_7seg_0_OBUF[0]),
        .O(salida_7seg_0[0]));
  OBUF \salida_7seg_0_OBUF[1]_inst 
       (.I(salida_7seg_0_OBUF[1]),
        .O(salida_7seg_0[1]));
  OBUF \salida_7seg_0_OBUF[2]_inst 
       (.I(salida_7seg_0_OBUF[2]),
        .O(salida_7seg_0[2]));
  OBUF \salida_7seg_0_OBUF[3]_inst 
       (.I(salida_7seg_0_OBUF[3]),
        .O(salida_7seg_0[3]));
  OBUF \salida_7seg_0_OBUF[4]_inst 
       (.I(salida_7seg_0_OBUF[4]),
        .O(salida_7seg_0[4]));
  OBUF \salida_7seg_0_OBUF[5]_inst 
       (.I(salida_7seg_0_OBUF[5]),
        .O(salida_7seg_0[5]));
  OBUF \salida_7seg_0_OBUF[6]_inst 
       (.I(salida_7seg_0_OBUF[6]),
        .O(salida_7seg_0[6]));
  OBUF \salida_7seg_1_OBUF[0]_inst 
       (.I(salida_7seg_1_OBUF[0]),
        .O(salida_7seg_1[0]));
  OBUF \salida_7seg_1_OBUF[1]_inst 
       (.I(salida_7seg_1_OBUF[1]),
        .O(salida_7seg_1[1]));
  OBUF \salida_7seg_1_OBUF[2]_inst 
       (.I(salida_7seg_1_OBUF[2]),
        .O(salida_7seg_1[2]));
  OBUF \salida_7seg_1_OBUF[3]_inst 
       (.I(salida_7seg_1_OBUF[3]),
        .O(salida_7seg_1[3]));
  OBUF \salida_7seg_1_OBUF[4]_inst 
       (.I(salida_7seg_1_OBUF[4]),
        .O(salida_7seg_1[4]));
  OBUF \salida_7seg_1_OBUF[5]_inst 
       (.I(salida_7seg_1_OBUF[5]),
        .O(salida_7seg_1[5]));
  OBUF \salida_7seg_1_OBUF[6]_inst 
       (.I(salida_7seg_1_OBUF[6]),
        .O(salida_7seg_1[6]));
  IBUF up_down_IBUF_inst
       (.I(up_down),
        .O(up_down_IBUF));
  count_decider updown_decider_0
       (.E(updown_decider_0_n_2),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .reset_IBUF(reset_IBUF),
        .s_clk_1hz(s_clk_1hz),
        .s_decrement_0(s_decrement_0),
        .s_increment_0(s_increment_0),
        .up_down_IBUF(up_down_IBUF));
  count_decider_0 updown_decider_1
       (.E(updown_decider_1_n_1),
        .clk_15k_IBUF_BUFG(clk_15k_IBUF_BUFG),
        .overflow(overflow),
        .reset_IBUF(reset_IBUF),
        .s_increment_1(s_increment_1),
        .up_down_IBUF(up_down_IBUF));
endmodule

module decimal_counter
   (overflow,
    salida_7seg_0_OBUF,
    clk_15k_IBUF_BUFG,
    s_increment_0,
    s_decrement_0,
    reset_IBUF,
    E);
  output overflow;
  output [6:0]salida_7seg_0_OBUF;
  input clk_15k_IBUF_BUFG;
  input s_increment_0;
  input s_decrement_0;
  input reset_IBUF;
  input [0:0]E;

  wire [0:0]E;
  wire clk_15k_IBUF_BUFG;
  wire geqOp__4;
  wire overflow;
  wire overflow_i_1_n_0;
  wire overflow_i_3_n_0;
  wire reset_IBUF;
  wire \s_count[0]_i_1_n_0 ;
  wire \s_count[1]_i_1_n_0 ;
  wire \s_count[2]_i_1_n_0 ;
  wire \s_count[3]_i_2_n_0 ;
  wire \s_count_reg_n_0_[0] ;
  wire \s_count_reg_n_0_[1] ;
  wire \s_count_reg_n_0_[2] ;
  wire \s_count_reg_n_0_[3] ;
  wire s_decrement_0;
  wire s_increment_0;
  wire [6:0]salida_7seg_0_OBUF;

  LUT6 #(
    .INIT(64'hFFFFAACF0000AAC0)) 
    overflow_i_1
       (.I0(geqOp__4),
        .I1(overflow_i_3_n_0),
        .I2(s_decrement_0),
        .I3(s_increment_0),
        .I4(reset_IBUF),
        .I5(overflow),
        .O(overflow_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    overflow_i_2
       (.I0(\s_count_reg_n_0_[2] ),
        .I1(\s_count_reg_n_0_[1] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[3] ),
        .O(geqOp__4));
  LUT4 #(
    .INIT(16'h0001)) 
    overflow_i_3
       (.I0(\s_count_reg_n_0_[2] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[3] ),
        .O(overflow_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    overflow_reg
       (.C(clk_15k_IBUF_BUFG),
        .CE(1'b1),
        .D(overflow_i_1_n_0),
        .Q(overflow),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h0000777F)) 
    \s_count[0]_i_1 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[2] ),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h55222254)) 
    \s_count[1]_i_1 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .I4(\s_count_reg_n_0_[0] ),
        .O(\s_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h52707024)) 
    \s_count[2]_i_1 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[1] ),
        .O(\s_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h64444449)) 
    \s_count[3]_i_2 
       (.I0(s_increment_0),
        .I1(\s_count_reg_n_0_[3] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[0] ),
        .I4(\s_count_reg_n_0_[2] ),
        .O(\s_count[3]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[0]_i_1_n_0 ),
        .Q(\s_count_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[1]_i_1_n_0 ),
        .Q(\s_count_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[2]_i_1_n_0 ),
        .Q(\s_count_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[3] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[3]_i_2_n_0 ),
        .Q(\s_count_reg_n_0_[3] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \salida_7seg_0_OBUF[0]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(salida_7seg_0_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h5390)) 
    \salida_7seg_0_OBUF[1]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(salida_7seg_0_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h5710)) 
    \salida_7seg_0_OBUF[2]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[1] ),
        .I2(\s_count_reg_n_0_[2] ),
        .I3(\s_count_reg_n_0_[0] ),
        .O(salida_7seg_0_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hC014)) 
    \salida_7seg_0_OBUF[3]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(salida_7seg_0_OBUF[3]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hA210)) 
    \salida_7seg_0_OBUF[4]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[0] ),
        .I2(\s_count_reg_n_0_[1] ),
        .I3(\s_count_reg_n_0_[2] ),
        .O(salida_7seg_0_OBUF[4]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hAC48)) 
    \salida_7seg_0_OBUF[5]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(salida_7seg_0_OBUF[5]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h2094)) 
    \salida_7seg_0_OBUF[6]_inst_i_1 
       (.I0(\s_count_reg_n_0_[3] ),
        .I1(\s_count_reg_n_0_[2] ),
        .I2(\s_count_reg_n_0_[0] ),
        .I3(\s_count_reg_n_0_[1] ),
        .O(salida_7seg_0_OBUF[6]));
endmodule

(* ORIG_REF_NAME = "decimal_counter" *) 
module decimal_counter__parameterized1
   (Q,
    E,
    clk_15k_IBUF_BUFG,
    reset_IBUF,
    s_increment_1);
  output [3:0]Q;
  input [0:0]E;
  input clk_15k_IBUF_BUFG;
  input reset_IBUF;
  input s_increment_1;

  wire [0:0]E;
  wire [3:0]Q;
  wire clk_15k_IBUF_BUFG;
  wire reset_IBUF;
  wire \s_count[0]_i_1_n_0 ;
  wire \s_count[1]_i_1_n_0 ;
  wire \s_count[2]_i_1_n_0 ;
  wire \s_count[3]_i_2_n_0 ;
  wire s_increment_1;

  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h00005776)) 
    \s_count[0]_i_1 
       (.I0(s_increment_1),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[0]),
        .O(\s_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h55220255)) 
    \s_count[1]_i_1 
       (.I0(s_increment_1),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\s_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h52507025)) 
    \s_count[2]_i_1 
       (.I0(s_increment_1),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(\s_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h44444440)) 
    \s_count[3]_i_2 
       (.I0(s_increment_1),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\s_count[3]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[0] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[0]_i_1_n_0 ),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[1] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[1]_i_1_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[2] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[2]_i_1_n_0 ),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \s_count_reg[3] 
       (.C(clk_15k_IBUF_BUFG),
        .CE(E),
        .CLR(reset_IBUF),
        .D(\s_count[3]_i_2_n_0 ),
        .Q(Q[3]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
