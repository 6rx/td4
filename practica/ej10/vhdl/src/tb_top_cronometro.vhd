library ieee;
use ieee.std_logic_1164.all;

entity cronometro_tb is
end cronometro_tb;

architecture testbench of cronometro_tb is

  component cronometro port(
    salida_7seg_0 : out std_logic_vector(6 downto 0);
    salida_7seg_1 : out std_logic_vector(6 downto 0);
    up_down : in std_logic;    
    clk_1k : out std_logic;
    reset : in std_logic;
    clk_15k : in std_logic);
  end component;

  signal seg_tb_0 : std_logic_vector(6 downto 0);
  signal seg_tb_1 : std_logic_vector(6 downto 0);
  signal reset_tb : std_logic;
  signal up_down_tb : std_logic;
  signal clk_salida : std_logic;
  signal clk_15k_tb : std_logic;

begin

  uut_cronometro : cronometro port map(
    salida_7seg_0 => seg_tb_0,
    salida_7seg_1 => seg_tb_1,
    clk_1k => clk_salida,
    up_down => up_down_tb,
    reset => reset_tb,
    clk_15k => clk_15k_tb);

  reset_tb_proc : process
    constant periodo : time := 33.333ns;
  begin
    reset_tb <= '0'; wait for periodo;
    reset_tb <= '1'; wait for 10*periodo;
    reset_tb <= '0'; wait;
  end process;

  updown_proc : process
    constant periodo : time := 33.333ns;
  begin
    up_down_tb <= '1'; wait for periodo*2*70*15*1000;
    up_down_tb <= '0'; wait for periodo*2*70*15*1000;
  end process;
  
  clock_tb_proc : process
    constant periodo : time := 33.333ns;
  begin
    clk_15k_tb <= '0'; wait for periodo;
    clk_15k_tb <= '1'; wait for periodo;
  end process;

end testbench;