--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº10: Cronómetro Up/Down de 0-59
-- Implementacion: top_cronometro.vhd, clk_divisor.vhd,
--                 bcd7seg.vhd, decimal_counter.vhd
-- Simulacion: tb_top_cronometro.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bcd7seg is port(
	segments : out std_logic_vector(6 downto 0);
	bcd : in std_logic_vector(3 downto 0));
end bcd7seg;

architecture logica of bcd7seg is
  
begin

  decoding : process(bcd)
  begin
      case bcd is
      when "0000" => segments <= "0000001"; -- "0"     
      when "0001" => segments <= "1001111"; -- "1" 
      when "0010" => segments <= "0010010"; -- "2" 
      when "0011" => segments <= "0000110"; -- "3" 
      when "0100" => segments <= "1001100"; -- "4" 
      when "0101" => segments <= "0100100"; -- "5" 
      when "0110" => segments <= "0100000"; -- "6" 
      when "0111" => segments <= "0001111"; -- "7" 
      when "1000" => segments <= "0000000"; -- "8"     
      when "1001" => segments <= "0000100"; -- "9" 
      when "1010" => segments <= "0000010"; -- a
      when "1011" => segments <= "1100000"; -- b
      when "1100" => segments <= "0110001"; -- C
      when "1101" => segments <= "1000010"; -- d
      when "1110" => segments <= "0110000"; -- E
      when "1111" => segments <= "0111000"; -- F
      when others => segments <= "1111111"; -- Prendo todos en error
      end case;
  end process;


end logica;
