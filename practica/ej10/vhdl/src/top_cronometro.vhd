--------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA)
-- Año: 2020.
-- Randazzo, Ignacio Agustín Leg. nº:70339
-- Práctico de entrenamiento nº10: Cronómetro Up/Down de 0-59
-- Implementacion: top_cronometro.vhd, clk_divisor.vhd,
--                 bcd7seg.vhd, decimal_counter.vhd
-- Simulacion: tb_top_cronometro.vhd
--------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity cronometro is port(
  salida_7seg_0 : out std_logic_vector(6 downto 0);
  salida_7seg_1 : out std_logic_vector(6 downto 0);
  clk_1k : out std_logic;
  up_down : in std_logic;
  reset : in std_logic;
  clk_15k : in std_logic);
end cronometro;

architecture logica of cronometro is
  
  component clk_divisor 
	generic(
		div : integer);
	port(
		reset : in std_logic;
		input_clk : in std_logic;
		output_clk : out std_logic);
  end component;
  
  component counter_7seg generic(
      max : integer := 9);
  port(
        clk : in std_logic;
        reset : in std_logic;
        overflow : out std_logic;        
        increment : in std_logic;
        decrement : in std_logic;
        segments : out std_logic_vector(6 downto 0));
  end component;
  
  component count_decider port(
    up_down : in std_logic;
    count_enable : in std_logic;
    increment : out std_logic;
    decrement : out std_logic;
    reset : in std_logic;    
    clk : in std_logic);
  end component;

  signal s_clk_1k : std_logic;
  signal s_clk_1hz : std_logic;
  signal s_increment : std_logic_vector(1 downto 0);
  signal s_decrement : std_logic_vector(1 downto 0);
  signal s_overflow : std_logic_vector(1 downto 0);  

begin
            
    divisor_15_1 : clk_divisor generic map(
        div => 15
    ) port map(
        reset => reset,
        input_clk => clk_15k,
        output_clk => s_clk_1k);      
          
    divisor_1000_1 : clk_divisor generic map(
        div => 1000
    ) port map(
        reset => reset,    
        input_clk => s_clk_1k,
        output_clk => s_clk_1hz);
        
        
    display_0 : counter_7seg port map(
      increment => s_increment(0),
      decrement => s_decrement(0),
      overflow => s_overflow(0),
      reset => reset,      
      segments => salida_7seg_0,    
      clk => clk_15k);      
      
    display_1 : counter_7seg generic map(
        max => 6) 
    port map(
      increment => s_increment(1),
      decrement => s_decrement(1),
      overflow => s_overflow(1),      
      reset => reset,
      segments => salida_7seg_1,    
      clk => clk_15k);
      
      
    updown_decider_0 : count_decider port map(
        up_down => up_down,
        count_enable => s_clk_1hz,
        increment => s_increment(0),
        decrement => s_decrement(0),
        reset => reset,
        clk => clk_15k);
        
    updown_decider_1 : count_decider port map(
        up_down => up_down,
        count_enable => s_overflow(0),
        increment => s_increment(1),
        decrement => s_decrement(1),
        reset => reset,
        clk => clk_15k);


    clk_1k <= s_clk_1k;    
        
end logica;
