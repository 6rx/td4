require 'open3'
require 'ruby2d'
require 'csv'

### User default config ###

vga_screen_size = [640, 480]
screen_scaling = 1
color_bits = 1
background_color = 'navy'
cursor_color = 'white'
sim_clk_freq_MHz = 125
pixel_clk_freq_MHz = 25.175
DEBUG = 0
front_h_porch = 16
back_h_porch = 48
front_v_porch = 11
back_v_porch = 31
screen_porch = [back_h_porch + front_h_porch, back_v_porch + front_v_porch]

### End of User default config ###


# AUX FUNCTIONS

def check_color_int(color, bits)
  if (color.is_a?Integer) then
    return (color / (2.0 ** (bits-1)))
  else
    return 0
  end
end

def dibujar(current_data, color_bits, draw_pos, screen_scaling)
  color_r = check_color_int( current_data[:red], color_bits )
  color_g = check_color_int( current_data[:green], color_bits )
  color_b = check_color_int( current_data[:blue], color_bits )
  
  pixel_color = [ color_r, color_g, color_b, 1]
  Square.new(x:draw_pos[0], y:draw_pos[1], size:screen_scaling, color: pixel_color)
  puts "Dibujo pixel color #{pixel_color}" if DEBUG==1
end


def getFilename()
  stdin, stdout, wait_thr = Open3.popen2('ruby -e "
    require \"fox16\"
    app = Fox::FXApp.new
    ven = Fox::FXMainWindow.new(app, \"Fox16-Ruby Interface\")
    app.create
    path = Fox::FXFileDialog.getOpenFilename(ven, \"abriendo\", \".\")
    app.runOneEvent()
    app.exit(0)
    app.destroy
    print path
  "')
  stdin.close
  sleep(1)
  Process.wait(wait_thr.pid)
  path = stdout.read
  stdout.close
  return path
end

# END AUX FUNCTIONS


@cursor = Square.new(x:0, y:0, size:screen_scaling, color:cursor_color)

set title: "VGA Emulator"
set background: background_color
set width: (vga_screen_size[0]+screen_porch[0])*screen_scaling
set height: (vga_screen_size[1]+screen_porch[1])*screen_scaling
set fps: 10000
p "Screen: [#{get :width},#{get :height}]" if DEBUG==1


data_file_path = getFilename()
p data_file_path if DEBUG==1
csv_data_lines = File.open(data_file_path).to_a
csv_names = csv_data_lines.first
p csv_names if DEBUG==1
csv_options = {headers: csv_names, converters: :numeric, header_converters: :symbol}

current_index = 245
draw_pos = [0,0]

update do
  dibujado = 0
  puts "Frame: #{get :frames}" if DEBUG==1
  while ( dibujado == 0 ) do
   current_data = CSV.parse_line(csv_data_lines[current_index], **csv_options)
    puts "Datos: #{current_data}" if DEBUG==1
    puts "[x,y]: [#{@cursor.x},#{@cursor.y}]" if DEBUG==1
    puts "Index: #{current_index}" if DEBUG==1

   if( current_data[:vsync] == 1 ) then
     @cursor.x = 0
     @cursor.y = 0
   elsif( current_data[:hsync] == 1 ) then
     @cursor.x = 0
     @cursor.y = draw_pos[1]+screen_scaling
   else
     draw_pos = [@cursor.x, @cursor.y]
     dibujar(current_data, color_bits, draw_pos, screen_scaling)
     dibujado = 1
     @cursor.x += screen_scaling
   end
   current_index += 1
  end
end

show
