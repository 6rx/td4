library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;


entity tb_top is
end tb_top;

architecture testbench of tb_top is
    constant periodo : time := 10 ns; -- 100MHz base clk
    constant pix_clk : time := 39.722 ns; -- approx 25.175MHz
    
    component topVGAdriver
        port ( clk : in  STD_LOGIC;
               reset : in  STD_LOGIC;
               selecHorVer : in  STD_LOGIC;
               VSync : out  STD_LOGIC;
               HSync : out  STD_LOGIC;
               R : out  STD_LOGIC;
               G : out  STD_LOGIC;
               B : out  STD_LOGIC);
    end component;
    
    signal tb_clk, tb_rst, tb_sel_hv : std_logic;
    signal tb_r, tb_g, tb_b, tb_hsync, tb_vsync: std_logic;
    file output_buf : text;
    
begin

    uut_vga : topVGAdriver port map(
        R => tb_r,
        G => tb_g,
        B => tb_b,
        VSync => tb_vsync,
        HSync => tb_hsync,
        selecHorVer => tb_sel_hv,       
        reset => tb_rst,      
        clk => tb_clk);

    reloj : process begin
        tb_clk <= '0';
        wait for periodo/2;
        tb_clk <= '1';
        wait for periodo/2;
    end process;
    
    reinicio : process begin
        tb_rst <= '1';
        wait for periodo*10;
        tb_rst <= '0'; wait;
    end process;

    selector : process begin
        tb_sel_hv <= '1';
        wait for periodo*1000;
        tb_sel_hv <= '0'; wait;
    end process;
    
    archivar : process
        variable write_col_to_output_buf : line;
    begin
        file_open(output_buf, "/home/nacho/repos/sexto/td4/utilidades/vga_emulator/vga_sim_data.csv",  write_mode);    
        wait for periodo/100;
        write(write_col_to_output_buf, string'("red, green, blue, hsync, vsync"));
        writeline(output_buf, write_col_to_output_buf);    
        loop   
            write(write_col_to_output_buf, tb_r); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_g); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_b); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_hsync); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_vsync);

            writeline(output_buf, write_col_to_output_buf);
            wait for pix_clk;                   
        end loop;
    end process;

end testbench;
