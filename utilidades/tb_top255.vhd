library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;


entity tb_top is
end tb_top;

architecture testbench of tb_top is
    constant periodo : time := 10 ns; -- 100MHz base clk
    --constant pix_clk : time := 38.722 ns; -- approx 25.175MHz
    constant pix_clk : time := 15.385 ns; -- approx 65.0MHz
    
    component topVGAdriver
        port ( clk : in  STD_LOGIC;
               vsync : out  STD_LOGIC;
               hsync : out  STD_LOGIC;
               r : out  STD_LOGIC;
               g : out  STD_LOGIC;
               b : out  STD_LOGIC);
    end component;
    
    signal tb_clk : std_logic;
    signal tb_r, tb_g, tb_b, tb_hsync, tb_vsync: std_logic;
    file output_buf : text;
    
begin

    uut_vga : topVGAdriver port map(
        r => tb_r,
        g => tb_g,
        b => tb_b,
        vsync => tb_vsync,
        hsync => tb_hsync,
        clk => tb_clk);

    reloj : process begin
        tb_clk <= '0';
        wait for periodo/2;
        tb_clk <= '1';
        wait for periodo/2;
    end process;
    
    archivar : process
        variable write_col_to_output_buf : line;
    begin
        file_open(output_buf, "/home/nacho/repos/sexto/td4/utilidades/vga_emulator/vga_sim_data.csv",  write_mode);    
        wait for periodo/100;
        write(write_col_to_output_buf, string'("red, green, blue, hsync, vsync"));
        writeline(output_buf, write_col_to_output_buf);    
        loop   
            write(write_col_to_output_buf, tb_r); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_g); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_b); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_hsync); write(write_col_to_output_buf, string'(","));
            write(write_col_to_output_buf, tb_vsync);

            writeline(output_buf, write_col_to_output_buf);
            wait for pix_clk;                   
        end loop;
    end process;

end testbench;